# custom_filters.py

from django import template

register = template.Library()

@register.filter
def mask_after_three(value):
    if len(value) > 3:
        return value[:3] + 'X' * (len(value) - 3)
    else:
        return value
