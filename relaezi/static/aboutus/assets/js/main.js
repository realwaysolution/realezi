"use strict";
/*
 * ----------------------------------------------------------------------------------------
 Author       : jrtemplate
 Template Name: Betra - Multipurpose
 Version      : 1.0
 * ----------------------------------------------------------------------------------------
 */

$(document).ready(function() {

  /*
   * ----------------------------------------------------------------------------------------
   *  PRELOADER JS
   * ----------------------------------------------------------------------------------------
   */
  $('.preloader').fadeOut();
  $('.preloader-area').delay(450).fadeOut('slow');

  /*
   * ----------------------------------------------------------------------------------------
   *  STICKY MENU JS
   * ----------------------------------------------------------------------------------------
   */

  var num = 0; //number of pixels before modifying styles

  $(window).bind('scroll', function() {
    if ($(window).scrollTop() > num) {
      $('.main-header').addClass('fixed');
    } else {
      $('.main-header').removeClass('fixed');
    }
  });

  /*
   * ----------------------------------------------------------------------------------------
   *  DROPDOWN MENU JS
   * ----------------------------------------------------------------------------------------
   */

  $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');

    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
      $('.dropdown-submenu .show').removeClass("show");
    });

    return false;
  });

  /*
   * ----------------------------------------------------------------------------------------
   *  Revolution Slider JS
   * ----------------------------------------------------------------------------------------
   */
   /* initialize the slider based on the Slider's ID attribute */
         
      jQuery('#rev_slider_1').show().revolution({
           sliderType: "standard",
           jsFileLocation: "assets/libs/revolution/js/",
           sliderLayout: "fullscreen",
           stopLoop: "off",
           stopAfterLoops: 0, //-1
           stopAtSlide: 1,
           responsiveLevels: [1240, 1024, 778, 485],
           gridwidth: [1400, 1170, 778, 480],
           /* [DESKTOP, LAPTOP, TABLET, SMARTPHONE] */
           visibilityLevels: [1240, 1024, 778, 485],
             /* basic navigation arrows and bullets */
             navigation: {

                 arrows: {
                     enable: false,
                     style: 'hesperiden',
                     hide_onleave: false
                 },

                 bullets: {
                     enable: true,
                     style: 'hesperiden',
                     hide_onleave: false,
                     h_align: 'center',
                     v_align: 'bottom',
                     h_offset: 0,
                     v_offset: 20,
                     space: 5
                 }
             }
         });


  /*
   * ----------------------------------------------------------------------------------------
   *  OWL CAROUSEL JS
   * ----------------------------------------------------------------------------------------
   */

  $('.owl-carousel-nav').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    margin: 15,
    nav: true,
    navText: ["&#xf3d5", "&#xf3d6"],
    pag: false,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  $('.owl-carousel-pag').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    margin: 10,
    nav: false,
    pag: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  $('.owl-carousel-team').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    margin: 30,
    nav: true,
    navText: ["&#xf361", "&#xf363"],
    pag: false,
    dots: false,
    responsive: {
      0: {
        items: 1,
        margin: 60
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  });

  $('.owl-carousel-team-pag').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    margin: 30,
    nav: false,
    pag: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  });

  $('.owl-carousel-clients').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    navText: ["&#xf361", "&#xf363"],
    pag: false,
    dots: false,
    responsive: {
      0: {
        items: 2,
        margin: 60
      },
      600: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });

  $('.owl-carousel-clients-pag').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    margin: 30,
    nav: false,
    pag: true,
    dots: true,
    responsive: {
      0: {
        items: 2
      },
      450: {
        items: 2
      },
      600: {
        items: 3
      },
      800: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
  });
  $('.owl-carousel-text-pag').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    margin: 30,
    nav: false,
    pag: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });
  $('.owl-carousel-text').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    margin: 30,
    nav: false,
    pag: true,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });

  /*
   * ----------------------------------------------------------------------------------------
   *  COUNTER JS
   * ----------------------------------------------------------------------------------------
   */
  if ($('.counter').length > 0) {
    $('.counter').counterUp({
      delay: 100,
      time: 1500
    });
  }

  /*
   * ----------------------------------------------------------------------------------------
   *  EASY PIE CHART JS
   * ----------------------------------------------------------------------------------------
   */

  if ($('.chart').length > 0) {
    $('.chart').easyPieChart({
      barColor: '#EF374A',
      scaleColor:false,
      easing: 'easeOutBounce',
      scaleLength: 1,
      lineCap: 'square',
      lineWidth:3,
      size:180,
      animate: {
                duration: 2500,
                enabled: true
            }
    });
  }

  /*
   * ----------------------------------------------------------------------------------------
   *  ACCORDIONS JS
   * ----------------------------------------------------------------------------------------
   */
  function toggleIcon(e) {
    $(e.target)
      .prev('.panel-heading')
      .find(".more-less")
      .toggleClass('ion-plus ion-minus');
  }
  $('.panel-group').on('hidden.bs.collapse', toggleIcon);
  $('.panel-group').on('shown.bs.collapse', toggleIcon);

  /*
   * ----------------------------------------------------------------------------------------
   *  ISOTOPE JS
   * ----------------------------------------------------------------------------------------
   */
  // activate isotope in container
  var $grid = $('#portfolio_items').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows'
  });

  // add isotope click function
  $('#portfolio_filter li').on("click", function() {
    var selector = $(this).attr('data-filter');
    $('#portfolio_items').isotope({
      filter: selector,
      animationOptions: {
        duration: 750,
        easing: 'linear',
        queue: false
      }
    });

    // change active class on li
    $('#portfolio_filter ul li').each(function() {
      var $portfolio_filter = $('#portfolio_filter ul li');
      $portfolio_filter.removeClass('active');
      $portfolio_filter.on('click', function() {
        $(this).addClass('active');
      });
    });
    return false;
  });



});
