let linechartBasicChart;

function getChartColorsArray(e) {
    const element = document.getElementById(e);
    if (element) {
        const colors = element.dataset.colors;
        if (colors) {
            return JSON.parse(colors).map(color => {
                const trimmedColor = color.replace(/\s/g, "");
                return trimmedColor.includes(",") ?
                    `rgba(${getComputedStyle(document.documentElement).getPropertyValue(trimmedColor.split(",")[0])}, ${trimmedColor.split(",")[1]})` :
                    getComputedStyle(document.documentElement).getPropertyValue(trimmedColor) || trimmedColor;
            });
        }
        console.warn("data-colors attribute not found on: " + e);
    }
}

function loadCharts() {
    updateChart('yesterday');
}

function updateChart(period) {
    let categories;
    let data;

    switch (period) {
        case 'yesterday':
            categories = Array.from({ length: 1 }, (_, index) => (index + 1).toString());
            data = [41];
            break;
        case 'last7days':
            categories = Array.from({ length: 7 }, (_, index) => (index + 1).toString());
            data = [10, 41, 35, 51, 49, 62, 69];
            break;
        case 'last30days':
            categories = Array.from({ length: 30 }, (_, index) => (index + 1).toString());
            data = [10, 41, 35, 51, 49, 62, 10, 41, 35, 51, 49, 62, 69, 91, 41, 35, 51, 49, 62, 69, 91, 10, 41, 35, 51, 49, 62, 69, 91, 148];
            break;
        default:
            categories = [];
            data = [];
    }

    const chartData = {
        series: [{
            name: "Desktops",
            data: data
        }],
        chart: {
            height: 350,
            type: "line",
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        markers: {
            size: 4
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: "straight"
        },
        xaxis: {
            categories: categories
        },
        yaxis: {
            min: 0,
            max: Math.max(...data) + 20
        }
    };

    if (linechartBasicChart) {
        linechartBasicChart.destroy();
    }

    linechartBasicChart = new ApexCharts(document.querySelector("#line_chart_basic"), chartData);
    linechartBasicChart.render();
}

// Initialize the chart on page load
window.addEventListener("resize", function() {
    setTimeout(() => {
        loadCharts();
    }, 250);
});

loadCharts();
