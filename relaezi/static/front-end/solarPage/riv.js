document.addEventListener("DOMContentLoaded", function () {
  const thunderLeft = new rive.Rive({
    src: "./assets/riv/thunder.riv",
    canvas: document.getElementById("thunder-left"),
    autoplay: true,
    onLoad: () => {
      thunderLeft.resizeDrawingSurfaceToCanvas();
    },
  });

  const thunderRight = new rive.Rive({
    src: "./assets/riv/thunder.riv",
    canvas: document.getElementById("thunder-right"),
    autoplay: true,
    onLoad: () => {
      thunderRight.resizeDrawingSurfaceToCanvas();
    },
  });

  const consultationSolar = new rive.Rive({
    src: "./assets/consultation-solar.riv",
    canvas: document.getElementById("desktopSolarConsultation"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });

  const solarInstallation = new rive.Rive({
    src: "./assets/installation-solar.riv",
    canvas: document.getElementById("desktopSolarInstallation"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });

  const serviceSupport = new rive.Rive({
    src: "./assets/service-support.riv",
    canvas: document.getElementById("desktopSolarServiceSupport"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });

  const mobileConsultationSolar = new rive.Rive({
    src: "./assets/consultation-solar.riv",
    canvas: document.getElementById("mobileSolarConsultation"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });

  const mobileSolarInstallation = new rive.Rive({
    src: "./assets/installation-solar.riv",
    canvas: document.getElementById("mobileSolarInstallation"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });

  const mobileServiceSupport = new rive.Rive({
    src: "./assets/service-support.riv",
    canvas: document.getElementById("mobileSolarServiceSupport"),
    autoplay: true,
    layout: new rive.Layout({
      fit: "contain",
      alignment: "center",
    }),
  });
});
