let normalstate = "";
let normalcategory = "";

// tablet or mobile view
let mobilestate = "";
let mobilecategory = "";

function selectState(normalselectedState) {
  normalstate = normalselectedState;
  document.getElementById("normalselectedState").textContent =
    normalselectedState;
}

function selectCategory(normalselectedCategory) {
  normalcategory = normalselectedCategory;
  document.getElementById("normalselectedCategory").textContent =
    normalselectedCategory;
}

// tablet or mobile view
function mobileselectState(selectedState) {
  mobilestate = selectedState;
  document.getElementById("selectedState").textContent = selectedState;
}

function mobileselectCategory(selectedCategory) {
  mobilecategory = selectedCategory;
  document.getElementById("selectedCategory").textContent = selectedCategory;
}

function calculate() {
  const consumption = parseFloat(
    document.getElementById("electricityBillInput").value
  );

  if (
    isNaN(consumption) ||
    consumption <= 0 ||
    !normalstate ||
    !normalcategory
  ) {
    alert(
      "Please enter a valid electricity bill and select both state and category."
    );
    return;
  }

  // Define conversion factors and costs
  const savingsFactor = 0.2; // 20% saving
  const costPerKwh = 8; // Cost per kWh in INR
  const spacePerKw = 100; // Space required per kW in sqft
  const co2PerKwYear = 3.2; // CO2 reduction per kW per year in tons

  // Calculate values
  const suggestedCapacity = consumption / 120;
  const monthlySavingsKwh = consumption * savingsFactor;
  const monthlySavingsInr = monthlySavingsKwh * costPerKwh;
  const requiredSpace = suggestedCapacity * spacePerKw;
  const co2Reduction = suggestedCapacity * co2PerKwYear;

  // Display results in your existing HTML structure
  document.getElementById("suggestedCapacity").textContent =
    suggestedCapacity.toFixed(1);
  document.getElementById("monthlySavingKWh").textContent =
    monthlySavingsKwh.toFixed(0);
  document.getElementById(
    "monthlySavingINR"
  ).textContent = `${monthlySavingsInr.toFixed(2)} ₹`;
  document.getElementById("requiredSpace").textContent =
    requiredSpace.toFixed(0);
  document.getElementById("co2Reduction").textContent = `${co2Reduction.toFixed(
    1
  )} ton`;
}

// tablet or mobile devices
function mobileCalculate() {
  const inputElement = document.getElementById("mobileElectricityBillInput");
  const consumption = parseFloat(inputElement.value);

  if (
    isNaN(consumption) ||
    consumption <= 0 ||
    !mobilestate ||
    !mobilecategory
  ) {
    alert(
      "Please enter a valid electricity bill and select both state and category."
    );
    return;
  }

  // Define conversion factors and costs
  const savingsFactor = 0.2; // 20% saving
  const costPerKwh = 8; // Cost per kWh in INR
  const spacePerKw = 100; // Space required per kW in sqft
  const co2PerKwYear = 3.2; // CO2 reduction per kW per year in tons

  // Calculate values
  const suggestedCapacity = consumption / 120;
  const monthlySavingsKwh = consumption * savingsFactor;
  const monthlySavingsInr = monthlySavingsKwh * costPerKwh;
  const requiredSpace = suggestedCapacity * spacePerKw;
  const co2Reduction = suggestedCapacity * co2PerKwYear;

  // Display results in your existing HTML structure
  document.getElementById("mobileSuggestedCapacity").textContent =
    suggestedCapacity.toFixed(1);
  document.getElementById("mobileMonthlySavingKWh").textContent =
    monthlySavingsKwh.toFixed(0);
  document.getElementById(
    "mobileMonthlySavingINR"
  ).textContent = `${monthlySavingsInr.toFixed(2)} ₹`;
  document.getElementById("mobileRequiredSpace").textContent =
    requiredSpace.toFixed(0);
  document.getElementById(
    "mobileCo2Reduction"
  ).textContent = `${co2Reduction.toFixed(1)} ton`;
}
