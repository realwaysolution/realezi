// document.addEventListener("DOMContentLoaded", function () {
//   const options = {
//     root: null,
//     rootMargin: "-250px",
//     threshold: 0.5,
//   };

//   let scrollingDown = true;

//   // Listen for scroll direction changes
//   let lastScrollTop = window.pageYOffset || document.documentElement.scrollTop;
//   window.addEventListener("scroll", function () {
//     let currentScrollTop =
//       window.pageYOffset || document.documentElement.scrollTop;

//     if (Math.abs(currentScrollTop - lastScrollTop) > 5) {
//       scrollingDown = currentScrollTop > lastScrollTop;
//       lastScrollTop = currentScrollTop <= 0 ? 0 : currentScrollTop;
//     }
//   });

//   const observer = new IntersectionObserver((entries) => {
//     entries.forEach((entry) => {
//       const targetId = entry.target.id.replace("content", "step");
//       const stepElement = document.getElementById(targetId);

//       if (entry.isIntersecting) {
//         // Activate the step
//         stepElement.classList.add("active");
//         entry.target.classList.add("active");

//         if (scrollingDown) {
//           // Disable scrolling when the center of steps 1 and 2 is visible (scrolling down)
//           if (entry.target.id !== "content3") {
//             document.body.classList.add("no-scroll");
//           }

//           // Enable scrolling when reaching the 3rd step (scrolling down)
//           if (entry.target.id === "content3") {
//             document.body.classList.remove("no-scroll");
//           }
//         } else {
//           // Disable scrolling when the center of steps 2 and 1 is visible (scrolling up)
//           if (entry.target.id !== "content1") {
//             document.body.classList.add("no-scroll");
//           }

//           // Enable scrolling when reaching the 1st step (scrolling up)
//           if (entry.target.id === "content1") {
//             document.body.classList.remove("no-scroll");
//           }
//         }
//       } else {
//         // Deactivate the step
//         stepElement.classList.remove("active");
//         entry.target.classList.remove("active");

//         // Ensure scroll is enabled when leaving the section (both directions)
//         if (entry.target.id === "content1" || entry.target.id === "content3") {
//           document.body.classList.remove("no-scroll");
//         }
//       }
//     });
//   }, options);

//   // Observe each step content
//   document.querySelectorAll(".step-content").forEach((section) => {
//     observer.observe(section);
//   });
// });
// Function to initialize GSAP animations
function initializeGsap() {
  gsap.registerPlugin(ScrollTrigger);

  const steps = document.querySelectorAll(".step");

  const tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".sticky-section",
      start: "top top",
      end: "+=2000",
      pin: true,
      scrub: true,
      anticipatePin: 1,
      onLeave: () => {
        ScrollTrigger.getById("sticky-section").kill();
      },
      onUpdate: (self) => {
        const progress = self.progress;
        const activeIndex = Math.min(
          Math.floor(progress * steps.length),
          steps.length - 1
        );

        steps.forEach((step, index) => {
          step.classList.toggle("active", index === activeIndex);
        });
      },
    },
  });

  tl.to(".way-work-card .card-body .step-content", {
    yPercent: -110 * (steps.length - 1),
    stagger: {
      each: 0.3,
    },
    ease: "none",
    duration: 1,
  });
}

// Function to check screen size and initialize GSAP
function checkScreenSize() {
  if (window.innerWidth > 992) {
    initializeGsap(); // Call the GSAP initialization function
  } else {
    // Optionally, you can kill the ScrollTrigger instance if it was created before
    ScrollTrigger.getAll().forEach((trigger) => trigger.kill());
  }
}

// Run on initial load
window.onload = checkScreenSize;

// Add event listener for resize
window.addEventListener("resize", checkScreenSize);

// Generic validation patterns
const patterns = {
  name: /^[A-Za-z\s]+$/,
  phone: /^\d{10}$/,
  otp: /^\d{4}$/,
  email: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
};

// Generic validation function
function validateInput(value, type) {
  return patterns[type].test(value);
}

// Function to validate and show error messages dynamically
function validateAndShowError(inputElement, errorElementId, type) {
  const value = inputElement.value.trim();
  const isValid = validateInput(value, type);
  const errorElement = document.getElementById(errorElementId);

  if (!isValid && value !== "") {
    errorElement.textContent = `Please enter a valid ${type}.`;
  } else {
    errorElement.textContent = ""; // Clear error message if valid or empty
  }

  return isValid;
}

// Function to validate checkboxes
function validateCheckbox(inputElement, errorElementId) {
  const isValid = inputElement.checked;
  const errorElement = document.getElementById(errorElementId);

  if (!isValid) {
    errorElement.textContent = "You must accept the terms and conditions.";
  } else {
    errorElement.textContent = ""; // Clear error message
  }

  return isValid;
}

// Function to toggle submit button based on form validity
function toggleSubmitButton(formId, buttonId, fields, checkbox) {
  const isValidForm =
    fields.every(({ id, type, errorId }) =>
      validateAndShowError(document.getElementById(id), errorId, type)
    ) &&
    validateCheckbox(document.getElementById(checkbox.id), checkbox.errorId);

  const submitBtn = document.getElementById(buttonId);

  if (isValidForm) {
    submitBtn.disabled = false;
    submitBtn.classList.remove("submit-btn-disabled");
    submitBtn.classList.add("submit-btn-enabled");
  } else {
    submitBtn.disabled = true;
    submitBtn.classList.remove("submit-btn-enabled");
    submitBtn.classList.add("submit-btn-disabled");
  }
}

// Setup dynamic validation for form fields
function setupValidation(formId, fields, buttonId, checkbox) {
  fields.forEach(({ id, type, errorId }) => {
    const inputElement = document.getElementById(id);
    inputElement.addEventListener("input", () => {
      validateAndShowError(inputElement, errorId, type);
      toggleSubmitButton(formId, buttonId, fields, checkbox);
    });
  });

  const checkboxElement = document.getElementById(checkbox.id);
  checkboxElement.addEventListener("change", () => {
    validateCheckbox(checkboxElement, checkbox.errorId);
    toggleSubmitButton(formId, buttonId, fields, checkbox);
  });
}

// Setup validation for "expertForm"
setupValidation(
  "expertForm",
  [
    { id: "fullname", type: "name", errorId: "nameError" },
    { id: "phone", type: "phone", errorId: "phoneError" },
    { id: "email", type: "email", errorId: "emailError" },
  ],
  "submitBtn",
  { id: "termsCondition", errorId: "checkError" }
);

document
  .getElementById("expertForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    // Hide the form
    document.getElementById("expertForm").style.display = "none";

    // Show the thank you message container
    const thankYouMessage = document.getElementById("thankYouMessage");
    thankYouMessage.style.display = "block";

    // Play the video
    const video = document.getElementById("thankYouVideo");
    video.play();

    setTimeout(() => {
      // Reduce the size of the video
      thankYouMessage.classList.add("reduceSize");

      // Show the text immediately and animate it from bottom to top
      document.getElementById("thankYouHeading").style.display = "block";
      document.getElementById("thankYouText").style.display = "block";

      // Trigger text animation
      document.getElementById("thankYouHeading").classList.add("show");
      document.getElementById("thankYouText").classList.add("show");
    }, 4000);
  });

// Function to show OTP sent message
function showOtpSentMessage(phoneNumber) {
  const messageElement = document.getElementById("otpSentMessage");
  messageElement.textContent = `We have sent the OTP to ${phoneNumber}.`;
  messageElement.style.display = "block";
}

// Event listener for Send OTP button
document.getElementById("sendOtpButton").addEventListener("click", () => {
  const countryCode = document.getElementById("countryCode").value;
  const phoneNumber = document
    .getElementById("freeconsultationphone")
    .value.trim();
  if (validateInput(phoneNumber, "phone")) {
    showOtpSentMessage(`${countryCode} ${phoneNumber}`);
  } else {
    document.getElementById("freeconsultationphoneError").textContent =
      "Please enter a valid phone number.";
  }
});

// Setup validation for "startSaving" form
setupValidation(
  "startSaving",
  [
    {
      id: "freeconsultationname",
      type: "name",
      errorId: "freeconsultationnameError",
    },
    {
      id: "freeconsultationphone",
      type: "phone",
      errorId: "freeconsultationphoneError",
    },
    { id: "otpInput", type: "otp", errorId: "otpInputError" },
    {
      id: "freeconsultationemail",
      type: "email",
      errorId: "freeconsultationemailError",
    },
  ],
  "freeconsultationsubmitButton",
  {
    id: "freeconsultationtermsCondition",
    errorId: "freeconsultationtermsConditionError",
  }
);

document
  .getElementById("startSaving")
  .addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent default form submission

    // Hide the form
    document.getElementById("startSaving").style.display = "none";

    // Show the thank you message container
    const thankYouMessage = document.getElementById(
      "freeconsultationthankYouMessage"
    );
    thankYouMessage.style.display = "block";

    // Play the video
    const video = document.getElementById("freeconsultationthankYouVideo");
    video.play();

    setTimeout(() => {
      // Reduce the size of the video
      thankYouMessage.classList.add("reduceSize");

      // Show the text immediately and animate it from bottom to top
      document.getElementById("freeconsultationthankYouHeading").style.display =
        "block";
      document.getElementById("freeconsultationthankYouText").style.display =
        "block";

      // Trigger text animation
      document
        .getElementById("freeconsultationthankYouHeading")
        .classList.add("show");
      document
        .getElementById("freeconsultationthankYouText")
        .classList.add("show");
    }, 4000);
  });
