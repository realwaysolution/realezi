from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class QuickInquiry(models.Model):
    location_type = models.CharField(max_length=50, blank=True, null=True)
    Budget = models.IntegerField(default=True, blank=True, null=True)
    property_type = models.CharField(max_length=50, blank=True, null=True)
    possession_type = models.CharField(max_length=50, blank=True, null=True)
    customer_name = models.CharField(max_length=150, blank=True, null=True)
    customer_email = models.CharField(max_length=150, blank=True, null=True)
    customer_contact = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        # app_label = 'default'
        db_table = "quick_inquiry"

    def _str_(self):
        return str(self.realezi)

class userMaster(models.Model):
    user_id = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    mobile_no = models.CharField(max_length=10)
    password = models.TextField(max_length=16)
    username = models.CharField(max_length=20,null=True)
    reg_otp = models.IntegerField(max_length=6,null=True)
    user_type = models.IntegerField(max_length=1,null=True)
    created_date = models.DateField(null=True)
    updated_date = models.TimeField(null=True)
    wallet_balance  = models.CharField(max_length=20,null=True)
    subscription_id = models.IntegerField(null=True)
    subscription_end_date = models.DateField(null=True)
    is_active = models.IntegerField(null=True)
    is_verify = models.IntegerField(default=0)
    is_agreement_agreed = models.IntegerField(default=0)
    is_profile_completed = models.IntegerField(null=True)
    latitude = models.CharField(max_length=20,null=True)
    longitude = models.CharField(max_length=20,null=True)
    

    class Meta:
        # app_label = 'default'
        db_table = "user_master"

    def _str_(self):
        return str(self.codetask)
    
    def check_otp(self, entered_otp):
        return self.reg_otp == int(entered_otp)

class UserMasterSubDetails(models.Model):
    id = models.AutoField(primary_key=True,max_length=20)
    builder_id = models.CharField(max_length=20)
    business_name = models.CharField(max_length=100,null=True)
    incorporated_since = models.CharField(max_length=30,null=True)
    ongoing_residential_projects = models.IntegerField(max_length=20,null=True)
    ongoing_commercial_projects = models.IntegerField(max_length=20,null=True)
    completed_residential_projects = models.IntegerField(max_length=20,null=True)
    completed_commercial_projects = models.IntegerField(max_length=20,null=True)
    company_hq_address = models.CharField(max_length=100,null=True)
    google_map_link = models.CharField(max_length=100,null=True)
    short_description = models.CharField(max_length=100,null=True)
    company_pan_no = models.CharField(max_length=20,null=True)
    rera_id  = models.CharField(max_length=20,null=True)
    rera_id  = models.CharField(max_length=20,null=True)
    personal_name = models.CharField(max_length=20,null=True)
    personal_designation = models.CharField(max_length=20,null=True)
    personal_phoneno = models.CharField(max_length=20,null=True)
    secondary_number = models.CharField(max_length=20,null=True)
    personal_whatsapp_number = models.CharField(max_length=20,null=True)
    personal_email = models.CharField(max_length=100,null=True)
    profile_picture = models.CharField(max_length=100,null=True)
    language = models.TextField(null=True)
    date_of_birth = models.DateField(null=True)
    city = models.CharField(max_length=20,null=True)
    pin_code = models.CharField(max_length=20,null=True)
    aadhar_image = models.CharField(max_length=100,null=True)
    gst_number = models.CharField(max_length=100,null=True)
    company_logo = models.CharField(max_length=100,null=True)
    pan_image = models.CharField(max_length=100,null=True)
    gst_image = models.CharField(max_length=100,null=True)
    gst_number = models.CharField(max_length=10,null=True)
    gst_name = models.CharField(max_length=10,null=True)
    bank_account_no = models.IntegerField(max_length=20,null=True)
    ifsc_code = models.CharField(max_length=20,null=True)
    bank_name = models.CharField(max_length=20,null=True)
    account_holder_name = models.CharField(max_length=20,null=True)    
    sales_director_name = models.CharField(max_length=20,null=True)
    sales_director_email = models.CharField(max_length=20,null=True)
    sales_director_number = models.CharField(max_length=20,null=True)    
    project_sales_manager_name = models.CharField(max_length=20,null=True)
    project_sales_manager_email = models.CharField(max_length=20,null=True)
    project_sales_manager_number = models.CharField(max_length=20,null=True)    
    finance_team_name = models.CharField(max_length=20,null=True)
    finance_team_email = models.CharField(max_length=20,null=True)
    finance_team_number = models.CharField(max_length=20,null=True)    
    whatsapp_notification = models.IntegerField(max_length=20,null=True)
    signature = models.CharField(max_length=100,null=True)
    signed_agreement = models.CharField(max_length=100,null=True)
    verification_stage = models.CharField(max_length=10,null=True)
    created_date = models.TimeField(null=True)
    updated_date = models.TimeField(null=True)
    is_active = models.IntegerField(null=True)

    class Meta:
        # app_label = 'default'
        db_table = "user_master_sub_details"

    def _str_(self):
        return str(self.codetask)
    
class Documents(models.Model):
    id = models.AutoField(primary_key=True, max_length=20)
    aadhar_document = models.FileField(upload_to='static/aadhar/', default='')
    pan_document = models.FileField(upload_to='static/pan/', default='')
    
    class Meta:
        # app_label = 'default'
        db_table = "document_path"

    def _str_(self):
        return str(self.codetask)

    
class BlogList(models.Model):
    blog_id = models.AutoField(primary_key=True)
    status = (
        ('active', 'Active'),
        ('deactive', 'Deactive'),
    )
    title = models.CharField(max_length=255)
    content = models.TextField(default='',null=True)
    author = models.TextField(default='',null=True)
    created_at = models.IntegerField(default=0)
    update_at =  models.IntegerField(default=0)

    class Meta:
        # app_label = 'default'
        db_table = "blog_list"

    def __str__(self):
        return self.name
    
class PropertyInquiry(models.Model):
    inquiry_id = models.AutoField(primary_key=True)
    status = (
        ('active', 'Active'),
        ('deactive', 'Deactive'),
    )
    name = models.CharField(max_length=255)
    mobile = models.TextField(default='',null=True)
    email = models.TextField(default='',null=True)
    message = models.TextField(default='',null=True)
    inquiry_property_id = models.IntegerField(default=1,max_length=12)
    created_at = models.IntegerField(default=0)
    update_at =  models.IntegerField(default=0)

    class Meta:
        # app_label = 'default'
        db_table = "property_inquiry"

    def __str__(self):
        return self.name

class PropertyMaster(models.Model):
    property_id = models.AutoField(primary_key=True)
    property_unique_id = models.CharField(max_length=255, null=True)
    property_for = models.CharField(max_length=255, verbose_name='Sale or Rent/Lease')
    property_type = models.CharField(max_length=255, verbose_name='Residential/Commercial')
    property_sub_type = models.CharField(max_length=255, verbose_name='showroom/coworkingspace')
    name = models.TextField(verbose_name='Name of Property')
    price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name='Price')
    rent_amount = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Rent Amount')
    rent_type = models.CharField(max_length=10, choices=[('monthly', 'Monthly'), ('yearly', 'Yearly')], null=True, blank=True, verbose_name='Monthly/Yearly')
    maintanence_amount = models.DecimalField(max_digits=30, decimal_places=2, null=True)
    maintainence_type = models.CharField(max_length=10, choices=[('monthly', 'Monthly'), ('quaterly', 'Quaterly'), ('yearly', 'Yearly')], null=True, blank=True)
    property_description = models.TextField(null=True)
    
    pg_name = models.TextField(null=True, blank=True, verbose_name='Name of PG')
    security_deposit = models.CharField(max_length=255, null=True, blank=True, choices=[('zero', 'Zero Deposit'),('one', 'One Month'),('two', 'Two Month'),('other', 'Other')])
    company_lease = models.CharField(max_length=255, null=True, blank=True, choices=[('yes', 'Yes'), ('no', 'No')])
    available_for = models.CharField(max_length=255, null=True, blank=True, choices=[('girls', 'Girls'), ('boys', 'Boys'), ('all', 'All')])
    available_from = models.CharField(max_length=255, null=True, blank=True, choices=[('immediately', 'Immediately'), ('later', 'Later')])
    suited_for = models.CharField(max_length=255, null=True, blank=True, choices=[('students', 'Students'), ('workingprofessionals', 'Working Professionals'), ('any', 'Any')])
    room_type = models.CharField(max_length=255, null=True, blank=True, choices=[('privateroom', 'Private Room'), ('twinsharing', 'Twin Sharing'), ('triplesharing', 'Triple Sharing'), ('quadsharing', 'Quad Sharing')])
    pg_furnished_status = models.CharField(max_length=255, null=True, blank=True, choices=[('furnished', 'Fully Furnished'), ('semifurished', 'Semi Furnished'), ('unfurnished', 'Un-Furnished')])
    food_available = models.CharField(max_length=255, null=True, blank=True, choices=[('breakfast', 'Breakfast'), ('lunch', 'Lunch'), ('dinner', 'dinner')])
    food_charges = models.CharField(max_length=255, null=True, blank=True, choices=[('yes', 'Yes'), ('no', 'No')])
    notice_period = models.CharField(max_length=255, null=True, blank=True, choices=[('15', '15'), ('30', '30'), ('45', '45'), ('others', 'Others')])
    electricity_charges = models.CharField(max_length=255, null=True, blank=True, choices=[('yes', 'Yes'), ('no', 'No')])
    no_of_bed = models.CharField(max_length=255, null=True)
    pg_rules = models.CharField(max_length=255, null=True, blank=True, choices=[('nosmoking', 'No Smoking'), ('noguardians', 'No Guardians Stay'), ('nodrinking', 'No Drinking'), ('nononveg', 'No Non-Veg'), ('visiterallow', 'Visiter Allow'), ('party', 'Party'), ('loudmusic', 'Loud music'), ('oppositegender', 'Opposite Gender')])
    gate_closing = models.CharField(max_length=255, null=True, blank=True, choices=[('yes', 'Yes'), ('no', 'No')])
    pg_services = models.CharField(max_length=255, null=True, blank=True, choices=[('wifi', 'WiFi'), ('laundary', 'Laundary Service'), ('wheelchair', 'Wheelchair Friendly'), ('pet', 'Pet Friendy'), ('roomcleaning', 'Room Cleaning')])
    
    
    
    
    ageofproperty_office = models.CharField(max_length=255, null=True)
    pantry = models.CharField(max_length=255, null=True)
    coworking_pantry = models.CharField(max_length=255, null=True)
    coveredparking = models.CharField(max_length=255, null=True)
    uncoveredparking = models.CharField(max_length=255, null=True)
    balcony = models.CharField(max_length=255, null=True)
    powerbackup = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    coworkingpowerbackup = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    parking = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    coworkingparking = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    liftavailable = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    coworkingliftavailable = models.CharField(max_length=3, choices=[('yes', 'Yes'), ('no', 'No')])
    office_view = models.CharField(max_length=20, choices=[('Beach View', 'Beach View'), ('Garden View', 'Garden View'), ('Golf Course', 'Golf Course'), ('Lake View', 'Lake View'), ('Park View', 'Park View'), ('Road View', 'Road View'), ('Community View', 'Community View'), ('Pool View', 'Pool View'), ('Creek View', 'Creek View'), ('Sea View', 'Sea View')])
    flooring = models.CharField(max_length=20, choices=[('Marble', 'Marble'), ('Concrete', 'Concrete'), ('Cemented', 'Cemented'), ('Carpeted', 'Carpeted'), ('Wooden', 'Wooden'), ('Others', 'Others')])
    floorno = models.IntegerField(null=True)
    coworkingfloorno = models.IntegerField(null=True)
    towerblock = models.CharField(max_length=255, null=True)
    unitno = models.CharField(max_length=200, null=True)
    no_of_rooms = models.IntegerField(null=True)
    possession_type = models.CharField(max_length=20, choices=[('Ready to Move', 'Ready to Move'), ('Under Construction', 'Under Construction')], blank=True)
    ageofproperty = models.CharField(max_length=255, null=True)
    possession_year = models.CharField(max_length=255, null=True)
    minlockinperiod = models.CharField(max_length=200, null=True)
    coworkingseatstype = models.CharField(max_length=20, choices=[('Conference Cabin', 'Conference Cabin'), ('Private Cabin', 'Private Cabin'), ('Open Seat', 'Open Seat')], null=True)
    coworking_noofseat = models.IntegerField(null=True)
    plotarea = models.DecimalField(max_digits=30, decimal_places=2, null=True)
    plot_area_type = models.CharField(max_length=255, null=True)
    terracearea = models.CharField(max_length=255, null=True)
    terrace_area_type = models.CharField(max_length=255, null=True)
    bathroom = models.IntegerField(null=True)
    booking_amount = models.DecimalField(max_digits=30, decimal_places=2, null=True)
    address = models.TextField(null=True)
    landmark = models.TextField(null=True)
    state = models.TextField(null=True)
    city = models.TextField(null=True)
    pincode = models.IntegerField(null=True)
    furnished_status = models.CharField(max_length=20, choices=[('Furnished', 'Furnished'), ('semifurished', 'Semi furnished'), ('No', 'No')], null=True)
    amenities = models.TextField(null=True)
    images = models.TextField(null=True)
    floorplan = models.TextField(null=True)
    rera_id = models.CharField(max_length=255, null=True)
    video_url = models.TextField(null=True)
    brochure_links = models.TextField(null=True)
    property_user_id = models.CharField(max_length=20, null=True)
    keyword =  models.TextField(null=True)
    property_sales_head =  models.CharField(max_length=20, null=True)
    sales_head_contact_no = models.CharField(max_length=20, null=True)
    sales_support_name = models.CharField(max_length=20, null=True)
    sales_support_contact_no = models.CharField(max_length=20, null=True)
    property_visits =models.IntegerField(default=0)
    site_visits = models.IntegerField(default=0)
    building_name = models.CharField(null=True,max_length=250)
    addressLine1 = models.CharField(null=True,max_length=250)
    addressLine2 = models.CharField(null=True,max_length=250)
    addressLine3 = models.CharField(null=True,max_length=250)
    commission = models.CharField(null=True, max_length=10)
    is_active = models.IntegerField(null=True)
    is_verify = models.IntegerField(null=True)
    is_permanently_deactive = models.IntegerField(null=True)
    religion_bias = models.CharField(max_length=20, null=True)
    religion_bias_note = models.CharField(max_length=20, null=True)
    google_map = models.CharField(max_length=50, null=True)
    hospital_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    school_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    railway_staion_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    mall_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    airport_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    restaurants_localitie = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    created_date = models.DateField(null=True)
    owner_name = models.CharField(max_length=20, null=True)
    owner_email = models.CharField(max_length=50, null=True)
    owner_number = models.CharField(max_length=20, null=True)
    owner_secondary_number = models.CharField(max_length=20, null=True)
    owner_aadhar_number = models.CharField(max_length=20, null=True)
    electricity_bill = models.CharField(max_length=50, null=True)
    property_deed = models.CharField(max_length=50, null=True)

    class Meta:
        # app_label = 'default'
        db_table = "property_master"

    def __str__(self):
        return self.name
    
class PropertyImages(models.Model):
    id = models.AutoField(primary_key=True)   
    property_id = models.CharField(max_length=20, null=True)
    images = models.TextField(null=True)
    category = models.CharField(max_length=20, null=True)
    cover_image = models.IntegerField(null=True)
    
    class Meta:
        db_table = "property_images"

    def __str__(self):
        return f"Property Image {self.id} "
    
class SavedProperty(models.Model):
    id = models.AutoField(primary_key=True)   
    property_id = models.CharField(max_length=20, null=True)
    property_name = models.CharField(max_length=20, null=True)
    user_id = models.CharField(max_length=20, null=True)
    created_date = models.DateField(null=True)
    
    class Meta:
        db_table = "saved_property"

    def __str__(self):
        return self.name
    
class EmployeeType(models.Model):
    id = models.AutoField(primary_key=True)   
    name = models.CharField(max_length=20, null=True)
    created_date = models.TimeField(null=True)
    updated_date = models.TimeField(null=True)
    
    class Meta:
        db_table = "employee_type"

    def __str__(self):
        return self.name
    
class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    employee_id = models.CharField(max_length=10, unique=True)    
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    mobile_no = models.CharField(max_length=10)
    password = models.TextField(max_length=16)
    decrypted_password = models.TextField(max_length=16, null=True)
    address = models.TextField()
    salary = models.CharField(max_length=50, null=True)
    aadhar_number = models.CharField(max_length=50, null=True)
    department = models.CharField(max_length=50, null=True)
    created_date = models.DateField(null=True)
    updated_date = models.DateField(null=True)
    is_active = models.IntegerField(null=True)
    employee_type = models.CharField(max_length=10, null=True)
        
    class Meta:
        db_table = "employee"

    def __str__(self):
        return self.name
    
class SiteVisit(models.Model):
    property_id = models.IntegerField(primary_key=True)
    property_name = models.CharField(max_length=100, null=True)
    buyer_id = models.CharField(max_length=10, null=True)
    buyer_name = models.CharField(max_length=100, null=True)
    builder_id = models.CharField(max_length=10, null=True)
    builder_name = models.CharField(max_length=100, null=True)
    visit_date = models.DateField(null=True)
    visit_time = models.CharField(max_length=20, null=True)
    created_date = models.DateField(null=True)
    updated_date = models.TimeField(null=True)
    status = models.IntegerField(null=True)
    rm_name = models.CharField(max_length=100, null=True)
    rm_id = models.CharField(max_length=20, null=True)
    buyer_note = models.TextField(null=True)
    admin_note = models.TextField(null=True)
    rm_note = models.TextField(null=True)
    otp = models.CharField(max_length=50, null=True)

    class Meta:
        # app_label = 'default'
        db_table = "site_visit"

    def __str__(self):
        return self.name
    
class SiteVisitHistory(models.Model):
    id = models.AutoField(primary_key=True)
    property_id = models.IntegerField(null=True)
    buyer_id = models.CharField(max_length=10, null=True)
    builder_id = models.CharField(max_length=10, null=True)
    employee_id = models.CharField(max_length=10, null=True)
    visit_date = models.DateField(null=True)
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)

    class Meta:
        # app_label = 'default'
        db_table = "site_visit_history"

    def __str__(self):
        return self.name

class Commissions(models.Model):
    id = models.AutoField(primary_key=True)
    property_id = models.CharField(max_length=20, null=True)
    buyer_id = models.CharField(max_length=10, null=True)
    builder_id = models.CharField(max_length=10, null=True)
    total_amount = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    commission_percentage = models.CharField(max_length=10, null=True)
    commission_amount = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    commission_payment_date = models.DateField(null=True)
    property_sold_date = models.DateField(null=True)
    status = models.IntegerField(null=True)
    created_date = models.DateField(null=True)
    updated_date = models.DateField(null=True)

    class Meta:
        # app_label = 'default'
        db_table = "commissions"

    def __str__(self):
        return self.name
    
class NotificationsList(models.Model):
    id = models.AutoField(primary_key=True)
    property_id = models.IntegerField(null=True)
    buyer_id = models.CharField(max_length=10, null=True)
    builder_id = models.CharField(max_length=10, null=True)
    employee_id = models.CharField(max_length=10, null=True)
    visit_date = models.DateField(null=True)
    builder_comments = models.TextField(null=True)
    buyer_comments = models.TextField(null=True)
    status = models.CharField(max_length=10,null=True)

    class Meta:
        # app_label = 'default'
        db_table = "notifications_list"

    def __str__(self):
        return str(self.property_id)
    
class TimeSlot(models.Model):
    slot_id = models.AutoField(primary_key=True)
    builder_id = models.CharField(max_length=10, null=True)
    days = models.CharField(max_length=20, null=True)
    from_time = models.CharField(max_length=20, null=True)
    to_time = models.CharField(max_length=20, null=True)
    created_date = models.DateField(null=True)
    updated_date = models.TimeField(null=True)

    class Meta:
        # app_label = 'default'
        db_table = "time_slot"

    def __str__(self):
        return self.days
    
class PropertyViewed(models.Model):    
    view_id = models.IntegerField(primary_key=True)
    property_master = models.ForeignKey(PropertyMaster, on_delete=models.CASCADE)
    buyer_id = models.CharField(max_length=10, null=True)
    viewed_date = models.TimeField(null=True)
    status = models.IntegerField(null=True)
    
    class Meta:
        # app_label = 'default'
        db_table = "property_viewed"

    def __str__(self):
        return self.name

class Payments(models.Model):
    payment_id = models.AutoField(primary_key=True)
    property_id = models.IntegerField(max_length=20, null=True)
    property_name = models.CharField(max_length=20, null=True)
    buyer_id = models.CharField(max_length=20, null=True)
    builder_id = models.CharField(max_length=20, null=True)
    total_payment = models.DecimalField(max_digits=15, decimal_places=2)
    received_payment = models.DecimalField(max_digits=15, decimal_places=2)
    pending_payment = models.DecimalField(max_digits=15, decimal_places=2)
    payment_type = models.CharField(max_length=20, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(auto_now=True)
    status = models.IntegerField(null=True)

    class Meta:
        db_table = "payments"

    def __str__(self):
        return f"{self.property_id} - {self.total_payment}"

class PaymentHistory(models.Model):
    id = models.AutoField(primary_key=True)
    payment_id = models.CharField(max_length=20, null=True)
    property_id = models.CharField(max_length=20, null=True)
    payment_amount = models.DecimalField(max_digits=10, decimal_places=2)
    payment_date = models.DateField(null=True)
    payment_note = models.TextField(null=True)
    cancel_payment_note = models.TextField(null=True)
    received_payment_date = models.DateField(null=True)
    status = models.IntegerField(null=True)

    class Meta:
        db_table = "payment_history"

    def __str__(self):
        return f"{self.property_id} - {self.payment_amount} - {self.payment_date}"
    
class PropertyViews(models.Model):
    property_id = models.IntegerField()
    views = models.IntegerField()
    clicks = models.IntegerField()
    date = models.DateField()
    
    class Meta:
        db_table = "property_views"
    
    def __str__(self):
        return f"{self.property_id} - Views: {self.views}, Clicks: {self.clicks}"  
    
class Variant(models.Model):
    sr_no = models.AutoField(primary_key=True)
    variant_id = models.ForeignKey(PropertyMaster, related_name='variants', on_delete=models.CASCADE)
    no_of_rooms = models.IntegerField(null=True)
    variant_type = models.CharField(max_length=20)    # Variant type (2BHK, 3BHK, or 4BHK)
    no_of_variant = models.IntegerField(null=True)
    sq_ft = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    bathroom = models.IntegerField(null=True)
    floor_image = models.TextField(null=True)
    
    class Meta:
        db_table = "variant"
    
    def __str__(self):
        return self.variant_type
    
class SubscriptionPlan(models.Model):
    subscription_id = models.AutoField(primary_key=True)
    subscription_name = models.TextField(null=True)
    subscription_price = models.DecimalField(max_digits=20, decimal_places=2, null=True)
    
    class Meta:
        db_table = "subscription_plan"
    
    def __str__(self):
        return self.name

class DataPoint(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    mobile_no = models.CharField(max_length=10)
    password = models.TextField(max_length=16)
    decrypted_password = models.TextField(max_length=16, null=True)
    date_of_birth = models.DateField(null=True)
    language = models.TextField(null=True)
    city = models.CharField(max_length=20,null=True)
    pin_code = models.CharField(max_length=20,null=True)
    facebook_links = models.CharField(max_length=200,null=True)
    instagram_links = models.CharField(max_length=200,null=True)
    linkedin_links = models.CharField(max_length=200,null=True)    
    registration_stage = models.CharField(max_length=10,null=True)
    
    class Meta:
        db_table = "data_point"
    
    def _str_(self):
        return self.name
    
class Links(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, null=True)
    link = models.CharField(max_length=100, null=True)
    link_type = models.CharField(max_length=10, null=True)
    mobile_no = models.CharField(max_length=10, null=True)
    allocated_for = models.CharField(max_length=10, null=True)
    comment = models.TextField(max_length=100, null=True)
    
    class Meta:
        db_table = "links_allocated"
    
    def _str_(self):
        return self.name

class Lead(models.Model):
    LEAD_SOURCES = [
        ('facebook', 'Facebook'),
        ('instagram', 'Instagram'),
        ('google_ads', 'Google Ads'),
        ('other', 'Other')
    ]

    LEAD_STATUSES = [
        ('new', 'New'),
        ('in_progress', 'In Progress'),
        ('converted', 'Converted'),
        ('closed', 'Closed'),
        ('deleted', 'Deleted')
    ]

    lead_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)
    source = models.CharField(max_length=20, choices=LEAD_SOURCES)
    status = models.CharField(max_length=20, choices=LEAD_STATUSES, default='new')
    revenue = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        db_table = "lead"
    def __str__(self):
        return f"{self.name} ({self.lead_id})"

class LeadHistory(models.Model):
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=Lead.LEAD_STATUSES)
    update_time = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = "lead_history"
    def __str__(self):
        return f"History of {self.lead.lead_id} ({self.status})"

class LeadSourceStats(models.Model):
    source = models.CharField(max_length=20, choices=Lead.LEAD_SOURCES)
    total_leads = models.IntegerField(default=0)
    converted_leads = models.IntegerField(default=0)
    total_revenue = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    period_start = models.DateField()
    period_end = models.DateField()

    class Meta:
        db_table = "lead_source_stats"
    def __str__(self):
        return f"Stats for {self.source}" 

class LeadReports(models.Model):
    REPORT_TYPES = [
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('custom', 'Custom')
    ]

    report_name = models.CharField(max_length=255)
    report_type = models.CharField(max_length=10, choices=REPORT_TYPES)
    report_data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "lead_reports"
    def __str__(self):
        return self.report_name
