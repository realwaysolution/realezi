try {
            DB::table('transaction_requests')->insert($txn_info);
        } catch (\Illuminate\Database\QueryException $ex) {
            if ($ex->getCode() == '45000') {
                // Duplicate transaction request error
                $response = [
                    'status' => 'false',
                    'msg' => 'Same receipt and amount just now hit one transaction. Try again after 5 minutes.',
                    'result' => ''
                ];
                
            } else {
                // Other database error
                //throw $ex;
                $response = array(
                    'status' => "false",
                    'msg' => "Same receipt and amount just now hit one Trasaction so Try again after 5 minute",
                    'result' =>""
                );
            
               
                Session::put('success','Kindly Check Transaction Report');
                
                $msg="Please Check Transaction Status in Report";
                
        		return redirect()->back()->with('merror', $msg ?? "Duplicate Transaction")->with('messages', $msg);
            }
        }