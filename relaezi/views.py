from django.contrib.auth import authenticate
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect

from relaezi.models import QuickInquiry, userMaster,UserMasterSubDetails, Documents,PropertyMaster, PropertyImages, SavedProperty, PropertyInquiry, Employee, SiteVisit, SiteVisitHistory, Commissions, NotificationsList, PropertyViewed, Payments, PaymentHistory, PropertyViews, Variant, TimeSlot, Links, DataPoint, Lead, LeadHistory, LeadSourceStats, LeadReports
from django.core.mail import send_mail
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User  # This is the default User model
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage

from django.http import HttpResponseServerError
from django.http import HttpResponseForbidden
from django.core.files.base import ContentFile
from django.db.models import Max, Min, Sum
from datetime import datetime, timedelta, date
from django.utils import timezone
from django.db.models import Q
from django.contrib.auth.hashers import make_password, check_password
from django.core.files.storage import default_storage
from django.urls import reverse

#from django.contrib.auth.models import PropertyMaster
import uuid
import os
import random
import string
import datetime
import inflect

import csv
import requests
from bs4 import BeautifulSoup
from django.http import HttpResponse

import time
import csv
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from django.http import HttpResponse
import pandas as pd
import matplotlib.pyplot as plt

def index(request):
    request.session['popup_shown'] = False
    del request.session['popup_shown']
    request.session['res_popup_shown'] = False
    del request.session['res_popup_shown']
    request.session['com_popup_shown'] = False
    del request.session['com_popup_shown']
    request.session['plo_popup_shown'] = False
    del request.session['plo_popup_shown']
    request.session['hom_popup_shown'] = False
    del request.session['hom_popup_shown']
    request.session['leg_popup_shown'] = False
    del request.session['leg_popup_shown']
    return render(request, 'indexone.html')

def newHome(request):
    return render(request, 'indexone.html')

def newHome2(request):
    return render(request, 'newhometwo.html')

def oldHome(request):
    return render(request, 'oldhomepagebackup.html')

def newLanding(request):
    return render(request, 'landing.html')

def propertyDetails(request, property_id):
    property = PropertyMaster.objects.filter(property_unique_id=property_id).first()
    
    if property:
        property.site_visits = (property.site_visits or 0) + 1 
        property.save()
        
    variants = property.variants.all() if property else []
    
    bhk_variants = {
        '2BHK': [],
        '3BHK': [],
        '4BHK': [],
    }
    
    for variant in variants:
        if variant.variant_type in bhk_variants:
            bhk_variants[variant.variant_type].append(variant)
            
    property_images = PropertyImages.objects.filter(property_id=property_id)

    categories = {}
    for image in property_images:
        if image.category not in categories:
            categories[image.category] = []
        categories[image.category].append(image)
        
    user = userMaster.objects.filter(user_id=property.property_user_id).first()
    show_floor_plan = user.user_type == 1 if user else False
    builder_name = user.name
    
    if user and user.user_type == 1:  # Builder
        unique_variants = set(variant.variant_type for variant in variants)
        property.configuration = sorted(unique_variants)
    elif user and user.user_type == 2:  # Buyer
        property.configuration = [f"{property.no_of_rooms}BHK"]
        
    booking_amount_value = None
    if property:
        if property.price and property.booking_amount:
            booking_amount_value = property.price * (property.booking_amount / 100)
            
    price_per_plot_area = None
    if property.plotarea and property.plotarea > 0:
        price_per_plot_area = property.price / property.plotarea
        
    property.amenities_list = property.amenities.split(', ') if property and property.amenities else []
        
    return render(request, 'propertydetails/index.html', {'property': property, 'bhk_variants': bhk_variants, 'categories': categories, 'property_images': property_images, 'show_floor_plan': show_floor_plan, 'booking_amount_value': booking_amount_value, 'builder_name': builder_name, 'price_per_plot_area': price_per_plot_area})

def save_property(request):
    if request.method == 'POST':
        property_id = request.POST.get('property_id')
        user_id = request.session.get('user_id')
        property_name = request.POST.get('property_name')

        SavedProperty.objects.create(
            property_id=property_id,
            property_name=property_name,
            user_id=user_id,
            created_date=datetime.date.today()
        )
        return redirect('buyer-journey')

    return JsonResponse({'success': False}, status=400)

def BookVisit(request):
    if request.method == 'POST':
        visit_date = request.POST.get('visit_date')
        visit_time = request.POST.get('visit_time')
        property_id = request.POST.get('property_id')
        property_name = request.POST.get('property_name')
        builder_name = request.POST.get('builder_name')
        builder_id = request.POST.get('builder_id')            
        buyer = userMaster.objects.get(user_id=request.session.get('user_id'))
        buyer_name = buyer.name            
        site_visit = SiteVisit(
            buyer_id=request.session['user_id'],
            buyer_name=buyer_name,
            builder_id=builder_id,
            builder_name=builder_name,
            visit_date=visit_date,
            property_id=property_id,
            property_name=property_name,
            visit_time=visit_time,
            otp = '12345',
            status=0 
        )
        site_visit.save()
            
        return redirect('buyer-journey')           
 

def format_price(value):
    """Format the price in K or Lakh format."""
    if value >= 100000:
        return f"Rs. {value / 100000:.2f} Lakh"
    elif value >= 1000:
        return f"Rs. {value / 1000:.2f} K"
    else:
        return f"Rs. {value:.2f}"

def propertyListingfront(request):
    sale_type = request.GET.get('sale_type', None)
    possession_stage = request.GET.get('possession_stage', None)
    bathroom_count = request.GET.get('bathroom_count', None)
    age_of_property = request.GET.get('age_of_property', None)
    selected_amenities = request.GET.getlist('amenities')
    
    properties = PropertyMaster.objects.annotate(
        min_variant_price=Min('variants__price'),
        max_variant_price=Max('variants__price'),
        min_variant_sq_ft=Min('variants__sq_ft'),
        max_variant_sq_ft=Max('variants__sq_ft'),
        total_variant_price=Sum('variants__price'),
        total_variant_sq_ft=Sum('variants__sq_ft')
    ).order_by('-property_id')
    
    bhk_types = request.GET.getlist('bhk_type')
    if bhk_types:
        properties = properties.filter(
            Q(variants__no_of_rooms__in=bhk_types) | Q(no_of_rooms__in=bhk_types)
        )
    
    if sale_type:
        if sale_type == "New Projects":
            properties = properties.filter(property_user_id__in=userMaster.objects.filter(user_type=1).values_list('user_id', flat=True))  # Builders
        elif sale_type == "Resale Property":
            properties = properties.filter(property_user_id__in=userMaster.objects.filter(user_type=2).values_list('user_id', flat=True))  # Buyers
            
    if possession_stage:
        if possession_stage == "Ready to Move":
            properties = properties.filter(possession_type='Ready to Move')
        elif possession_stage == "In 1 year":
            properties = properties.filter(possession_year=datetime.datetime.now().year + 1)
        elif possession_stage == "1 - 3 years":
            properties = properties.filter(possession_year=datetime.datetime.now().year + 1, possession_year__lte=datetime.datetime.now().year + 3)
        elif possession_stage == "3+ years":
            properties = properties.filter(possession_year__gte=datetime.datetime.now().year + 3)

    if bathroom_count:
        properties = properties.filter(
            Q(variants__bathroom__gte=int(bathroom_count)) | Q(bathroom__gte=int(bathroom_count))
        ).distinct()
        
    if age_of_property:
        if age_of_property == "Less than 1":
            properties = properties.filter(ageofproperty='0-1')
        elif age_of_property == "Less than 3":
            properties = properties.filter(ageofproperty='2-4')
        elif age_of_property == "Less than 7":
            properties = properties.filter(ageofproperty='5-7')
        elif age_of_property == "Less than 10":
            properties = properties.filter(ageofproperty='8-10')
        elif age_of_property == "More than 10":
            properties = properties.filter(ageofproperty='10+')
    
    if selected_amenities:
        amenities_query = Q()
        for amenity in selected_amenities:
            amenities_query |= Q(amenities__icontains=amenity)
        properties = properties.filter(amenities_query).distinct()
    
    for property in properties:
        if property.property_user_id:
            user_type = userMaster.objects.get(user_id=property.property_user_id).user_type
            if user_type == 1:
                property.posted_by_builder = True
            else:
                property.posted_by_builder = False
                
        if property.total_variant_sq_ft and property.total_variant_sq_ft > 0:
            property.average_price_per_sq_ft = property.total_variant_price / property.total_variant_sq_ft
        else:
            property.average_price_per_sq_ft = property.price / property.plotarea if property.plotarea else 0
                
        cover_image = PropertyImages.objects.filter(property_id=property.property_unique_id, cover_image=1).first()
        property.cover_image_url = cover_image.images if cover_image else None
    # return HttpResponse(bhk_types)
    return render(request, 'propertylisting/index.html', {'properties': properties})

def news_blog_page(request):
    return render(request, 'Frontend/news-blog-page.html')

def career(request):
    return render(request, 'Frontend/career.html', {'current_page': 'career'})

def application_form(request):
    return render(request, 'Frontend/application-form.html', {'current_page': 'career'})

def refer_and_earn_realezi(request):
    return render(request, 'Frontend/refer-and-earn-realezi.html')

def news_page(request):
    return render(request, 'Frontend/news-page.html')

def movers_and_packers(request):
    return render(request, 'Frontend/movers-and-packers.html')

def customer_review(request):
    return render(request, 'Frontend/customer_review.html')

def faq_first_page(request):
    return render(request, 'Frontend/faq-first-page.html', {'current_page': 'faq_first_page'})

def faq_second_page(request):
    return render(request, 'Frontend/faq-second-page.html', {'current_page': 'faq_second_page'})

def job_description(request):
    return render(request, 'Frontend/job-description.html', {'current_page': 'career'})

def job_list(request):
    return render(request, 'Frontend/job-list.html', {'current_page': 'career'})

def architecture(request):
    return render(request, 'Frontend/architecture.html', {'current_page': 'architecture'})

def disclaimer(request):
    return render(request, 'Frontend/disclaimer.html')

def privacy_page(request):
    return render(request, 'Frontend/privacy-page.html')

def terms_page(request):
    return render(request, 'Frontend/terms-page.html')

def solar_page(request):
    return render(request, 'Frontend/solar-page.html')

def contact_us(request):
    return render(request, 'Frontend/contact-us.html')

def Legal_Aid_Realezi(request):
    return render(request, 'Frontend/Legal_Aid_Realezi.html')

def interior(request):
    return render(request, 'Frontend/interior.html')

def blog_rent_buy(request):
    return render(request, 'Frontend/blog-page.html')

def footer_page(request):
    return render(request, 'Frontend/footer-page-new.html')

def subscription(request):
    if request.method == 'POST':
        subscription_id = request.POST.get('subscription_id')
        try:
            user = userMaster.objects.get(user_id=request.session['user_id'])
            user.subscription_id = subscription_id
            user.save()
            return redirect('buyer-bdashboard')
        except userMaster.DoesNotExist:
            return HttpResponse("User not found.")
    return render(request, 'Frontend/subscription.html')

def Contact(request):
    return render(request, 'contactus.html')

def AccessControl(request):
    return render(request, 'admin/accesscontrol.html')

def Aboutus(request):
    return render(request, 'aboutus.html')

def singlepropertyview(request,property_id):
    property = PropertyMaster.objects.filter(property_id=property_id).first()
    amenities_str = property.amenities
    first_image = ''
    
    property_images =  property.images
            #print(property_images)
    images_list = property_images.strip('[]').replace("'","").split(', ')
    
    
    if images_list:
            first_image = property_images[0]
    amenities_list = [amenity.strip() for amenity in amenities_str.split(',')]
    
    return render(request,'singlepropertyview.html',{'property':property,'amenities_list':amenities_list,'first_image':first_image})

def propertyInquiry(request):
    template_name = 'singleproperty.html'
    name = request.POST.get('name')
    email = request.POST.get('email')
    phone = request.POST.get('phone')
    property_id = 1
    
    message = request.POST.get('message')
    inquiry = PropertyInquiry(
        name=name,
        inquiry_property_id=property_id,
        email=email,
        mobile=phone,
        message = message
    )
    inquiry.save()
    
    property = PropertyMaster.objects.filter(property_id=property_id).first()
    amenities_str = property.amenities
    first_image = ''
    
    property_images =  property.images
            #print(property_images)
    images_list = property_images.strip('[]').replace("'","").split(', ')
    
    
    if images_list:
            first_image = property_images[0]
    amenities_list = [amenity.strip() for amenity in amenities_str.split(',')]
    return render(request,'singlepropertyview.html',{'property':property,'amenities_list':amenities_list,'first_image':first_image})

def ourservices(request):
    return render(request,'ourservice.html')
def faq(request):
    return render(request,'faq.html')
def termsandcondition(request):
    return render(request,'terms.html')
def blogs(request):
    return render(request,'blogs.html')
def privacy(request):
    return render(request,'privacy.html')

def blogdetail(request):
    return render(request,'blogsingle.html')
# def propertyList(request):
#     return render(request, 'myproperty.html')

def propertyview(request):
    return render(request, 'viewproperty.html')

def index_static(request):
    return render(request, 'index_static.html')


def scrapdata1(request):
    # Set up Selenium webdriver
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')  # Run Chrome in headless mode
    driver = webdriver.Chrome(options=options)
    
    #url = "https://housing.com/in/buy/searches/AB1D2P683blq40i8dktu9j_1wryht7qcuk4cbfu"
    url = "https://housing.com/rent/search-D2P1wryht7qcuk4cbfu"
    # Load the webpage
    driver.get(url)
    
    # Scroll down to load more content
    SCROLL_PAUSE_TIME = 3
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    
    # Parse the HTML content
    soup = BeautifulSoup(driver.page_source, "html.parser")
   
    # Find all divs with the specified class that contain titles
    divs_with_titles = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_3d3547ab _7s5wglyw _5vy24jg8 _blas1v10 new-title")
    
    # Find all divs with owner name
    divs_with_owners = soup.find_all("div", class_="css-1qe16qa")
    
    # Find all divs with price
    divs_with_prices = soup.find_all("div", class_="_csbfng _c8f6fq _g3gktf _ldyh40 _7l1ulh T_a6707275")
    
    # Find all divs with built-up area
    divs_with_areas = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_efe231cd _vy1ipv _7ltvct _g3dlk8 _c81fwx _cs1nn1 value")
    a_elements = soup.find_all("a", class_="_j31f9d _c8dlk8 _g3l52n _csbfng _frwh2y T_e4485809 _ks15vq _vv1q9c _sq1l2s T_091c165f")
    
    # Extract the href links for each <a> element
    href_links = [a_element.get("href") for a_element in a_elements]
    div_with_project = ''
    
    # Find all divs with project name
    
    # Check if any data is found
    if not divs_with_titles:
        return HttpResponse("No data found on the webpage.")

    # Prepare data for CSV
    data = []
    for title, owner, price, area,href_links in zip(divs_with_titles, divs_with_owners, divs_with_prices, divs_with_areas,href_links):
        title_text = title.text.strip()
        owner_text = owner.text.strip()
        price_text = price.text.strip()
        area_text = area.text.strip()
        project_text = ''
        href_links = f"https://housing.com{href_links}"
        data.append([title_text, owner_text, price_text, area_text,project_text,href_links])

    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="property_data.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Title', 'Owner', 'Price', 'Built-up Area', 'Project Name','link'])
    writer.writerows(data)
    
    # Close the Selenium webdriver
    driver.quit()

    return response

def scrapplot(request):
    # Set up Selenium webdriver
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')  # Run Chrome in headless mode
    driver = webdriver.Chrome(options=options)
    
    #url = "https://housing.com/in/buy/searches/AB1D2P683blq40i8dktu9j_1wryht7qcuk4cbfu"
    url = "https://housing.com/in/buy/searches/D2M3kP683blq40i8dktu9j_1wryht7qcuk4cbfu"
    # Load the webpage
    driver.get(url)
    
    # Scroll down to load more content
    SCROLL_PAUSE_TIME = 3
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    
    # Parse the HTML content
    soup = BeautifulSoup(driver.page_source, "html.parser")
   
    # Find all divs with the specified class that contain titles
    divs_with_titles = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_3d3547ab _7s5wglyw _5vy24jg8 _blas1v10 new-title")
    
    # Find all divs with owner name
    divs_with_owners = soup.find_all("div", class_="css-1qe16qa")
    
    # Find all divs with price
    divs_with_prices = soup.find_all("div", class_="_csbfng _c8f6fq _g3gktf _ldyh40 _7l1ulh T_a6707275")
    
    # Find all divs with built-up area
    divs_with_areas = soup.find_all("div", class_="T_d78b8637 _e21f4h T_9bc58cf2 _l8jw1y44 _1yzm15vq _1qsz1y44 _1ti0ioci _1y0kexct _lzn0jl56 _0h1h6o _fc1b1v _amkb7n _r31h6o _9s1txw")

    div_with_project = ''
    divs_with_areas = soup.find_all("div", class_="T_d78b8637 _e21f4h T_9bc58cf2 _l8jw1y44 _1yzm15vq _1qsz1y44 _1ti0ioci _1y0kexct _lzn0jl56 _0h1h6o _fc1b1v _amkb7n _r31h6o _9s1txw")
    # Find all divs with project name
    
    a_elements = soup.find_all("a", class_="_j31f9d _c8dlk8 _g3l52n _csbfng _frwh2y T_e4485809 _ks15vq _vv1q9c _sq1l2s T_091c165f")
    
    # Extract the href links for each <a> element
    href_links = [a_element.get("href") for a_element in a_elements]

    



    # Check if any data is found
    if not divs_with_titles:
        return HttpResponse("No data found on the webpage.")

    # Prepare data for CSV
    data = []
    for title, owner, price, area,href_link in zip(divs_with_titles, divs_with_owners, divs_with_prices, divs_with_areas,href_links):
        title_text = title.text.strip()
        owner_text = owner.text.strip()
        price_text = price.text.strip()
        area_text = area.text.strip()
        link_text = f"https://housing.com{href_link}"
        project_text = ''
        data.append([title_text, owner_text, price_text, area_text,project_text,link_text])
    
    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="property_data.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Title', 'Owner', 'Price', 'Built-up Area', 'Project Name','Link'])
    writer.writerows(data)
    
    # Close the Selenium webdriver
    driver.quit()

    return response

def scraparentdata(request):
    # Set up Selenium webdriver
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')  # Run Chrome in headless mode
    driver = webdriver.Chrome(options=options)
    
    #url = "https://housing.com/in/buy/searches/AB1D2P683blq40i8dktu9j_1wryht7qcuk4cbfu"
    url = "https://housing.com/rent/search-D2P1wryht7qcuk4cbfu"
    
    # Load the webpage
    driver.get(url)
    
    # Scroll down to load more content
    SCROLL_PAUSE_TIME = 3
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    
    # Parse the HTML content
    soup = BeautifulSoup(driver.page_source, "html.parser")
    
    # Find all divs with the specified class that contain titles
    divs_with_titles = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_3d3547ab _7s5wglyw _5vy24jg8 _blas1v10 new-title")
    
    # Find all divs with owner name
    divs_with_owners = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_e4485809 _1jfm1994 _7lr3uz")
    
    # Find all divs with price
    divs_with_prices = soup.find_all("div", class_="_csbfng _c8exct _g3qslr _fr1nms _1q73f6fq _4okugktf _1qkcc65e _j3gpidpf _4nrxyh40 T_a6707275")
    
    
    # Find all divs with built-up area
    divs_with_areas = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_efe231cd _vy1ipv _7ltvct _g3dlk8 _c81fwx _cs1nn1 value")
    

    #div_with_project = soup.find_all("div",class_="T_9bc58cf2 _l8jw1y44 _1yzm15vq _1qsz1y44 _1ti0ioci _1y0kexct _lzn0jl56 _0h1h6o _fc1b1v _e21a70 _amkb7n _r31h6o _9s1txw")
    
    # Find all divs with project name
    
    # Check if any data is found
    if not divs_with_titles:
        return HttpResponse("No data found on the webpage.")

    # Prepare data for CSV
    data = []
    for title, owner, price, area in zip(divs_with_titles, divs_with_owners, divs_with_prices, divs_with_areas):
        title_text = title.text.strip()
        owner_text = owner.text.strip()
        price_text = price.text.strip()
        area_text = area.text.strip()
        project_text = ''
        data.append([title_text, owner_text, price_text, area_text,project_text])

    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="property_data.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Title', 'Owner', 'Price', 'Built-up Area', 'Project Name'])
    writer.writerows(data)
    
    # Close the Selenium webdriver
    driver.quit()

    return response

def scrapcommercialdata(request):
    # Set up Selenium webdriver
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')  # Run Chrome in headless mode
    driver = webdriver.Chrome(options=options)
    
    #url = "https://housing.com/in/buy/searches/AB1D2P683blq40i8dktu9j_1wryht7qcuk4cbfu"
    url = "https://housing.com/commercial/buy/searches/P683blq40i8dktu9j"
    
    # Load the webpage
    driver.get(url)
    
    # Scroll down to load more content
    SCROLL_PAUSE_TIME = 3
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    
    # Parse the HTML content
    soup = BeautifulSoup(driver.page_source, "html.parser")
    
    # Find all divs with the specified class that contain titles
    divs_with_titles = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_3d3547ab _7s5wglyw _5vy24jg8 _blas1v10 new-title")
    
    # Find all divs with owner name
    divs_with_owners = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_e4485809 _1jfm1994 _7lr3uz")
    
    # Find all divs with price
    divs_with_prices = soup.find_all("div", class_="_csbfng _c8exct _g3qslr _fr1nms _1q73f6fq _4okugktf _1qkcc65e _j3gpidpf _4nrxyh40 T_a6707275")
    
    
    # Find all divs with built-up area
    divs_with_areas = soup.find_all("div", class_="T_091c165f _sq1l2s _vv1q9c _ks15vq T_efe231cd _vy1ipv _7ltvct _g3dlk8 _c81fwx _cs1nn1 value")
    

    #div_with_project = soup.find_all("div",class_="T_9bc58cf2 _l8jw1y44 _1yzm15vq _1qsz1y44 _1ti0ioci _1y0kexct _lzn0jl56 _0h1h6o _fc1b1v _e21a70 _amkb7n _r31h6o _9s1txw")
    
    # Find all divs with project name
    
    # Check if any data is found
    if not divs_with_titles:
        return HttpResponse("No data found on the webpage.")

    # Prepare data for CSV
    data = []
    for title, owner, price, area in zip(divs_with_titles, divs_with_owners, divs_with_prices, divs_with_areas):
        title_text = title.text.strip()
        owner_text = owner.text.strip()
        price_text = price.text.strip()
        area_text = area.text.strip()
        project_text = ''
        data.append([title_text, owner_text, price_text, area_text,project_text])

    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="property_data.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Title', 'Owner', 'Price', 'Built-up Area', 'Project Name'])
    writer.writerows(data)
    
    # Close the Selenium webdriver
    driver.quit()

    return response

def scraprentaldata(request):
    # Set up Selenium webdriver
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')  # Run Chrome in headless mode
    driver = webdriver.Chrome(options=options)
    
    url = "https://housing.com/paying-guests/search-P1wryht7qcuk4cbfu"
    
    # Load the webpage
    driver.get(url)
    
    # Scroll down to load more content
    SCROLL_PAUSE_TIME = 3
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    
    # Parse the HTML content
    soup = BeautifulSoup(driver.page_source, "html.parser")
    
    # Find all divs with the specified class that contain titles
    divs_with_titles = soup.find_all("div", class_="css-1ljyd60")
    
    # Find all divs with owner name
    divs_with_owners = soup.find_all("div", class_="css-1qe16qa")
    
    # Find all divs with price
    # Find the label element containing the price information
    label_elements = soup.find_all("label", class_="css-10oy6g6")
    
    # Extract the price text for each label
    prices = [label_element.get_text(strip=True) for label_element in label_elements]
    
    # Find the div elements containing the gender information
    gender_divs = soup.find_all("div", class_="css-jfno6a")
    
    # Extract the gender text for each gender div
    genders = [gender_div.get_text(strip=True).lower() if gender_div else "" for gender_div in gender_divs]
    
    # Combine the extracted information
    divs_with_prices = [f"{price} for {gender}" for price, gender in zip(prices, genders)]
    
    # Find all <a> elements with class "css-kfkq5b"
    a_elements = soup.find_all("a", class_="css-kfkq5b")
    
    # Extract the href links for each <a> element
    href_links = [a_element.get("href") for a_element in a_elements]

    
    
    # Check if any data is found
    if not divs_with_titles:
        return HttpResponse("No data found on the webpage.")

    # Prepare data for CSV
    data = []
    for title, owner, price, href_link in zip(divs_with_titles, divs_with_owners, divs_with_prices, href_links):
        title_text = title.text.strip()
        owner_text = owner.text.strip()
        price_text = price.strip()
        project_text = ''
        link_text = f"https://housing.com{href_link}"
        data.append([title_text, owner_text, price_text, project_text, link_text])
    
    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="property_data.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Title', 'Owner', 'Price', 'Project Name', 'Link'])
    writer.writerows(data)
    
    # Close the Selenium webdriver
    driver.quit()

    return response

def scrapdata(request):
    url = "https://housing.com/in/buy/searches/AB1D2P683blq40i8dktu9j_1wryht7qcuk4cbfu"

    # Send a GET request to the URL
    response = requests.get(url)

    # Parse the HTML content
    soup = BeautifulSoup(response.text, "html.parser")

    # Find all anchor tags within the specified class
    anchor_tags = soup.find_all("a", class_="_j31f9d _c8dlk8 _g3l52n _csbfng _frwh2y T_e4485809 _ks15vq _vv1q9c _sq1l2s T_091c165f")

    # Extract links from anchor tags and append "https://housing.com/" to each link
    links = ["https://housing.com/" + tag.get('href') for tag in anchor_tags]

    # Prepare data for CSV
    data = []
    for link in links:
        data.append([link])

    # Prepare CSV response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="links.csv"'

    # Write data to CSV
    writer = csv.writer(response)
    writer.writerow(['Link'])
    writer.writerows(data)

    return response

def BuilderIndex(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    properties = PropertyMaster.objects.filter(property_user_id=request.session['user_id'])
    active_properties = properties.filter(is_active=1).count()
    visit_scheduled = SiteVisit.objects.filter(builder_id=request.session['user_id']).count()
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    properties = PropertyMaster.objects.count()
    getStatusofAgreement = userMaster.objects.filter(user_id=request.session['user_id']).first()
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    payment_histories = PaymentHistory.objects.filter(payment_id__in=Payments.objects.filter(builder_id=request.session['user_id']).values_list('payment_id', flat=True))

    data = []
    for history in payment_histories:
        # Get corresponding Payment record
        payment = Payments.objects.filter(payment_id=history.payment_id).first()
        if payment:
            data.append({
                'payment_id': payment.payment_id,
                'payment_amount': history.payment_amount,
                'payment_date': history.payment_date,
                'id': history.id,
                'status': history.status
            })
            
    valid_payments = [item for item in data if item['status'] in {0, 2}]

    if valid_payments:
        next_payment_date = min(item['payment_date'] for item in valid_payments)
        next_payment_amount = next(item['payment_amount'] for item in valid_payments if item['payment_date'] == next_payment_date)
    else:
        next_payment_date = None 
        next_payment_amount = 0 
    
    commissions = Commissions.objects.filter(builder_id=request.session.get('user_id'), status=0)
    total_commissions = sum(commission.commission_amount for commission in commissions if commission.commission_amount is not None)
    if total_commissions is None:
        total_commissions = 0
    return render(request, 'builderpanel/builder-dashboard.html', {'user_details' : user_details, 'active_properties': active_properties, 'visit_scheduled': visit_scheduled, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset, 'next_payment_amount': next_payment_amount, 'total_commissions': total_commissions})

def addresidentialproperty(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_master = userMaster.objects.get(user_id=request.session['user_id'])
    if user_master.is_agreement_agreed != 1:
        return HttpResponse("Error: You must agree to the terms and conditions before creating a property.")

    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/add-residential-property.html', {'user_details' : user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def addcommercialproperty(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_master = userMaster.objects.get(user_id=request.session['user_id'])
    if user_master.is_agreement_agreed != 1:
        return HttpResponse("Error: You must agree to the terms and conditions before creating a property.")

    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/add-commercial-property.html', {'user_details' : user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def addBuilderProperty(request):
    #return render(request, 'builder/choosepackage.html')
    return render(request, 'builder/addresidentialproperty.html')

def addBuilderPropertyOld(request):
    #return render(request, 'builder/choosepackage.html')
    return render(request, 'builder/addcommercialproperty.html')

def manageInquiryView(request):
    inquiry = PropertyInquiry.objects.filter()
    return render(request,'builder/manageinquiry.html',{'inquiry':inquiry})

def SearchCriteriaView(request):
    template_name = 'newpropertylisting.html'
    alerts = ''
    if request.method == "POST":
        property_for=request.POST.get('property_for')
        property_type=request.POST.get('property_type')
        budget = request.POST.get('budget')
        keyword = request.POST.get('keyword')
        min_price = request.POST.get('min_price')
        max_price = request.POST.get('max_price')
        possession_type = request.POST.get('possession_type')
        template_name = "newpropertylisting.html"
        properties = PropertyMaster.objects.all()
        
        if property_for:
            properties = properties.filter(property_for = property_for)
        if property_type:
            properties = properties.filter(property_sub_type = property_type)
        
        if keyword:
            properties = properties.filter(keyword = keyword)
        if budget:
            min_budget, max_budget = budget.split(';')
            properties = properties.filter(price__gt = min_budget)
        if min_price:
            properties = properties.filter(price__gt = min_price)
        if max_price:
            properties = properties.filter(price__lt = max_price)
        if possession_type:
            properties = properties.filter(possession_type = possession_type)
        # properties = PropertyMaster.objects.filter(
        #     property_for = property_for,
        #     property_sub_type = property_type,
        #     price__gt = min_budget,
        #     keyword__icontains=keyword,
        # )
        
        if not properties:
            if property_for:
                properties = PropertyMaster.objects.filter(
                    price__gt = 0
        )
                alerts=alerts+"No Relevant Property Found, But Here's matching your need"
        images_list = []
        first_image = ''
        for property in properties:
            property_images =  property.images
            #print(property_images)
            property_images = property_images.strip('[]').replace("'","").split(', ')
            images_list.extend(property_images)
        print(properties.query)
        if images_list:
            first_image = property_images[0]
            
        #print(properties.query)
    return render(request,template_name,{'properties':properties,'first_image':first_image,'alerts':alerts})

def myproperty(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    # Get the user's ID from the session
    user_id = request.session.get('user_id')
    #return render(request,'propertylist.html')
    # Fetch all properties with user_id matching the logged-in user
    properties = PropertyMaster.objects.filter(property_user_id=user_id).order_by('-property_id')
    # Initialize an empty list to store all images
    images_list = []

    # Iterate over each property and collect its images
    for property in properties:
        # Assuming images is a field in your PropertyMaster model
        property_images = property.images
        images_list.extend(property_images)

    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/propertylist.html', {'properties': properties, 'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def bpersonaldetails(request):
    user_id = request.session.get('user_id')
    userdetails=UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    return render(request, 'builder/bpersonaldetails.html',{'userdetails':userdetails })

def mypropertys(request):
    # Get the user's ID from the session
    user_id = request.session.get('user_id')

    # Fetch all properties with user_id matching the logged-in user
    properties = PropertyMaster.objects.all()
    
    # Render the template with the properties
    return render(request, 'builderpanel/propertylist.html',locals())

def AllPropertyView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    properties = PropertyMaster.objects.all().order_by('-property_unique_id')
    return render(request, 'adminpanel/all-property.html', {'notifications': notifications, 'notification_count': notification_count, 'properties': properties})

def PropertyDetailView(request, property_id):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    seller = userMaster.objects.get(user_id=property.property_user_id)
    amenities_list = property.amenities.split(',') if property.amenities else []
    return render(request, 'adminpanel/view-property-detail.html',{'property': property, 'seller': seller, 'amenities_list': amenities_list, 'notifications': notifications, 'notification_count': notification_count})

def VerifyPropertyView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    properties = PropertyMaster.objects.filter(is_verify=1)    
    # Render the template with the properties
    return render(request, 'adminpanel/verify-property.html', {'properties': properties, 'notifications': notifications, 'notification_count': notification_count})

def AdminSiteVisitView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    site_visits = SiteVisit.objects.all()
    site_visit_history = SiteVisitHistory.objects.all()
    employees = Employee.objects.filter(is_active=1)
    if request.method == 'POST':
        property_id = request.POST.get('property_id')
        employee_id = request.POST.get('employee_id')
        site_visit = SiteVisit.objects.get(property_id=property_id)
        employee = get_object_or_404(Employee, employee_id=employee_id)
        site_visit.rm_name = employee.name
        site_visit.rm_id = employee.employee_id
        site_visit.save()
    return render(request, 'adminpanel/site-visit.html', {'site_visits': site_visits, 'employees': employees, 'site_visit_history': site_visit_history, 'notifications': notifications, 'notification_count': notification_count})

def AdminCancelSiteVisitView(request):
    if request.method == 'POST':
        admin_note = request.POST.get('admin_note')
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        site_visits.status = 7
        site_visits.admin_note = admin_note
        site_visits.save()
        return redirect('realeziadmin-site-visit')

def CommissionView(request):
    if request.method == 'POST':
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        buyer_id = request.POST.get('buyer_id')
        builder_id = request.POST.get('builder_id')
        total_amount = request.POST.get('total_amount')
        commission_percentage = request.POST.get('commission_percentage')
        commission_amount = request.POST.get('commission_amount')
        
        site_visits.status = 9
        site_visits.save()
        commission = Commissions(
            property_id=property_id,
            buyer_id=buyer_id,
            builder_id=builder_id,
            total_amount=total_amount,
            commission_percentage=commission_percentage,
            commission_amount=commission_amount,
            status=0,
            created_date=datetime.date.today()
        )
        commission.save()
        return redirect('realeziadmin-site-visit')

def PropertyVerificationView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.is_verify = not property.is_verify
    property.save()
    return redirect('realeziadmin-manageproperty')

def PropertyStatusView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.is_active = not property.is_active
    property.save()
    return redirect('realeziadmin-manageproperty')

def PropertyAdminStatusView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.is_permanently_deactive = not property.is_permanently_deactive
    property.save()
    return redirect('realeziadmin-manageproperty')

def PropertyDeleteView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.delete()
    return redirect('realeziadmin-manageproperty')

def BuyerPropertyDeleteView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.delete()
    return redirect('buyer-property')

def BuilderPropertyDeleteView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.delete()
    return redirect('myproperty')

def propertydetail(request,id):
    property_data = {'id': id}
    property_object = get_object_or_404(PropertyMaster, property_id=id)
    return render(request, 'builder/propertydetail.html', {'property': property_object})

def deletetetemproperty(request, id):
    property_data = get_object_or_404(propertyList,property_id = id)
    property_data.is_deleted = 0
    property_data.save()

    #Retrive propertylist of user logged in
    user_id = request.session.get('user_id')
    properties = propertyList.objects.filter(user_id=user_id,is_deleted=1)

    return render(request, 'addmin/myproperty.html',{'properties':properties})



#Ends Here


def adminSignIn(request):
    return render(request, 'addmin/sign-up.html')


def agent(request):
    return render(request, 'addmin/agent.html')


def contact(request):
    request.session['popup_shown'] = False
    del request.session['popup_shown']
    request.session['res_popup_shown'] = False
    del request.session['res_popup_shown']
    request.session['com_popup_shown'] = False
    del request.session['com_popup_shown']
    request.session['plo_popup_shown'] = False
    del request.session['plo_popup_shown']
    request.session['hom_popup_shown'] = False
    del request.session['hom_popup_shown']
    request.session['leg_popup_shown'] = False
    del request.session['leg_popup_shown']
    return render(request, 'contact.html')


def aboutus(request):
    request.session['popup_shown'] = False
    del request.session['popup_shown']
    request.session['res_popup_shown'] = False
    del request.session['res_popup_shown']
    request.session['com_popup_shown'] = False
    del request.session['com_popup_shown']
    request.session['plo_popup_shown'] = False
    del request.session['plo_popup_shown']
    request.session['hom_popup_shown'] = False
    del request.session['hom_popup_shown']
    request.session['leg_popup_shown'] = False
    del request.session['leg_popup_shown']
    return render(request, 'about-us.html')


def home_loan(request):
    return render(request, "home_loan.html")

def update_agreement_status(request):
    user_profile = userMaster.objects.get(username=request.session["user_username"])
    user_profile.is_agreement_agreed = 1
    user_profile.save()
    return JsonResponse({'status': 'success'})

def legal_doc(request):
    return render(request, "legal_doc.html")

def SaveInquiry(request):
    if request.method == "POST":
        cities = request.POST.get("cities")
        budget = request.POST.get("budget")
        ptypes = request.POST.get("ptypes")
        postypes = request.POST.get("postypes")
        name = request.POST.get("name")
        pno = request.POST.get("pno")
        email = request.POST.get("email")

        # Send email
        url = ''
        subject = 'Quick Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()} ,\n Locality {cities.capitalize()},\n Budget: {budget.capitalize()},\n Property Type: {ptypes.capitalize()},\n Possession: {postypes.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['popup_shown'] = False
            del request.session['popup_shown']

    return redirect('/')


def PlotsInquiry(request):
    if request.method == "POST":
        cities = request.POST.get("cities")
        budget = request.POST.get("budget")
        name = request.POST.get("name")
        pno = request.POST.get("pno")
        email = request.POST.get("email")
        sqft = request.POST.get("sqft")

        # Send email
        url = ''
        subject = 'Plots Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()} ,\n Locality {cities.capitalize()},\n Budget: {budget.capitalize()},\n Area: {sqft.capitalize()} Sqft'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('plo_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['plo_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['plo_popup_shown'] = False
            del request.session['plo_popup_shown']
    return redirect('/')


def CommercialInquiry(request):
    if request.method == "POST":
        cities = request.POST.get("cities")
        budget = request.POST.get("budget")
        name = request.POST.get("name")
        pno = request.POST.get("pno")
        email = request.POST.get("email")
        sqft = request.POST.get("sqft")

        # Send email 
        url = ''
        subject = 'Commercial Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()} ,\n Locality {cities.capitalize()},\n Budget: {budget.capitalize()},\n Carpet Area: {sqft.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('com_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['com_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['com_popup_shown'] = False
            del request.session['com_popup_shown']
    return redirect('/')


def ResidentialInquiry(request):
    if request.method == "POST":
        cities = request.POST.get("cities")
        budget = request.POST.get("budget")
        postypes = request.POST.get("postypes")
        name = request.POST.get("name")
        pno = request.POST.get("pno")
        email = request.POST.get("email")

        # Send email
        url = ''
        subject = 'Residential Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()} ,\n Locality {cities.capitalize()},\n Budget: {budget.capitalize()},\n Possession: {postypes.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('res_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['res_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['res_popup_shown'] = False
            del request.session['res_popup_shown']
    return redirect('/')


def BusinessInquiry(request):
    if request.method == "POST":

        name = request.POST.get("name")
        pno = request.POST.get("phone")
        email = request.POST.get("email")

        # Send email
        url = ''
        subject = 'Builder Registration Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('bui_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['bui_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['bui_popup_shown'] = False
            del request.session['bui_popup_shown']
    return redirect('/')


def HomeLoanInquiry(request):
    if request.method == "POST":
        name = request.POST.get("name")
        pno = request.POST.get("phone")
        email = request.POST.get("email")
        loan_amount = request.POST.get("loan_amount")

        # Send email
        url = ''
        subject = 'Home Loan Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()},\n Loan_Amount: {loan_amount.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('hom_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['hom_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['hom_popup_shown'] = False
            del request.session['hom_popup_shown']
    return redirect('/')


def LegalDocInquiry(request):
    if request.method == "POST":
        name = request.POST.get("name")
        pno = request.POST.get("phone")
        email = request.POST.get("email")

        # Send email
        url = ''
        subject = 'Legal Documents Inquiry From Website'
        message = f'Name: {name.capitalize()}, \n Phone: {pno.capitalize()},\n Email: {email.capitalize()}'
        from_email = 'mehtashyam13@gmail.com'
        recipient_list = ['hitesh@realezi.com']

        send_mail(subject, message, from_email, recipient_list)
        # QI_obj = QuickInquiry(location_type=cities, Budget=budget, property_type=ptypes,
        #                       customer_contact=pno, customer_email=email, possession_type=postypes,
        #                       customer_name=name)
        # QI_obj.save()
        show_popup = True

        show_popup = True  # or False, based on your conditions

        # Check if the popup has already been shown in the session
        if not request.session.get('leg_popup_shown', False):
            # Set a session variable to indicate that the popup has been shown
            request.session['leg_popup_shown'] = True

            # Redirect to a different route
            return render(request, 'index.html')
        else:
            # Clear the session variable
            request.session['leg_popup_shown'] = False
            del request.session['leg_popup_shown']
    return redirect('/')


@csrf_exempt
def signupView(request):
    if request.method == "POST":

        return redirect('/bdashboard')
        customer_name = request.POST.get("customer_name")
        customer_contact = request.POST.get("customer_contact")
        customer_email = request.POST.get("customer_email")
        business_name = request.POST.get("business_name")
        password = request.POST.get("password")
        confirm_password = request.POST.get("confirm_password")

        signUp = signup(customer_name=customer_name,
                        customer_email=customer_email,
                        customer_contact=customer_contact,
                        business_name=business_name,
                        password=password,
                        confirm_password=confirm_password)
        signUp.save()

    return redirect('/')


def sign_upView(request):
    if 'signupsuccess' in request.session:
        del request.session['signupsuccess']
    template_name = "login-signup/signup.html"
    if request.method == "POST":
        
        user_type = request.POST.get("user_type")
        name = request.POST.get("name")
        mobile_no = request.POST.get("mobile_no")
        email = request.POST.get("email")
        password = request.POST.get("password")
        repassword = request.POST.get("repassword")
        if password != repassword:
            # If passwords don't match, add error message and redirect
            messages.error(request, "Passwords do not match. Please try again.")
            return redirect('signup')
        password = make_password(password)
        reg_otp = random.randint(100000,999999)
        today_date = datetime.date.today()
        
        subscription_end_date = None
        subscription_id = None
        prefix = None
        
        # Generate unique user_id
        if user_type == '1':
            prefix = 'REBU'
            
        elif user_type == '2':
            prefix = 'RBUY'
            subscription_end_date = today_date + timedelta(days=30)
            subscription_id = 1
            
        else:
            raise ValueError("Invalid user_type")

        last_user_id = userMaster.objects.filter(user_type=user_type).order_by('user_id').last()
        if last_user_id:
            last_id = int(last_user_id.user_id[4:])  # remove prefix and convert to int
            new_id = last_id + 1
        else:
            new_id = 10001

        user_id = f"{prefix}{new_id:05d}"
        
        signUp = userMaster(user_id=user_id,
                        name=name,
                        email=email,
                        mobile_no=mobile_no,
                        reg_otp=reg_otp,
                        password=password,
                        user_type=user_type,
                        subscription_id=subscription_id,
                        subscription_end_date=subscription_end_date)
        signUp.save()
        
        # user_sub_details = UserMasterSubDetails(builder_id=signUp.user_id)
        user_sub_details = UserMasterSubDetails(builder_id=signUp.user_id,
                                               personal_name=signUp.name,
                                               personal_phoneno=signUp.mobile_no,
                                               personal_email=signUp.email)
        user_sub_details.save()
        
        # Add default time slots for builders
        if user_type == '1':
            time_slot = TimeSlot(builder_id=signUp.user_id,
                                days='Sunday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()        
            time_slot = TimeSlot(builder_id=signUp.user_id,
                                days='Monday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()        
            time_slot = TimeSlot(builder_id=signUp.user_id,    
                                days='Tuesday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()
            time_slot = TimeSlot(builder_id=signUp.user_id,   
                                days='Wednesday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()
            time_slot = TimeSlot(builder_id=signUp.user_id,         
                                days='Thursday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()
            time_slot = TimeSlot(builder_id=signUp.user_id,                  
                                days='Friday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()
            time_slot = TimeSlot(builder_id=signUp.user_id,
                                days='Saturday',
                                from_time='10 AM',
                                to_time='7 PM')
            time_slot.save()
        
        # Send a welcome email
        html_message = render_to_string('welcome_email.html', {'user': 'Realezi','otp':reg_otp})
        subject = 'Realezi-Your Profile Registered Successfully'
        message = 'Thank you for registering on our website!'
        from_email = settings.EMAIL_HOST_USER
        to_email = [email]

        send_mail(subject, message, from_email, to_email,html_message=html_message, fail_silently=False)

            # Log the user in
        
        #return redirect('home')
        request.session['mobile_no'] = mobile_no
        return redirect('/otpverification')
        request.session['signupsuccess'] = True

    return render(request, template_name, locals())

def signupView(request):
    if 'signupsuccess' in request.session:
        del request.session['signupsuccess']
        
    template_name = "login-signup/signup.html"
    
    if request.method == "POST":
        if not request.session.get('otp_verified'):
            return JsonResponse({"message": "OTP not verified."}, status=400)
        user_type = request.POST.get("register-as")
        name = request.POST.get("name")
        mobile_no = request.POST.get("mobile_no")
        email = request.POST.get("email")
        # today_date = datetime.date.today()
        subscription_start_date = datetime.datetime.strptime('2024-10-19', '%Y-%m-%d').date()

        subscription_end_date = None
        subscription_id = None
        prefix = None
        
        # Generate unique user_id
        if user_type == '1':
            prefix = 'REBU'
        elif user_type == '2':
            prefix = 'RBUY'
            subscription_end_date = subscription_start_date + datetime.timedelta(days=90)
            subscription_id = 0
        elif user_type == '3':
            prefix = 'RBVE'
        else:
            raise ValueError("Invalid user_type")

        last_user_id = userMaster.objects.filter(user_type=user_type).order_by('user_id').last()
        if last_user_id:
            last_id = int(last_user_id.user_id[4:])  # Remove prefix and convert to int
            new_id = last_id + 1
        else:
            new_id = 10001

        user_id = f"{prefix}{new_id:05d}"
        
        signUp = userMaster(
            user_id=user_id,
            name=name,
            email=email,
            mobile_no=mobile_no,
            reg_otp=1234,
            is_verify=1,
            user_type=user_type,
            subscription_id=subscription_id,
            subscription_end_date=subscription_end_date
        )
        signUp.save()
        
        user_sub_details = UserMasterSubDetails(
            builder_id=signUp.user_id,
            personal_name=signUp.name,
            personal_phoneno=signUp.mobile_no,
            personal_email=signUp.email
        )
        user_sub_details.save()
        
        # Add default time slots for builders
        if user_type == '1':
            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
            for day in days:
                TimeSlot.objects.create(builder_id=signUp.user_id, days=day, from_time='10 AM', to_time='7 PM')
        
        # Send a welcome email
        html_message = render_to_string('welcome_email.html', {'user': 'Realezi'})
        subject = 'Realezi - Your Profile Registered Successfully'
        message = 'Thank you for registering on our website!'
        from_email = settings.EMAIL_HOST_USER
        to_email = [email]
        send_mail(subject, message, from_email, to_email, html_message=html_message, fail_silently=False)

        request.session['mobile_no'] = mobile_no
        request.session['signupsuccess'] = True
        
        if user_type == '1':
            return redirect('service-dashboard')
        elif user_type == '2':
            return redirect('buyer-bdashboard')
        else:
            return redirect('login')

    return render(request, template_name)

def send_signup_otp(request):
    if request.method == "POST":
        mobile_no = request.POST.get("mobile_no")
        otp = 123456  # Use your OTP generation logic here
        request.session['otp'] = otp
        return JsonResponse({"message": "OTP sent successfully!"})

def verify_signup_otp(request):
    if request.method == "POST":
        otp = request.POST.get("otp")
        stored_otp = request.session.get('otp')
        if otp == str(stored_otp):
            request.session['otp_verified'] = True
            return JsonResponse({"message": "OTP verified successfully!"})
        else:
            return JsonResponse({"message": "Invalid OTP!"}, status=400)
        
def UserLoginView(request):
    template_name = "login-signup/login.html" 
    if request.method == 'POST':
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        customer_contact = request.POST.get('mobile_no')
        entered_otp = request.POST.get('otp')

        try:
            user = userMaster.objects.get(mobile_no=customer_contact)
        except userMaster.DoesNotExist:
            user = None

        if user:
            # Check if the OTP is correct
            if entered_otp == str(user.reg_otp):
                # Check if the account is active
                if user.is_active == 1:
                    # Additional check for builders
                    if user.user_type == 1 and user.is_verify != 1:
                        error_message = "Your account has not been activated or verified by the admin."
                    else:
                        # Save user's latitude and longitude
                        user.latitude = latitude
                        user.longitude = longitude
                        user.save()

                        # Authentication successful, log the user in
                        request.session['name'] = user.name
                        request.session['user_username'] = user.mobile_no
                        request.session['user_id'] = user.user_id
                        
                        user_sub_details = UserMasterSubDetails.objects.get(builder_id=user.user_id)
                        verification_stage = user_sub_details.verification_stage
                        
                        if user.user_type == 2:
                            return redirect('buyer-bdashboard')
                        elif user.user_type == 1:
                            return redirect('bdashboard' if verification_stage == '7' else 'service-dashboard')
                else:
                    error_message = "Your account has not been activated by the admin."
            else:
                error_message = "Invalid login credentials."
        else:
            error_message = "Invalid login credentials."

        return render(request, template_name, {'error_message': error_message})
    return render(request, template_name)

def send_login_otp(request):
    if request.method == 'POST':
        mobile_no = request.POST.get('mobile_no')
        otp = 1234
        try:
            user = userMaster.objects.get(mobile_no=mobile_no)
            user.reg_otp = otp
            user.save()
            
            return JsonResponse({'status': 'success', 'message': 'OTP sent successfully!'})
        except userMaster.DoesNotExist:
            return JsonResponse({'status': 'error', 'message': 'No user found with this mobile number.'})

    return JsonResponse({'status': 'error', 'message': 'Invalid request.'})

def otpverification(request):
    template_name = "otpverification.html"
    mobile_no = request.session.get('mobile_no', None)
    entered_otp = request.POST.get('entered_otp')
    
    if request.method == "POST":
        
        try:
            user_profile = userMaster.objects.filter(mobile_no=mobile_no).first()
            
            if user_profile:
                request.session["isLoggedIn"] = True
                request.session["user_username"] =  user_profile.name
                request.session["user_id"] = user_profile.user_id
                if user_profile.user_type == '1':
                    return redirect('builderpanel-login')
                elif user_profile.user_type == '2':
                    return redirect('buyerpanel-login')
                
            else:
                return render(request, 'otpverification.html',{'error_message':'Invalid OTP'})
        except userMaster.DoesNotExist:
            return render(request,'otpverification.html',{'error_message':'User Not Found'})

    return render(request,template_name,locals())

def builderLogin(request):
    template_name = "login-signup/buyer-login.html"
    if request.method == 'POST':
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        customer_contact = request.POST.get('mobile_no')
        password = request.POST.get('password')
        entered_otp = request.POST.get('otp')
        
        try:
            user = userMaster.objects.get(mobile_no=customer_contact, user_type=1) 
        except userMaster.DoesNotExist:
            user = None

        if user:
            # Check if the password is correct
            if entered_otp == str(user.reg_otp):
                # Check if the account is active
                if user.is_active == 1 and user.is_verify == 1:
                    # Save user's latitude and longitude
                    user.latitude = latitude
                    user.longitude = longitude
                    user.save()

                    # Authentication successful, log the user in
                    request.session['name'] = user.name
                    request.session['user_username'] = user.mobile_no
                    request.session['user_id'] = user.user_id
                    
                    user_sub_details = UserMasterSubDetails.objects.get(builder_id=user.user_id)
                    verification_stage = user_sub_details.verification_stage
                    
                    if verification_stage == '7':
                        return redirect('bdashboard')
                    else:
                        return redirect('service-dashboard')
                else:
                    # Account is inactive, display an error message
                    error_message = "Your account has not been activated or verified by the admin."
            else:
                # Password does not match, display an error message
                error_message = "Invalid login credentials."
        else:
            # User does not exist, display an error message
            error_message = "Invalid login credentials."

        return render(request, template_name, {'error_message': error_message})
    return render(request, template_name)

def logout(request):
    # Clear user session data
    request.session.clear()

    # Redirect to a login or home page after logout
    return redirect('login')


def myprofile(request):
    template_name='builder/myprofile.html'
    if request.method == 'POST':
        uploaded_file = request.FILES['aadhar_document']
        
        try:
            fs = FileSystemStorage(location='static/')
            filename = fs.save(uploaded_file.name, uploaded_file)
            file_url = fs.url(filename)
            
            # Do something with the file_url, such as saving it to the database
            # YourModel.objects.create(file_url=file_url)

            return HttpResponse("ok")
        except Exception as e:
            print(f"Error uploading file: {e}")
            return HttpResponseServerError("Internal Server Error")
        

        return HttpResponse("ok")
        builder_id = request.POST.get('builder_name')
        user = userMaster.objects.get(mobile_no=request.POST.get('customer_contact'))
        user.name = request.POST.get('builder_name')
        user.email = request.POST.get('email')
        user.mobile_no = request.POST.get('customer_contact')
        user.save()
        
        # user = UserMasterSubDetails.objects.get(builder_id=request.POST.get('customer_contact'))
        # user.business_name = request.POST.get('business_name')
        # user.incorporated_since = request.POST.get('incorporated_since')
        # user.ongoing_projects = request.POST.get('ongoing_projects')
        # user.complete_projects = request.POST.get('complete_projects')
        # user.about = request.POST.get('about')

        #Save Aadhar, PAN Card Images in Document table and store that id in usermastersubdetails table
        aadhar_document_path = request.FILES.get('aadhar_document')
        pan_document_path = request.FILES.get('pan_document')
        # Save the documents
        # Save the documents
        try:
            aadhar_path = handle_uploaded_file(aadhar_document_path, 'aadhar')
            #pan_path = handle_uploaded_file(pan_document_path, 'pan')
        except HttpResponseServerError as e:
            # Handle the error response or log it
            return e
        
        return HttpResponse("ok")
        aadhar_document_path = handle_uploaded_file(aadhar_document_path, 'aadhar')
        pan_document_path = handle_uploaded_file(pan_document_path, 'pan')
        # Save the documents
        aadhar_document = Documents(aadhar_document=aadhar_document_path)
        aadhar_document.save()

        pan_document = Documents(pan_document=pan_document_path)
        pan_document.save()

        # Create or get UserMasterSubDetails instance
        user_sub_details, created = UserMasterSubDetails.objects.get_or_create(builder_id=builder_id)
        user_sub_details.aadhar_document = aadhar_document
        user_sub_details.pan_document = pan_document
        user_sub_details.save()

        messages.success(request,'Profile updated successfully!',extra_tags="alert alert-success alert-dismissible fade show")
    user = userMaster.objects.get(id=request.session['user_id'])
    return render(request,'builder/myprofile.html',locals())

def handle_uploaded_file(file, folder):
    try:
        fs = FileSystemStorage(location=settings.MEDIA_ROOT + '/' + folder)
        filename = fs.save(file.name, file)
        return fs.url(filename)
    except Exception as e:
        # Handle the exception based on your application's needs
        # For example, you can log the error or return a custom response
        print(f"Error uploading file to {folder} folder: {e}")
        raise HttpResponseServerError("Internal Server Error")


def addpropertyView(request):
    return render(request, 'addproperty.html')

def businessinsights(request):
    return render(request,'businessinsights.html')

def addpropertyFromAdminView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    return render(request, 'adminpanel/add-property.html', {'notifications': notifications, 'notification_count': notification_count})

def SalesFromAdminView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    employees = Employee.objects.filter(department='Sales')
    return render(request, 'adminpanel/sales.html', {'employees': employees, 'notifications': notifications, 'notification_count': notification_count})

def AddSalesFromAdminView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        mobile_no = request.POST.get('mobile_no')
        password = request.POST.get("password")
        date_of_joining = request.POST.get('date_of_joining')
        salary = request.POST.get('salary')
        aadhar_number = request.POST.get('aadhar_number')
        address = request.POST.get('address')        
        encrypted_password  = make_password(password)
        decrypted_password = password
        last_employee = Employee.objects.all().order_by('id').last()
        if last_employee:
            last_id = int(last_employee.employee_id[3:])
            new_id = last_id + 1
        else:
            new_id = 10001
        
        employee_id = f'EMP{new_id:05d}'

        employee = Employee(
            employee_id=employee_id,
            name=name,
            email=email,
            mobile_no=mobile_no,
            password=encrypted_password,
            decrypted_password=decrypted_password,
            department='Sales',
            address=address,
            salary=salary,
            aadhar_number=aadhar_number,
            created_date=date_of_joining,
            is_active=0
        )
        employee.save()
        return redirect('realeziadmin-sales')
    return render(request, 'adminpanel/add-sales.html', {'notifications': notifications, 'notification_count': notification_count})

def SalesStatusView(request,  employee_id):
    employees = Employee.objects.get(employee_id=employee_id)
    employees.is_active = not employees.is_active
    employees.save()
    return redirect('realeziadmin-sales')

def DeleteSalesView(request, employee_id):
    employees = Employee.objects.get(employee_id=employee_id)
    employees.delete()
    return redirect('realeziadmin-sales')

def BuilderList(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    user_master_list = userMaster.objects.filter(user_type=1).order_by('-user_id')
    user_master_sub_details = UserMasterSubDetails.objects.all()

    # Join the two tables using the builder_id field
    users = []
    for user_master in user_master_list:
        sub_details = UserMasterSubDetails.objects.filter(builder_id=user_master.user_id).first()
        users.append({
            'user_master': user_master,
            'user_master_sub_details': sub_details,
        })
        
    all_builder = user_master_list.filter(user_type=1).count()
    active_builder = user_master_list.filter(is_active=1).count()
    pending_verification = user_master_list.filter(is_verify=0).count()
    today_registered_builders = user_master_list.filter(created_date=date.today()).count()

    return render(request,  "adminpanel/builder-dashboard.html", {'notifications': notifications, 'notification_count': notification_count, 'users': users, 'all_builder': all_builder, 'active_builder': active_builder, 'pending_verification': pending_verification, 'today_registered_builders': today_registered_builders})

def change_builder_status(request, user_id):
    users = userMaster.objects.get(user_id=user_id)
    users.is_active = not users.is_active
    users.save()
    return redirect('realeziadmin-builder-dashboard')

def change_buyer_status(request, user_id):
    users = userMaster.objects.get(user_id=user_id)
    users.is_active = not users.is_active
    users.save()
    return redirect('realeziadmin-user-dashboard')

def BuilderDetail(request, user_id):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    users = UserMasterSubDetails.objects.get(builder_id=user_id)
    user_detail = userMaster.objects.get(user_id=user_id)
    properties = PropertyMaster.objects.filter(property_user_id=user_id)

    if request.method == 'POST':
        tab = request.POST.get('tab')
        if tab == 'businessDetail':
            subject = 'Business Detail Rejection'
            message = 'Your business details are not proper. Please update your details.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        elif tab == 'personalDetail':
            subject = 'Personal Detail Rejection'
            message = 'Your personal details are not proper. Please update your details.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        elif tab == 'kycDetail':
            subject = 'KYC Detail Rejection'
            message = 'Your KYC details are not proper. Please update your details.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        elif tab == 'bankDetail':
            subject = 'Bank Detail Rejection'
            message = 'Your bank details are not proper. Please update your details.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        elif tab == 'teamDetail':
            subject = 'Team Detail Rejection'
            message = 'Your team details are not proper. Please update your details.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        elif tab == 'signatureDetail':
            subject = 'Signature Rejection'
            message = 'Your signature are not proper. Please update your signature.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [user_detail.email])
            return redirect('realeziadmin-builder-detail', user_id=user_id)
        
        else:
            if user_detail.is_verify == 0:
                user_detail.is_verify = 1
            else:
                user_detail.is_verify = 0
            user_detail.save()
            return redirect('realeziadmin-builder-detail', user_id=user_id)

    return render(request,  "adminpanel/builder-detail.html", {'notifications': notifications, 'notification_count': notification_count, 'users': users, 'properties': properties, 'user_detail': user_detail})

def UserList(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    user_master_list = userMaster.objects.filter(user_type=2).order_by('-user_id')
    user_master_sub_details = UserMasterSubDetails.objects.all()

    users = []
    for user_master in user_master_list:
        sub_details = UserMasterSubDetails.objects.filter(builder_id=user_master.user_id).first()
        property_count = PropertyMaster.objects.filter(property_user_id=user_master.user_id).count()
        users.append({
            'user_master': user_master,
            'user_master_sub_details': sub_details,
            'property_count': property_count,
        })
        
    all_user = user_master_list.filter(user_type=2).count()
    active_user = user_master_list.filter(is_active=1).count()
    pending_verification = UserMasterSubDetails.objects.filter(builder_id__in=user_master_list.values_list('user_id', flat=True)).exclude(verification_stage=1).count()
    today_registered_users = user_master_list.filter(created_date=date.today()).count()
    all_resellers = user_master_list.filter(user_id__in=PropertyMaster.objects.values('property_user_id')).count()
    
    return render(request,  "adminpanel/user-dashboard.html", {'notifications': notifications, 'notification_count': notification_count, 'users': users, 'all_user': all_user, 'active_user': active_user, 'pending_verification': pending_verification, 'today_registered_users': today_registered_users, 'all_resellers': all_resellers})


def dashboard(request):
    return render(request, 'dashboard.html')


def createPropertyView(request):
    user_id = request.session['user_id']
    user = userMaster.objects.filter(user_id=user_id).first()
    
    if request.method == "POST":
        last_property = PropertyMaster.objects.all().order_by('property_id').last()

        if last_property:
            # Extract the last sequential number from property_id
            last_id = int(last_property.property_unique_id.split('_')[-1])  # Get the last part after the underscore
            new_id = last_id + 1
        else:
            new_id = 10001  # Start with 10001 if no properties exist or property_unique_id is None

        # Create the new property_id with zero-padding
        property_unique_id = f'PROP_{user_id}_{new_id:05d}'
        
        name = request.POST.get('name')
        furnished_status = request.POST.get('furnished_status')
        property_for = request.POST.get('property_for', 'sale')
        property_type = request.POST.get('property_type', 'residential')
        property_sub_type = request.POST.get('property_sub_type', 'apartment')
        if property_sub_type == 'pg':
            name = request.POST.get('pg_name')        
        price = float(request.POST.get('price', 500000)) if request.POST.get('price') else 0
        rent_amount = float(request.POST.get('rent_amount', 0)) if request.POST.get('rent_amount') else 0 
        rent_type = request.POST.get('rent_type', 'monthly')
        maintanence_amount = float(request.POST.get('maintanence_amount', 0)) if request.POST.get('maintanence_amount') else 0 
        maintainence_type = request.POST.get('maintainence_type', 'monthly')
        property_description = request.POST.get('property_description', 'A beautiful property')
        ageofproperty_office = request.POST.get('ageofproperty_office', '5 years')
        pantry = request.POST.get('pantry', 'yes')
        coworking_pantry = request.POST.get('coworking_pantry', 'yes')
        coveredparking = request.POST.get('coveredparking', 'yes')
        uncoveredparking = request.POST.get('uncoveredparking', 'no')
        balcony = request.POST.get('balcony', 'yes')
        powerbackup = request.POST.get('powerbackup', 'yes')
        coworkingpowerbackup = request.POST.get('coworkingpowerbackup', 'no')
        parking = request.POST.get('parking', 'yes')
        coworkingparking = request.POST.get('coworkingparking', 'no')
        liftavailable = request.POST.get('liftavailable', 'yes')
        coworkingliftavailable = request.POST.get('coworkingliftavailable', 'no')
        office_view = request.POST.get('office_view', 'Garden View')
        flooring = request.POST.get('flooring', 'Marble')
        floorno = int(request.POST.get('floorno', 0)) if request.POST.get('floorno') else 0  # Adjust as needed, providing a default value of 0
        coworkingfloorno = int(request.POST.get('coworkingfloorno', 0)) if request.POST.get('coworkingfloorno') else 0  # Adjust as needed, providing a default value of 0
        towerblock = request.POST.get('towerblock', 'Tower A')
        unitno = request.POST.get('unitno', 'A-101')
        no_of_rooms = int(request.POST.get('no_of_rooms', 0)) if request.POST.get('no_of_rooms') else 0  # Adjust as needed, providing a default value of 0
        possession_type = request.POST.get('possession_type', 'Ready to Move')
        ageofproperty = request.POST.get('ageofproperty', '10 years')
        possession_year = request.POST.get('possession_year', '2022')
        minlockinperiod = request.POST.get('minlockinperiod', '6 months')
        coworkingseatstype = request.POST.get('coworkingseatstype', 'Open Seat')
        coworking_noofseat = int(request.POST.get('coworking_noofseat', 0)) if request.POST.get('coworking_noofseat') else 0  # Adjust as needed, providing a default value of 0
        plotarea = float(request.POST.get('plotarea', 3000.5)) if request.POST.get('plotarea') else 0  # Adjust as needed
        plot_area_type = request.POST.get('plot_area_type', 'Square Feet')
        terracearea = request.POST.get('terracearea', '500 sq. ft.')
        terrace_area_type = request.POST.get('terrace_area_type', 'Square Feet')
        bathroom = int(request.POST.get('bathroom', 2)) if request.POST.get('bathroom') else 0  # Adjust as needed, providing a default value of 0
        booking_amount = float(request.POST.get('booking_amount', 50000.0)) if request.POST.get('booking_amount') else 0   # Adjust as needed
        address = request.POST.get('address', '123 Main Street')
        landmark = request.POST.get('landmark', 'Near Park')
        state = request.POST.get('state', 'California')
        city = request.POST.get('city', 'Los Angeles')
        pincode = int(request.POST.get('pincode', 90001))  if request.POST.get('pincode') else 0
        furnished_status = request.POST.get('furnished_status', 'Furnished')
        selected_amenities = request.POST.getlist('amenities[]')
        amenities_str = ', '.join(selected_amenities)
        images = request.FILES.getlist('Interior')
        keyword = request.POST.get('keyword','vadodara')
        property_sales_head=request.POST.get('property_sales_head'),
        sales_head_contact_no=request.POST.get('sales_head_contact_no'),
        sales_support_name=request.POST.get('sales_support_name','vadodara'),
        sales_support_contact_no=request.POST.get('sales_support_contact_no','vadodara')
        property_user_id=request.session['user_id']
        addressLine1 = request.POST.get('addressLine1','vadodara')
        addressLine2 = request.POST.get('addressLine2','vadodara')
        addressLine3 = request.POST.get('addressLine3','vadodara')
        commission = request.POST.get('commission','vadodara')
        building_name = request.POST.get('building_name','abc')
        
        pg_name = request.POST.get('pg_name')
        security_deposit = request.POST.get('security_deposit')
        company_lease = request.POST.get('company_lease')
        available_for = request.POST.get('available_for')
        available_from = request.POST.get('available_from')
        suited_for = request.POST.get('suited_for')
        room_type = request.POST.get('room_type')
        pg_furnished_status = request.POST.get('pg_furnished_status', 'semifurished')
        food_available = request.POST.get('food_available')
        food_charges = request.POST.get('food_charges')
        notice_period = request.POST.get('notice_period')
        electricity_charges = request.POST.get('electricity_charges')
        no_of_bed = request.POST.get('no_of_bed')
        pg_rules = request.POST.get('pg_rules')
        gate_closing = request.POST.get('gate_closing')
        pg_services = request.POST.get('pg_services')        
        religion_bias = request.POST.get('religion_bias')
        religion_bias_note = request.POST.get('religion_bias_note')
        google_map = request.POST.get('google_map')
        hospital_localitie = request.POST.get('hospital_localitie') if request.POST.get('hospital_localitie') else 0
        school_localitie = request.POST.get('school_localitie') if request.POST.get('school_localitie') else 0
        railway_staion_localitie = request.POST.get('railway_staion_localitie') if request.POST.get('railway_staion_localitie') else 0
        mall_localitie = request.POST.get('mall_localitie') if request.POST.get('mall_localitie') else 0
        airport_localitie = request.POST.get('airport_localitie') if request.POST.get('airport_localitie') else 0
        restaurants_localitie = request.POST.get('restaurants_localitie') if request.POST.get('restaurants_localitie') else 0
        owner_name = request.POST.get('owner_name')
        owner_email = request.POST.get('owner_email')
        owner_number = request.POST.get('owner_number')
        owner_secondary_number = request.POST.get('owner_secondary_number')
        created_date = datetime.date.today()
        
        if user.user_type == 2:
            timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
            
            electricity_bill = request.FILES['electricity_bill']
            bill_content_types = ['application/pdf', 'image/png', 'image/jpeg']
            if electricity_bill and electricity_bill.content_type not in bill_content_types:
                return HttpResponse("Only PDF, PNG, and JPG files are allowed.")
            electricity_bill_name = f'electricity_bill_{user_id}_{timestamp}{os.path.splitext(electricity_bill.name)[1]}'
            electricity_bill_path = os.path.join(settings.ELECTRICITY_BILL_PATH, electricity_bill_name)
            os.makedirs(os.path.dirname(electricity_bill_path), exist_ok=True)
            with open(electricity_bill_path, 'wb') as destination_file:
                for chunk in electricity_bill.chunks():
                    destination_file.write(chunk)
            
            property_deed = request.FILES['property_deed']
            if property_deed and property_deed.content_type != 'application/pdf':
                return HttpResponse("Only PDF files are allowed.")
            property_deed_name = f'property_deed_{user_id}_{timestamp}{os.path.splitext(property_deed.name)[1]}'
            property_deed_path = os.path.join(settings.PROPERTY_DEED_PATH, property_deed_name)
            os.makedirs(os.path.dirname(property_deed_path), exist_ok=True)
            with open(property_deed_path, 'wb') as destination_file:
                for chunk in property_deed.chunks():
                    destination_file.write(chunk)
        
        saved_paths = []
        if request.method == 'POST' and request.FILES.getlist('Interior'):
            try:
                images = request.FILES.getlist('Interior')
                cover_image_name = request.POST.get('coverImage')  # Get the name of the selected cover image
                for image in images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename = f"IMG_{user_id}_{timestamp}_{image.name}"
                    # Construct the destination path
                    
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)
                    saved_paths.append(new_filename)
                    
                    # Get the category information from the request
                    category = request.POST.get('category_' + image.name)
                    is_cover_image = 1 if new_filename == cover_image_name else 0  # Determine if this image is the cover image
                    
                    # Save the image information to the database
                    PropertyImages.objects.create(
                        property_id=property_unique_id,
                        images=new_filename,
                        category=category,
                        cover_image=is_cover_image
                    )
                    # return JsonResponse({'message': 'Images saved successfully.'})
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'No images provided.'}, status=400)
        # Handle Floor Plan Images
        if request.FILES.getlist('floorplan'):
            try:
                floorplan_images = request.FILES.getlist('floorplan')
                for image in floorplan_images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    floorplan_filename = f"FLOOR_{user_id}_{timestamp}_{image.name}"
                    
                    # Construct the destination path
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floorplan_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)

                    # Save the floor plan image information to the database
                    PropertyImages.objects.create(
                        property_id=property_unique_id,
                        images=floorplan_filename
                    )
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'No floor plan images provided.'}, status=400)
        rera_id = request.POST.get('rera_id', 'ABC123')
        video_url = request.POST.get('video_url', 'https://www.youtube.com/watch?v=example')
        brochure_links = request.POST.get('brochure_links', 'brochure1.pdf,brochure2.pdf')
        property = PropertyMaster(
            property_unique_id=property_unique_id,
            name=name,            
            pg_name=pg_name,
            security_deposit=security_deposit,
            company_lease=company_lease,
            available_for=available_for,
            available_from=available_from,
            suited_for=suited_for,
            room_type=room_type,
            pg_furnished_status=pg_furnished_status,
            food_available=food_available,
            food_charges=food_charges,
            notice_period=notice_period,
            electricity_charges=electricity_charges,
            no_of_bed=no_of_bed,
            pg_rules=pg_rules,
            gate_closing=gate_closing,
            pg_services=pg_services,
            
            furnished_status=furnished_status,
            property_for=property_for, #bookingamoutn, price, pincode
            property_type=property_type,
            property_sub_type=property_sub_type,
            price=price,
            rent_amount=rent_amount,
            rent_type=rent_type,
            maintanence_amount=maintanence_amount,
            maintainence_type=maintainence_type,
            property_description=property_description,
            ageofproperty_office=ageofproperty_office,
            pantry=pantry,
            coworking_pantry=coworking_pantry,
            coveredparking=coveredparking,
            uncoveredparking=uncoveredparking,
            balcony=balcony,
            powerbackup=powerbackup,
            coworkingpowerbackup=coworkingpowerbackup,
            parking=parking,
            coworkingparking=coworkingparking,
            liftavailable=liftavailable,
            coworkingliftavailable=coworkingliftavailable,
            office_view=office_view,
            flooring=flooring,
            floorno=floorno,
            coworkingfloorno=coworkingfloorno,
            towerblock=towerblock,
            unitno=unitno,
            no_of_rooms=no_of_rooms,
            possession_type=possession_type,
            ageofproperty=ageofproperty,
            possession_year=possession_year,
            minlockinperiod=minlockinperiod,
            coworkingseatstype=coworkingseatstype,
            coworking_noofseat=coworking_noofseat,
            plotarea=plotarea,
            plot_area_type=plot_area_type,
            terracearea=terracearea,
            terrace_area_type=terrace_area_type,
            bathroom=bathroom,
            booking_amount=booking_amount,
            address=address,
            landmark=landmark,
            state=state,
            city=city,
            pincode=pincode,
            amenities=amenities_str,
            images=saved_paths,
            rera_id=rera_id,
            video_url=video_url,
            brochure_links=brochure_links,
            keyword=keyword,
            property_sales_head=property_sales_head,
            sales_head_contact_no=sales_head_contact_no,
            sales_support_name=sales_support_name,
            sales_support_contact_no=sales_support_contact_no,
            property_user_id=property_user_id,
            building_name = building_name,
            addressLine1 = addressLine1,
            addressLine2 = addressLine2,
            addressLine3 = addressLine3,
            commission = commission,
            religion_bias = religion_bias,
            religion_bias_note = religion_bias_note,
            google_map = google_map,
            hospital_localitie = hospital_localitie,
            school_localitie = school_localitie,
            railway_staion_localitie = railway_staion_localitie,
            mall_localitie = mall_localitie,
            airport_localitie = airport_localitie,
            restaurants_localitie = restaurants_localitie,
            owner_name = owner_name,
            owner_email = owner_email,
            owner_number = owner_number,
            owner_secondary_number = owner_secondary_number,
            created_date = created_date,
            electricity_bill = electricity_bill_name,
            property_deed = property_deed_name,
            is_active = 0,
            is_verify = 0
        )        
        property.save()       
        
        create_variants(request, property.property_id)
           
    if request.POST.get("property_posted_from") == 'buyer':
        return redirect('buyer-property')
    else:
        return redirect('myproperty')

def create_variants(request, property_id):
    property_master = PropertyMaster.objects.get(property_id=property_id)

    if request.method == 'POST':
        if request.POST.get('no_of_rooms2'):
            no_of_variant2 = int(request.POST.get('number_of_variants2', 0))
            for i in range(1, no_of_variant2 + 1):
                sq_ft2 = float(request.POST.get(f'variant_sq_ft_2_{i}', 0.0))
                price2 = float(request.POST.get(f'variant_price_2_{i}', 0.0))
                bathroom2 = request.POST.get(f'variant_bathroom_2_{i}')
                floor_image2 = request.FILES.get(f'variant_floor_image_2_{i}')
                
                if floor_image2:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename2 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image2.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename2)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image2.chunks():
                            destination_file.write(chunk)
                
                variant2 = Variant(
                    variant_id=property_master,
                    no_of_rooms=2,
                    variant_type='2BHK',
                    no_of_variant=no_of_variant2,
                    sq_ft=sq_ft2,
                    price=price2,
                    bathroom=bathroom2,
                    floor_image=new_filename2 if floor_image2 else None
                )
                variant2.save()

        if request.POST.get('no_of_rooms3'):
            no_of_variant3 = int(request.POST.get('number_of_variants3', 0))
            for i in range(1, no_of_variant3 + 1):
                sq_ft3 = float(request.POST.get(f'variant_sq_ft_3_{i}', 0.0))
                price3 = float(request.POST.get(f'variant_price_3_{i}', 0.0))
                bathroom3 = request.POST.get(f'variant_bathroom_3_{i}')
                floor_image3 = request.FILES.get(f'variant_floor_image_3_{i}')
                
                if floor_image3:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename3 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image3.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename3)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image3.chunks():
                            destination_file.write(chunk)
                            
                variant3 = Variant(
                    variant_id=property_master,
                    no_of_rooms=3,
                    variant_type='3BHK',
                    no_of_variant=no_of_variant3,
                    sq_ft=sq_ft3,
                    price=price3,
                    bathroom=bathroom3,
                    floor_image=new_filename3 if floor_image3 else None
                )
                variant3.save()

        if request.POST.get('no_of_rooms4'):
            no_of_variant4 = int(request.POST.get('number_of_variants4', 0))
            for i in range(1, no_of_variant4 + 1):
                sq_ft4 = float(request.POST.get(f'variant_sq_ft_4_{i}', 0.0))
                price4 = float(request.POST.get(f'variant_price_4_{i}', 0.0))
                bathroom4 = request.POST.get(f'variant_bathroom_4_{i}')
                floor_image4 = request.FILES.get(f'variant_floor_image_4_{i}')
                
                if floor_image4:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename4 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image4.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename4)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image4.chunks():
                            destination_file.write(chunk)
                
                variant4 = Variant(
                    variant_id=property_master,
                    no_of_rooms=4,
                    variant_type='4BHK',
                    no_of_variant=no_of_variant4,
                    sq_ft=sq_ft4,
                    price=price4,
                    bathroom=bathroom4,
                    floor_image=new_filename4 if floor_image4 else None
                )
                variant4.save()

        return redirect('myproperty')

    return render(request, 'builderpanel/add-residential-property')

def createPropertyFromAdminView(request):
    if request.method == "POST":
        name = request.POST.get('name')
        furnished_status = request.POST.get('furnished_status')
        property_for = request.POST.get('property_for', 'sale')
        property_type = request.POST.get('property_type', 'residential')
        property_sub_type = request.POST.get('property_sub_type', 'apartment')
        price = float(request.POST.get('price', 500000))  # Adjust as needed
        rent_amount = float(request.POST.get('rent_amount', 1000))  # Adjust as needed
        rent_type = request.POST.get('rent_type', 'monthly')
        maintanence_amount = float(request.POST.get('maintanence_amount', 200))  # Adjust as needed
        maintainence_type = request.POST.get('maintainence_type', 'monthly')
        property_description = request.POST.get('property_description', 'A beautiful property')
        ageofproperty_office = request.POST.get('ageofproperty_office', '5 years')
        pantry = request.POST.get('pantry', 'yes')
        coworking_pantry = request.POST.get('coworking_pantry', 'yes')
        coveredparking = request.POST.get('coveredparking', 'yes')
        uncoveredparking = request.POST.get('uncoveredparking', 'no')
        balcony = request.POST.get('balcony', 'yes')
        powerbackup = request.POST.get('powerbackup', 'yes')
        coworkingpowerbackup = request.POST.get('coworkingpowerbackup', 'no')
        parking = request.POST.get('parking', 'yes')
        coworkingparking = request.POST.get('coworkingparking', 'no')
        liftavailable = request.POST.get('liftavailable', 'yes')
        coworkingliftavailable = request.POST.get('coworkingliftavailable', 'no')
        office_view = request.POST.get('office_view', 'Garden View')
        flooring = request.POST.get('flooring', 'Marble')
        floorno = int(request.POST.get('floorno', 0)) if request.POST.get('floorno') else 0  # Adjust as needed, providing a default value of 0
        coworkingfloorno = int(request.POST.get('coworkingfloorno', 0)) if request.POST.get('coworkingfloorno') else 0  # Adjust as needed, providing a default value of 0
        towerblock = request.POST.get('towerblock', 'Tower A')
        unitno = request.POST.get('unitno', 'A-101')
        no_of_rooms = int(request.POST.get('no_of_rooms', 0)) if request.POST.get('no_of_rooms') else 0  # Adjust as needed, providing a default value of 0
        possession_type = request.POST.get('possession_type', 'Ready to Move')
        ageofproperty = request.POST.get('ageofproperty', '10 years')
        possession_year = request.POST.get('possession_year', '2022')
        minlockinperiod = request.POST.get('minlockinperiod', '6 months')
        coworkingseatstype = request.POST.get('coworkingseatstype', 'Open Seat')
        coworking_noofseat = int(request.POST.get('coworking_noofseat', 0)) if request.POST.get('coworking_noofseat') else 0  # Adjust as needed, providing a default value of 0
        plotarea = float(request.POST.get('plotarea', 3000.5))  # Adjust as needed
        plot_area_type = request.POST.get('plot_area_type', 'Square Feet')
        terracearea = request.POST.get('terracearea', '500 sq. ft.')
        terrace_area_type = request.POST.get('terrace_area_type', 'Square Feet')
        bathroom = int(request.POST.get('bathroom', 2)) if request.POST.get('bathroom') else 0  # Adjust as needed, providing a default value of 0
        booking_amount = float(request.POST.get('booking_amount', 50000.0))  # Adjust as needed
        address = request.POST.get('address', '123 Main Street')
        landmark = request.POST.get('landmark', 'Near Park')
        state = request.POST.get('state', 'California')
        city = request.POST.get('city', 'Los Angeles')
        pincode = int(request.POST.get('pincode', 90001))  # Adjust as needed
        furnished_status = request.POST.get('furnished_status', 'Furnished')
        amenities = request.POST.get('amenities', 'Swimming Pool, Gym, Tennis Court')
        images = request.POST.get('images', 'image1.jpg,image2.jpg,image3.jpg')
        floorplan = request.POST.get('floorplan', 'floorplan.jpg')
        rera_id = request.POST.get('rera_id', 'ABC123')
        video_url = request.POST.get('video_url', 'https://www.youtube.com/watch?v=example')
        brochure_links = request.POST.get('brochure_links', 'brochure1.pdf,brochure2.pdf')
        property = PropertyMaster(
            name=name,
            furnished_status=furnished_status,
            property_for=property_for,
            property_type=property_type,
            property_sub_type=property_sub_type,
            price=price,
            rent_amount=rent_amount,
            rent_type=rent_type,
            maintanence_amount=maintanence_amount,
            maintainence_type=maintainence_type,
            property_description=property_description,
            ageofproperty_office=ageofproperty_office,
            pantry=pantry,
            coworking_pantry=coworking_pantry,
            coveredparking=coveredparking,
            uncoveredparking=uncoveredparking,
            balcony=balcony,
            powerbackup=powerbackup,
            coworkingpowerbackup=coworkingpowerbackup,
            parking=parking,
            coworkingparking=coworkingparking,
            liftavailable=liftavailable,
            coworkingliftavailable=coworkingliftavailable,
            office_view=office_view,
            flooring=flooring,
            floorno=floorno,
            coworkingfloorno=coworkingfloorno,
            towerblock=towerblock,
            unitno=unitno,
            no_of_rooms=no_of_rooms,
            possession_type=possession_type,
            ageofproperty=ageofproperty,
            possession_year=possession_year,
            minlockinperiod=minlockinperiod,
            coworkingseatstype=coworkingseatstype,
            coworking_noofseat=coworking_noofseat,
            plotarea=plotarea,
            plot_area_type=plot_area_type,
            terracearea=terracearea,
            terrace_area_type=terrace_area_type,
            bathroom=bathroom,
            booking_amount=booking_amount,
            address=address,
            landmark=landmark,
            state=state,
            city=city,
            pincode=pincode,
            amenities=amenities,
            images=images,
            floorplan=floorplan,
            rera_id=rera_id,
            video_url=video_url,
            brochure_links=brochure_links
        )
        property.save()
    return redirect('realeziadmin-manageproperty')


def createpropertyViews(request):
    
    if request.method == 'POST':
        
        # # Check if any files were uploaded
        # if 'Interior' in request.FILES:
            
        #     # Create a unique folder for each request
        #     upload_folder = create_upload_folder()

        #     # Handle uploaded images and get a list of saved image names
        #     saved_image_names = handle_files(request.FILES.getlist('Interior'), upload_folder)

        #     # Join saved image names into a comma-separated string
        #     image_names_str = ', '.join(saved_image_names)
        # else:
        #     # No images were uploaded, set image_names_str to an empty string or None
        #     image_names_str = None
        

        # Create a new Property instance
        new_property = propertyList()

        selected_amenities = request.POST.getlist('amenities[]')
        amenities_str = ', '.join(selected_amenities)
        
        # Set attributes based on request data
        new_property.name = request.POST.get('name')
        new_property.property_type = request.POST.get('property_type')
        new_property.building_type = request.POST.get('building_type') 
        new_property.property_for = request.POST.get('property_for') 
        new_property.area_type = request.POST.get('area_type') 
        new_property.price = request.POST.get('price')
        new_property.area = request.POST.get('area')
        new_property.bedroom = request.POST.get('bedroom')
        new_property.bathroom = request.POST.get('bathroom')

        new_property.builtyear = 0
        new_property.address = request.POST.get('address')
        new_property.city = request.POST.get('city')
        new_property.state = request.POST.get('state')
        new_property.pincode = request.POST.get('pincode')
        new_property.amenities = amenities_str
        new_property.possession_type = request.POST.get('possession_type')
        new_property.furnished_status = request.POST.get('furnished_status')
        new_property.booking_amount = request.POST.get('booking_amount')
        new_property.project_area = request.POST.get('project_area')
        new_property.rera_id = request.POST.get('rera_id')
        new_property.brochure_link = request.POST.get('brochure_link')
        new_property.is_active = request.POST.get('is_active')
        new_property.is_verify = request.POST.get('is_verify')
        new_property.is_deleted = 0
        new_property.about = request.POST.get('about')
        new_property.interiorimages = 0
        # new_property.user_id = request.session['user_id']
        new_property.save()

        return redirect('myproperty') 
    return redirect('myproperty')     


def create_upload_folder():
    # Define the path where you want to create the upload folder
    folder_name = "uploads"
    base_path = "/static/propertyimages/"  # Replace with the desired base path

    # Join the base path and folder name to create the full path
    upload_folder = os.path.join(base_path, folder_name)

    # Check if the folder already exists, and create it if not
    if not os.path.exists(upload_folder):
        os.makedirs(upload_folder)

    return upload_folder

def login_view(request):
    if request.method == 'POST':
        customer_contact = request.POST['customer_contact']
        password = request.POST['password']

        # Check if the user exists and the password is correct
        user = authenticate(request, customer_contact=customer_contact, password=password)

        if user is not None:
            login(request, user)
            messages.success(request, 'Logged in successfully!')
            request.session['user'] = user
            return redirect('bdashboard')  # Redirect to the home page after successful login
        else:
            messages.error(request, 'Invalid phone number or password.')

    return render(request, 'dashboard.html')  # Replace 'login.html' with your login template

def EmployeeListView(request):
    employees = Employee.objects.all()
    return render(request, 'admin/employelist.html', {'employees': employees})

@login_required
def AdminDashboardView(request):    
    total_earnings = Commissions.objects.aggregate(Sum('commission_amount'))['commission_amount__sum'] or 0    
    properties = PropertyMaster.objects.all()
    active_properties = properties.filter(is_active=1).count()
    properties = PropertyMaster.objects.all()
    house_sold_today = Commissions.objects.filter(created_date=date.today()).count()
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    return render(request, 'adminpanel/admin-dashboard.html', {'total_earnings': total_earnings, 'active_properties': active_properties, 'house_sold_today': house_sold_today, 'notifications': notifications, 'notification_count': notification_count})


def EmployeeAddView(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        mobile_no = request.POST.get('mobile_no')
        password = request.POST.get('password')
        address = request.POST.get('address')        
        last_employee = Employee.objects.all().order_by('id').last()
        if last_employee:
            last_id = int(last_employee.employee_id[3:])
            new_id = last_id + 1
        else:
            new_id = 10001
        
        employee_id = f'EMP{new_id:05d}'
        Employee.objects.create(employee_id=employee_id,name=name, email=email, mobile_no=mobile_no, password=password, address=address)        
        
        return redirect('realeziadmin-employeelist')
    return render(request, 'admin/addemplyee.html')

def employee_status(request, user_id):
    employees = Employee.objects.get(id=user_id)
    employees.is_active = not employees.is_active
    employees.save()
    return redirect('realeziadmin-employeelist')

def EmployeeEditView(request, employee_id):
    employee = Employee.objects.get(id=employee_id)
    if request.method == 'POST':
        employee.name = request.POST.get('name')
        employee.email = request.POST.get('email')
        employee.mobile_no = request.POST.get('mobile_no')
        employee.password = request.POST.get('password')
        employee.address = request.POST.get('address')
        employee.save()
        return redirect('realeziadmin-employeelist')
    return render(request, 'admin/employee_edit.html', {'employee': employee})

# def AdminLoginView(request):
#     user_agent = request.META.get('HTTP_USER_AGENT', '')

#     if 'Apache-HttpClient/4.5.13' in user_agent:
#         print("robot")
#         return HttpResponseForbidden("Access Forbidden")
    
#     template_name = "admin/sign-in.html"
#     # ip_add = requests.get("https://api64.ipify.org?format=json")
#     # resp = ip_add.json()
#     # ip_address = resp["ip"]
#     # print(ip_address)

#     if request.method == "POST":
#         postdata = request.POST

#         uname = postdata.get("username", None)
#         pswd = postdata.get('password', None)

#         user = authenticate(request, username=str(uname).strip(), password=str(pswd).strip())
#         return HttpResponse(user)
#         if user is None:
#             messages.success(request, 'username or password not valid')
#             url = reverse('webapp:index_pagep')
#             return HttpResponseRedirect(url)

#         # elif str(uname) not in ['withclarity_admin@gmail.com']:
#         #     messages.success(request, 'username is not valid')
#         #     url = reverse('webapp_dev:index_page')
#         #     return HttpResponseRedirect(url)


#         diamondObj = DiamondClass()

#         if uname and pswd:
#             # check having user or not with given credentials
#             user = authenticate(request, username=str(uname).strip(), password=str(pswd).strip())

#             try:
#                 users_in_group = Group.objects.get(name="live_supplier").user_set.all()
#             except Exception as ex:
#                 users_in_group = None

#             if user is not None:
#                 # Login Here
#                 try:

#                     if user.is_superuser:
#                         login(request, user)
#                         diamondObj.generateLoginLog(request)

#                         url = reverse('webapp:vendor_addition')
#                         return HttpResponseRedirect(url)

#                     elif user in users_in_group:
#                         login(request, user)
#                         diamondObj.generateLoginLog(request)

#                         url = reverse('seller:custom_diamonds')
#                         return HttpResponseRedirect(url)

#                     else:
#                         messages.success(request, 'username or password not valid')
#                         url = reverse('webapp:index_page')
#                         return HttpResponseRedirect(url)

#                 except Exception as e:
#                     messages.success(request, str(e))
#             else:
#                 messages.success(request, 'username or password not valid')

#         else:
#             messages.success(request, 'Please fill username and password')
#             url = reverse('webapp:index_page')
#             return HttpResponseRedirect(url)

#     return render(request, template_name, locals())

from django.contrib.auth import authenticate, login, logout

def AdminLoginView(request):
    
    template_name = "admin/sign-in.html"
    
    if request.method == 'POST':
        # return HttpResponse(request)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        # return HttpResponse(user)
        if user is not None:
            login(request, user)
            return redirect('realeziadmin-dashboard')
        else:
            return render(request, 'admin/sign-in.html', {'error': 'Invalid username or password'})

    return render(request, template_name)

def AdminsignupView(request):
    template_name = "admin/sign-up.html"
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')

        if password == confirm_password:
            user = User.objects.create_user(username, email, password)
            user.save()
            return redirect('login')
        else:
            messages.error(request, 'Passwords do not match')

    return render(request, template_name)


def saveProfileDetailsView(request):
    if request.method == 'POST' and 'businessdetailsubmit' in request.POST:
        user_id = request.session.get('user_id')
        user_details = get_object_or_404(UserMasterSubDetails,builder_id=user_id)
        user_details.business_name = request.POST.get('business_name')
        user_details.incorporated_since = request.POST.get('incorporated_since')
        user_details.ongoing_residential_projects = int(request.POST.get('ongoing_projects_residential'))
        user_details.ongoing_commercial_projects = int(request.POST.get('ongoing_projects_commercial'))
        user_details.completed_residential_projects = int(request.POST.get('completed_projects_residential'))
        user_details.completed_commercial_projects = int(request.POST.get('completed_projects_commercial'))
        user_details.company_hq_address = request.POST.get('company_hq_address')
        user_details.short_description = request.POST.get('short_description')
        user_details.name = request.POST.get('name')
        user_details.designation = request.POST.get('designation')
        user_details.phone_no = request.POST.get('phone_no')
        user_details.email_id = request.POST.get('email_id')
        user_details.pan_no = request.POST.get('pan_no')
        user_details.pan_card = request.FILES.get('pan_card')
        user_details.rera_id = request.POST.get('rera_id')
        
        user_details.save()
        
        url = 'bpersonaldetails'
        return redirect(url)
    
    elif request.method == 'POST' and 'personaldetailsubmit' in request.POST:
        user_id = request.session.get('user_id')
        user_details = get_object_or_404(UserMasterSubDetails,builder_id=user_id)
        user_details.personal_name = request.POST.get('personal_name')
        user_details.personal_phoneno = request.POST.get('personal_phoneno')
        user_details.personal_email = request.POST.get('personal_email')
        user_details.save()
        
        url = 'bpersonaldetails'
        return redirect(url)
    
    else:
        return HttpResponse('Invalid request method')
    messages.success(request, 'username or password not valid')
    
    
def EmployeeLoginView(request):
    
    template_name = "employee/sign-in.html"
    
    if request.method == 'POST':
        employee_id = request.POST.get('employee_id') 
        password = request.POST.get('password') 
        employee = Employee.objects.filter(employee_id=employee_id,password=password)
        if employee is not None:
            request.session['employee_id']=employee_id
            return redirect('realeziemployee-dashboard')
        else:
            return render(request, 'employee/sign-in.html', {'error': 'Invalid username or password'})
        
    return render(request, template_name)

def EmployeeDashboardView(request):
    employees = Employee.objects.filter(employee_id=request.session['employee_id']).first()
    employee_id = employees.employee_id
    site_visits_count = SiteVisit.objects.filter(rm_id=employee_id).count()
    pending_visits = SiteVisit.objects.filter(rm_id=employee_id, status=0).count()
    done_site_visits = SiteVisit.objects.filter(rm_id=employee_id, status=10).count()
    today = datetime.date.today()
    today_visits = SiteVisit.objects.filter(rm_id=employee_id, visit_date=today).count()
    
    return render(request, 'employeepanel/employee-dashboard.html', {'employees': employees, 'site_visits_count': site_visits_count, 'pending_visits': pending_visits, 'today_visits': today_visits, 'done_site_visits': done_site_visits})

def EmployeeSiteVisitView(request):
    employees = Employee.objects.filter(employee_id=request.session['employee_id']).first()
    employee_id = employees.employee_id
    site_visits = SiteVisit.objects.filter(rm_id=employee_id)
    return render(request, 'employeepanel/site-visit.html', {'site_visits': site_visits})

def EmployeeSiteVisitApprovalView(request, property_id):
    site_visits = SiteVisit.objects.get(property_id=property_id)
    site_visits.status = 1
    site_visits.save()
    
    builder_comments = f"{site_visits.buyer_name} has inquired for {site_visits.property_name}"
    buyer_comments = f"Congratulations your site visit for {site_visits.property_name} is Booked."
    notification = NotificationsList(
        property_id=site_visits.property_id,
        buyer_id=site_visits.buyer_id,
        builder_id=site_visits.builder_id,
        employee_id=request.session['employee_id'],
        visit_date=site_visits.visit_date,
        builder_comments=builder_comments,
        buyer_comments=buyer_comments
    )
    notification.save()
    return redirect('realeziemployee-site-visit')

def EmployeeSiteVisitStartView(request):
    if request.method == 'POST':
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        start_otp = request.POST.get('start_otp')
        if site_visits.otp == start_otp:
            site_visits.status = 2
            site_visits.save()
            
            SiteVisitHistory.objects.create(
                property_id=site_visits.property_id,
                buyer_id=site_visits.buyer_id,
                builder_id=site_visits.builder_id,
                employee_id=site_visits.rm_id,
                visit_date=site_visits.visit_date,
                start_time=timezone.now()
            )
        else:
            messages.error(request, 'Invalid OTP. Please try again.')
    
    return redirect('realeziemployee-site-visit')

def EmployeeSiteVisitDoneView(request):
    if request.method == 'POST':
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        end_otp = request.POST.get('end_otp')
        if site_visits.otp == end_otp:
            site_visits.status = 10
            site_visits.save()
            site_visit_history = SiteVisitHistory.objects.get(property_id=property_id)
            site_visit_history.end_time = timezone.now()
            site_visit_history.save()
        else:
            messages.error(request, 'Invalid OTP. Please try again.')
    return redirect('realeziemployee-site-visit')

def EmployeeCancelSiteVisitView(request):
    if request.method == 'POST':
        rm_note = request.POST.get('rm_note')
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        site_visits.status = 8
        site_visits.rm_note = rm_note
        site_visits.save()
        return redirect('realeziemployee-site-visit')
    
def BuyerLoginView(request):
    template_name = "login-signup/buyer-login.html"
    if request.method == 'POST':
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        customer_contact = request.POST.get('mobile_no')
        password = request.POST.get('password')
        entered_otp = request.POST.get('otp')
        
        try:
            user = userMaster.objects.get(mobile_no=customer_contact, user_type=2)
        except userMaster.DoesNotExist:
            user = None

        if user:
            # Check if the password is correct
            if entered_otp == str(user.reg_otp):
                # Check if the account is active
                if user.is_active == 1:
                    # Save user's latitude and longitude
                    user.latitude = latitude
                    user.longitude = longitude
                    user.save()

                    # Authentication successful, log the user in
                    request.session['name'] = user.name
                    request.session['user_username'] = user.mobile_no
                    request.session['user_id'] = user.user_id
                    
                    user_sub_details = UserMasterSubDetails.objects.get(builder_id=user.user_id)
                    verification_stage = user_sub_details.verification_stage
                    
                    if verification_stage == '1':
                        return redirect('buyer-bdashboard')
                    else:
                        return redirect('buyer-service-dashboard')
                else:
                    # Account is inactive, display an error message
                    error_message = "Your account has been not activated by the admin."
            else:
                # Password does not match, display an error message
                error_message = "Invalid login credentials."
        else:
            # User does not exist, display an error message
            error_message = "Invalid login credentials."

        return render(request, template_name, {'error_message': error_message})
    return render(request, template_name)

def buyer_service_dashboard(request):    
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = userMaster.objects.filter(user_id=request.session['user_id']).first()
    user_id = request.session.get('user_id')
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists() 
    return render(request, 'buyerpanel/service-dashboard.html', {'user_details':user_details, 'has_properties': has_properties})

def buyer_information(request):    
    user_details = userMaster.objects.filter(id=request.session['user_id']).first()
    user_id = request.session.get('user_id')
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists() 
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/buyer-information.html', {'user_details':user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def BuyerIndex(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    properties = PropertyMaster.objects.filter(property_user_id=request.session['user_id'])
    has_properties = properties.exists()
    active_properties = properties.filter(is_active=1).count()
    user_master = userMaster.objects.filter(user_id=request.session['user_id']).first()
    show_modal = str(user_master.is_agreement_agreed)
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    today_date = datetime.date.today()
    site_visit_count = SiteVisit.objects.filter(buyer_id=request.session['user_id'], visit_date=today_date).count()
    if user_master.subscription_end_date is not None:
        subscription_end_date = (user_master.subscription_end_date - today_date).days
    else:
        subscription_end_date = None    
    site_visits = SiteVisit.objects.filter(buyer_id=request.session['user_id']).order_by('-visit_date')[:6]
    all_site_visits = SiteVisit.objects.filter(buyer_id=request.session['user_id']).order_by('-visit_date')
    
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    
    popular_properties = PropertyMaster.objects.exclude(property_user_id=request.session['user_id']).order_by('-property_id')[:6]
    
    return render(request, 'buyerpanel/buyer-dashboard.html', {'show_modal': show_modal,'user_master': user_master, 'user_details': user_details, 'has_properties': has_properties, 'active_properties': active_properties, 'site_visit_count': site_visit_count, 'subscription_end_date': subscription_end_date, 'site_visits': site_visits, 'all_site_visits': all_site_visits, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count, 'popular_properties': popular_properties})

def buyer_propertys(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    # Get the user's ID from the session
    user_id = request.session.get('user_id')

    # Fetch all properties with user_id matching the logged-in user
    # properties = PropertyMaster.objects.all()
    properties = PropertyMaster.objects.filter(property_user_id=user_id).order_by('-property_id')
    has_properties = properties.exists()
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    
    # Render the template with the properties
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/propertylist.html', {'user_details': user_details, 'properties': properties, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def BuyerPropertyStatusView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.is_active = not property.is_active
    property.save()
    return redirect('buyer-property')

def BuilderPropertyStatusView(request, property_id):
    property = PropertyMaster.objects.get(property_unique_id=property_id)
    property.is_active = not property.is_active
    property.save()
    return redirect('myproperty')

def BuyerAddProperty(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_master = userMaster.objects.get(user_id=request.session['user_id']) 
    user_id = request.session['user_id']
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()    
    if user_details.verification_stage != '1' and user_master.is_agreement_agreed != 1 and user_master.subscription_id == 0:
        return redirect('buyer-service-dashboard')
    else:
        return render(request, 'buyerpanel/add-residential-property.html', {'user_details': user_details, 'user_master': user_master, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})
    return HttpResponse('Add Residential')

def buyeraddcommercialproperty(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_master = userMaster.objects.get(user_id=request.session['user_id']) 
    user_id = request.session['user_id']
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    if user_details.verification_stage != '1' and user_master.is_agreement_agreed != 1 and user_master.subscription_id == 0:
        return redirect('buyer-service-dashboard')
    else:
        return render(request, 'buyerpanel/add-commercial-property.html', {'user_details': user_details, 'user_master': user_master, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})
    return HttpResponse('Add Commercial')

def EditResidentialProperty(request):
    return render(request, 'buyerpanel/edit-residential-property.html')

def EditCommercialProperty(request):
    return render(request, 'buyerpanel/edit-commercial-property.html')

def EditBuyerProperty(request,property_id):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session['user_id']
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()    
    property_obj = PropertyMaster.objects.filter(property_unique_id=property_id).first()     
    property_images = PropertyImages.objects.filter(property_id=property_id)
    upload_images = property_images.filter(images__startswith='IMG')
    floorplan_images = property_images.filter(images__startswith='FLOOR')
    if request.method == 'POST':
        property_obj.property_for = request.POST.get('property_for')
        property_obj.property_sub_type = request.POST.get('property_sub_type')
        property_obj.name = request.POST.get('name')
        property_obj.price = request.POST.get('price')
        if property_obj.property_for != 'Sale':
            property_obj.rent_amount = request.POST.get('rent_amount')
            property_obj.rent_type = request.POST.get('rent_type')
            property_obj.maintanence_amount = request.POST.get('maintanence_amount')
            property_obj.maintainence_type = request.POST.get('maintainence_type')
        property_obj.property_description = request.POST.get('property_description')
        property_obj.security_deposit = request.POST.get('security_deposit')
        if property_obj.property_type == 'commercial':
            property_obj.ageofproperty_office = request.POST.get('ageofproperty_office')
            property_obj.pantry = request.POST.get('pantry')
            property_obj.coveredparking = request.POST.get('coveredparking')
            property_obj.uncoveredparking = request.POST.get('uncoveredparking')
            property_obj.balcony = request.POST.get('balcony')
            property_obj.powerbackup = request.POST.get('powerbackup')
            property_obj.liftavailable = request.POST.get('liftavailable')
            property_obj.office_view = request.POST.get('office_view')
            property_obj.flooring = request.POST.get('flooring')
            property_obj.floorno = request.POST.get('floorno')
            property_obj.towerblock = request.POST.get('towerblock')
            property_obj.unitno = request.POST.get('unitno')
            property_obj.coworking_pantry = request.POST.get('coworking_pantry')
            property_obj.coworkingparking = request.POST.get('parking')
            property_obj.coworkingpowerbackup = request.POST.get('coworkingpowerbackup')
            property_obj.coworkingliftavailable = request.POST.get('coworkingliftavailable')
            property_obj.coworkingfloorno = request.POST.get('floorno')
            property_obj.minlockinperiod = request.POST.get('minlockinperiod')
            property_obj.coworkingseatstype = request.POST.get('seatstype')
            property_obj.coworking_noofseat = request.POST.get('coworking_noofseat')
            property_obj.possession_type = request.POST.get('possession_type')
            property_obj.ageofproperty = request.POST.get('ageofproperty')
            property_obj.possession_year = request.POST.get('possession_year')
        property_obj.no_of_rooms = request.POST.get('no_of_rooms') if request.POST.get('no_of_rooms') else 0
        property_obj.plotarea = request.POST.get('plotarea') if request.POST.get('plotarea') else 0
        property_obj.plot_area_type = request.POST.get('area_type')
        property_obj.terracearea = request.POST.get('residential_terracearea')
        property_obj.terrace_area_type = request.POST.get('terrace_area_type')
        property_obj.bathroom = request.POST.get('bathroom')
        property_obj.booking_amount = request.POST.get('booking_amount')
        property_obj.building_name = request.POST.get('building_name')
        property_obj.addressLine1 = request.POST.get('addressLine1')
        property_obj.addressLine2 = request.POST.get('addressLine2')
        property_obj.addressLine3 = request.POST.get('addressLine3')
        property_obj.address = request.POST.get('address')
        property_obj.landmark = request.POST.get('landmark')
        property_obj.state = request.POST.get('state')
        property_obj.city = request.POST.get('city')
        property_obj.pincode = request.POST.get('pincode')
        property_obj.furnished_status = request.POST.get('furnished_status')
        property_obj.amenities = request.POST.get('amenities[]')
        property_obj.rera_id = request.POST.get('rera_id')
        property_obj.video_url = request.POST.get('video_url')
        property_obj.property_sales_head = request.POST.get('property_sales_head')
        property_obj.sales_head_contact_no = request.POST.get('sales_head_contact_no')
        property_obj.sales_support_name = request.POST.get('sales_support_name')
        property_obj.sales_support_contact_no = request.POST.get('sales_support_contact_no')
        property_obj.keyword = request.POST.get('keyword')
        
        property_obj.pg_name = request.POST.get('pg_name')
        property_obj.security_deposit = request.POST.get('security_deposit')
        property_obj.company_lease = request.POST.get('company_lease')
        property_obj.available_for = request.POST.get('available_for')
        property_obj.available_from = request.POST.get('available_from')
        property_obj.suited_for = request.POST.get('suited_for')
        property_obj.room_type = request.POST.get('room_type')
        property_obj.pg_furnished_status = request.POST.get('pg_furnished_status')
        property_obj.food_available = request.POST.get('food_available')
        property_obj.food_charges = request.POST.get('food_charges')
        property_obj.notice_period = request.POST.get('notice_period')
        property_obj.electricity_charges = request.POST.get('electricity_charges')
        property_obj.no_of_bed = request.POST.get('no_of_bed')
        property_obj.pg_rules = request.POST.get('pg_rules')
        property_obj.gate_closing = request.POST.get('gate_closing')
        property_obj.pg_services = request.POST.get('pg_services')
        property_obj.religion_bias = request.POST.get('religion_bias')
        property_obj.religion_bias_note = request.POST.get('religion_bias_note')
        property_obj.google_map = request.POST.get('google_map')
        property_obj.hospital_localitie = request.POST.get('hospital_localitie')
        property_obj.school_localitie = request.POST.get('school_localitie')
        property_obj.railway_staion_localitie = request.POST.get('railway_staion_localitie')
        property_obj.mall_localitie = request.POST.get('mall_localitie')
        property_obj.airport_localitie = request.POST.get('airport_localitie')
        property_obj.restaurants_localitie = request.POST.get('restaurants_localitie')
        property_obj.owner_name = request.POST.get('owner_name')
        property_obj.owner_email = request.POST.get('owner_email')
        property_obj.owner_number = request.POST.get('owner_number')
        property_obj.owner_secondary_number = request.POST.get('owner_secondary_number')
        if 'electricity_bill' in request.FILES:
            property_obj.electricity_bill = request.FILES['electricity_bill']
        if 'property_deed' in request.FILES:
            property_obj.property_deed = request.FILES['property_deed']
        property_obj.save()
        
        saved_paths = []
        if request.method == 'POST' and request.FILES.getlist('Interior'):
            try:
                images = request.FILES.getlist('Interior')
                cover_image_name = request.POST.get('coverImage')  # Get the name of the selected cover image
                for image in images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename = f"IMG_{property_obj.property_user_id}_{timestamp}_{image.name}"
                    # Construct the destination path
                    
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)
                    saved_paths.append(new_filename)
                    
                    # Get the category information from the request
                    category = request.POST.get('category_' + image.name)
                    is_cover_image = 1 if new_filename == cover_image_name else 0  # Determine if this image is the cover image
                    
                    # Save the image information to the database
                    PropertyImages.objects.create(
                        property_id=property_obj.property_unique_id,
                        images=new_filename,
                        category=category,
                        cover_image=is_cover_image
                    )
                    # return JsonResponse({'message': 'Images saved successfully.'})
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        # Handle Floor Plan Images
        if request.method == 'POST' and request.FILES.getlist('floorplan'):
            try:
                floorplan_images = request.FILES.getlist('floorplan')
                for image in floorplan_images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    floorplan_filename = f"FLOOR_{property_obj.property_user_id}_{timestamp}_{image.name}"
                    
                    # Construct the destination path
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floorplan_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)

                    # Save the floor plan image information to the database
                    PropertyImages.objects.create(
                        property_id=property_obj.property_unique_id,
                        images=floorplan_filename
                    )
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        
        if request.POST.get("property_edited_from") == 'builder':
            return redirect('myproperty')
        else:
            return redirect('buyer-property')
            
    if property_obj.property_type == 'residential':
        return render(request, 'buyerpanel/edit-residential-property.html',{'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count, 'property_obj':property_obj, 'upload_images': upload_images, 'floorplan_images': floorplan_images})
    else:
        return render(request, 'buyerpanel/edit-commercial-property.html',{'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count, 'property_obj':property_obj, 'upload_images' :upload_images, 'floorplan_images': floorplan_images})
        
    # return render(request, 'buyer/editresidentialproperty.html', {'property_obj': property_obj})

def EditBuilderProperty(request,property_id):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    property_obj = PropertyMaster.objects.filter(property_unique_id=property_id).first()
    variants = Variant.objects.filter(variant_id=property_obj.property_id)    
    property_images = PropertyImages.objects.filter(property_id=property_id)
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    existing_amenities = property_obj.amenities.split(', ') if property_obj.amenities else []
    if request.method == 'POST':
        
        property_obj.property_for = request.POST.get('property_for')
        property_obj.property_sub_type = request.POST.get('property_sub_type')
        property_obj.name = request.POST.get('name')
        property_obj.price = request.POST.get('price')
        if property_obj.property_for != 'Sale':
            property_obj.rent_amount = request.POST.get('rent_amount')
            property_obj.rent_type = request.POST.get('rent_type')
            property_obj.maintanence_amount = request.POST.get('maintanence_amount')
            property_obj.maintainence_type = request.POST.get('maintainence_type')
        property_obj.property_description = request.POST.get('property_description')
        property_obj.security_deposit = request.POST.get('security_deposit')
        if property_obj.property_type == 'commercial':
            property_obj.ageofproperty_office = request.POST.get('ageofproperty_office')
            property_obj.pantry = request.POST.get('pantry')
            property_obj.coveredparking = request.POST.get('coveredparking')
            property_obj.uncoveredparking = request.POST.get('uncoveredparking')
            property_obj.balcony = request.POST.get('balcony')
            property_obj.powerbackup = request.POST.get('powerbackup')
            property_obj.liftavailable = request.POST.get('liftavailable')
            property_obj.office_view = request.POST.get('office_view')
            property_obj.flooring = request.POST.get('flooring')
            property_obj.floorno = request.POST.get('floorno')
            property_obj.towerblock = request.POST.get('towerblock')
            property_obj.unitno = request.POST.get('unitno')
            property_obj.coworking_pantry = request.POST.get('coworking_pantry')
            property_obj.coworkingparking = request.POST.get('parking')
            property_obj.coworkingpowerbackup = request.POST.get('coworkingpowerbackup')
            property_obj.coworkingliftavailable = request.POST.get('coworkingliftavailable')
            property_obj.coworkingfloorno = request.POST.get('coworkingfloorno')
            property_obj.minlockinperiod = request.POST.get('minlockinperiod')
            property_obj.coworkingseatstype = request.POST.get('seatstype')
            property_obj.coworking_noofseat = request.POST.get('coworking_noofseat') if request.POST.get('coworking_noofseat') else 0
            property_obj.possession_type = request.POST.get('possession_type')
            property_obj.ageofproperty = request.POST.get('ageofproperty')
            property_obj.possession_year = request.POST.get('possession_year')
        property_obj.no_of_rooms = request.POST.get('no_of_rooms') if request.POST.get('no_of_rooms') else 0
        property_obj.plotarea = request.POST.get('residential_area') if request.POST.get('residential_area') else 0
        property_obj.plot_area_type = request.POST.get('area_type')
        property_obj.terracearea = request.POST.get('residential_terracearea')
        property_obj.terrace_area_type = request.POST.get('terrace_area_type')
        property_obj.bathroom = request.POST.get('bathroom')
        property_obj.booking_amount = request.POST.get('booking_amount')
        property_obj.building_name = request.POST.get('building_name')
        property_obj.addressLine1 = request.POST.get('addressLine1')
        property_obj.addressLine2 = request.POST.get('addressLine2')
        property_obj.addressLine3 = request.POST.get('addressLine3')
        property_obj.address = request.POST.get('address')
        property_obj.landmark = request.POST.get('landmark')
        property_obj.state = request.POST.get('state')
        property_obj.city = request.POST.get('city')
        property_obj.pincode = request.POST.get('pincode')
        property_obj.furnished_status = request.POST.get('furnished_status')
        selected_amenities = request.POST.getlist('amenities[]')
        property_obj.amenities = ', '.join(selected_amenities)
        property_obj.rera_id = request.POST.get('rera_id')
        property_obj.video_url = request.POST.get('video_url')
        property_obj.property_sales_head = request.POST.get('property_sales_head')
        property_obj.sales_head_contact_no = request.POST.get('sales_head_contact_no')
        property_obj.sales_support_name = request.POST.get('sales_support_name')
        property_obj.sales_support_contact_no = request.POST.get('sales_support_contact_no')
        property_obj.keyword = request.POST.get('keyword')
        
        property_obj.pg_name = request.POST.get('pg_name')
        property_obj.security_deposit = request.POST.get('security_deposit')
        property_obj.company_lease = request.POST.get('company_lease')
        property_obj.available_for = request.POST.get('available_for')
        property_obj.available_from = request.POST.get('available_from')
        property_obj.suited_for = request.POST.get('suited_for')
        property_obj.room_type = request.POST.get('room_type')
        property_obj.pg_furnished_status = request.POST.get('pg_furnished_status')
        property_obj.food_available = request.POST.get('food_available')
        property_obj.food_charges = request.POST.get('food_charges')
        property_obj.notice_period = request.POST.get('notice_period')
        property_obj.electricity_charges = request.POST.get('electricity_charges')
        property_obj.no_of_bed = request.POST.get('no_of_bed')
        property_obj.pg_rules = request.POST.get('pg_rules')
        property_obj.gate_closing = request.POST.get('gate_closing')
        property_obj.pg_services = request.POST.get('pg_services')
        property_obj.religion_bias = request.POST.get('religion_bias')
        property_obj.religion_bias_note = request.POST.get('religion_bias_note')
        property_obj.google_map = request.POST.get('google_map')
        property_obj.hospital_localitie = request.POST.get('hospital_localitie')
        property_obj.school_localitie = request.POST.get('school_localitie')
        property_obj.railway_staion_localitie = request.POST.get('railway_staion_localitie')
        property_obj.mall_localitie = request.POST.get('mall_localitie')
        property_obj.airport_localitie = request.POST.get('airport_localitie')
        property_obj.restaurants_localitie = request.POST.get('restaurants_localitie')
        property_obj.save()
        
        # for variant in variants:
        #     if variant.variant_type == '2BHK':
        #         no_of_variant2 = int(request.POST.get('number_of_variants2', 0))
        #         for i in range(1, no_of_variant2 + 1):
        #             sq_ft2 = float(request.POST.get(f'variant_sq_ft_2_{i}', 0.0))
        #             price2 = float(request.POST.get(f'variant_price_2_{i}', 0.0))
        #             bathroom2 = request.POST.get(f'variant_bathroom_2_{i}')
        #             floor_image2 = request.FILES.get(f'variant_floor_image_2_{i}')
                
        #             if floor_image2:
        #                 destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floor_image2.name)
        #                 os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                        
        #                 with open(destination_path, 'wb') as destination_file:
        #                     for chunk in floor_image2.chunks():
        #                         destination_file.write(chunk)     
                                                                
        #             print("my loop 1")
        #             variant.no_of_rooms = 2
        #             variant.variant_type = '2BHK'
        #             variant.no_of_variant = no_of_variant2
        #             variant.sq_ft = sq_ft2
        #             variant.price = price2
        #             variant.bathroom = bathroom2
        #             variant.floor_image=floor_image2.name if floor_image2 else None
        #             print("sq_ft "+str(sq_ft2)+" price"+str(price2)+" bathroom"+str(bathroom2)+" floor_image"+str(floor_image2.name))
        #             variant.save()

        #     if variant.variant_type == '3BHK':
        #         no_of_variant3 = int(request.POST.get('number_of_variants3', 0))
        #         for j in range(1, no_of_variant3 + 1):
        #             sq_ft3 = float(request.POST.get(f'variant_sq_ft_3_{j}', 0.0))
        #             price3 = float(request.POST.get(f'variant_price_3_{j}', 0.0))
        #             bathroom3 = request.POST.get(f'variant_bathroom_3_{j}')
        #             floor_image3 = request.FILES.get(f'fileInput_floorplan3_{j}')
        #             if floor_image3:
        #                 destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floor_image3.name)
        #                 os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                        
        #                 with open(destination_path, 'wb') as destination_file:
        #                     for chunk in floor_image3.chunks():
        #                         destination_file.write(chunk)   
        #             print("my loop 2")
        #             variant.no_of_rooms = 3
        #             variant.variant_type = '3BHK'
        #             variant.no_of_variant = no_of_variant3
        #             variant.sq_ft = sq_ft3
        #             variant.price = price3
        #             variant.bathroom = bathroom3
        #             variant.floor_image=floor_image3.name if floor_image3 else None
        #             variant.save()

        #     if variant.variant_type == '4BHK':
        #         no_of_variant4 = int(request.POST.get('number_of_variants4', 0))
        #         for k in range(1, no_of_variant4 + 1):
        #             sq_ft4 = float(request.POST.get(f'variant_sq_ft_4_{k}', 0.0))
        #             price4 = float(request.POST.get(f'variant_price_4_{k}', 0.0))
        #             bathroom4 = request.POST.get(f'variant_bathroom_4_{k}')
        #             floor_image4 = request.FILES.get(f'fileInput_floorplan4_{k}')
        #             if floor_image4:
        #                 destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floor_image4.name)
        #                 os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                        
        #                 with open(destination_path, 'wb') as destination_file:
        #                     for chunk in floor_image4.chunks():
        #                         destination_file.write(chunk)   
        #             print("my loop 3")
        #             variant.no_of_rooms = 4
        #             variant.variant_type = '4BHK'
        #             variant.no_of_variant = no_of_variant4
        #             variant.sq_ft = sq_ft4
        #             variant.price = price4
        #             variant.bathroom = bathroom4
        #             variant.floor_image=floor_image4.name if floor_image4 else None
        #             variant.save()
        
        variants.delete()
        
        update_variants(request, property_obj.property_id)
        
        images = request.FILES.getlist('Interior')
        saved_paths = []
        if request.method == 'POST' and request.FILES.getlist('Interior'):
            try:
                images = request.FILES.getlist('Interior')
                cover_image_name = request.POST.get('coverImage')  # Get the name of the selected cover image
                for image in images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename = f"IMG_{property_obj.property_user_id}_{timestamp}_{image.name}"
                    # Construct the destination path
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)
                    saved_paths.append(new_filename)
                    
                    # Get the category information from the request
                    category = request.POST.get('category_' + image.name)
                    is_cover_image = 1 if new_filename == cover_image_name else 0  # Determine if this image is the cover image
                    
                    # Save the image information to the database
                    PropertyImages.objects.create(
                        property_id=property_obj.property_unique_id,
                        images=new_filename,
                        category=category,
                        cover_image=is_cover_image
                    )
                    # return JsonResponse({'message': 'Images saved successfully.'})
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        
        # Handle updates to existing images
        for image in property_images:
            category = request.POST.get('category_' + image.images)
            cover_image_status = 1 if request.POST.get('coverImage') == image.images else 0
            image.category = category
            image.cover_image = cover_image_status
            image.save()
            
        return redirect('myproperty')
            
    if property_obj.property_type == 'residential':
        return render(request, 'builderpanel/edit-residential-property.html',{'property_obj':property_obj, 'existing_amenities': existing_amenities, 'user_details': user_details, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset, 'variants': variants, 'property_images': property_images})
    else:
        return render(request, 'builderpanel/edit-commercial-property.html',{'property_obj':property_obj, 'user_details': user_details, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def update_variants(request, property_id):
    property_master = PropertyMaster.objects.get(property_id=property_id)   
    existing_variant  = Variant.objects.filter(variant_id=property_master.property_id) 

    if request.method == 'POST':
        if request.POST.get('no_of_rooms2'):
            no_of_variant2 = int(request.POST.get('number_of_variants2', 0))
            for i in range(1, no_of_variant2 + 1):
                sq_ft2 = float(request.POST.get(f'variant_sq_ft_2_{i}', 0.0))
                price2 = float(request.POST.get(f'variant_price_2_{i}', 0.0))
                bathroom2 = request.POST.get(f'variant_bathroom_2_{i}')
                floor_image2 = request.FILES.get(f'variant_floor_image_2_{i}')            
                existing_image2 = request.POST.get(f'variant_existing_image_2_{i}')

                if floor_image2:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename2 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image2.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename2)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image2.chunks():
                            destination_file.write(chunk)
                    floor_image_name = new_filename2
                else:
                    floor_image_name = existing_image2
                variant2 = Variant(
                    variant_id=property_master,
                    no_of_rooms=2,
                    variant_type='2BHK',
                    no_of_variant=no_of_variant2,
                    sq_ft=sq_ft2,
                    price=price2,
                    bathroom=bathroom2,
                    floor_image=floor_image_name
                )
                variant2.save()

        if request.POST.get('no_of_rooms3'):
            no_of_variant3 = int(request.POST.get('number_of_variants3', 0))
            for i in range(1, no_of_variant3 + 1):
                sq_ft3 = float(request.POST.get(f'variant_sq_ft_3_{i}', 0.0))
                price3 = float(request.POST.get(f'variant_price_3_{i}', 0.0))
                bathroom3 = request.POST.get(f'variant_bathroom_3_{i}')
                floor_image3 = request.FILES.get(f'variant_floor_image_3_{i}')      
                existing_image3 = request.POST.get(f'variant_existing_image_3_{i}')
                
                if floor_image3:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename3 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image3.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename3)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image3.chunks():
                            destination_file.write(chunk)
                    floor_image_name = new_filename3
                else:
                    floor_image_name = existing_image3
                            
                variant3 = Variant(
                    variant_id=property_master,
                    no_of_rooms=3,
                    variant_type='3BHK',
                    no_of_variant=no_of_variant3,
                    sq_ft=sq_ft3,
                    price=price3,
                    bathroom=bathroom3,
                    floor_image=floor_image_name
                )
                variant3.save()

        if request.POST.get('no_of_rooms4'):
            no_of_variant4 = int(request.POST.get('number_of_variants4', 0))
            for i in range(1, no_of_variant4 + 1):
                sq_ft4 = float(request.POST.get(f'variant_sq_ft_4_{i}', 0.0))
                price4 = float(request.POST.get(f'variant_price_4_{i}', 0.0))
                bathroom4 = request.POST.get(f'variant_bathroom_4_{i}')
                floor_image4 = request.FILES.get(f'variant_floor_image_4_{i}')      
                existing_image4 = request.POST.get(f'variant_existing_image_4_{i}')
                
                if floor_image4:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename4 = f"FLOOR_{property_master.property_user_id}_{timestamp}_{floor_image4.name}"
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename4)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                    
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in floor_image4.chunks():
                            destination_file.write(chunk)
                    floor_image_name = new_filename4
                else:
                    floor_image_name = existing_image4
                
                variant4 = Variant(
                    variant_id=property_master,
                    no_of_rooms=4,
                    variant_type='4BHK',
                    no_of_variant=no_of_variant4,
                    sq_ft=sq_ft4,
                    price=price4,
                    bathroom=bathroom4,
                    floor_image=floor_image_name
                )
                variant4.save()

        return redirect('myproperty')

    return render(request, 'builderpanel/add-residential-property')

   
def delete_image(request):
    if request.method == 'POST':
        image_name = request.POST.get('image_name')
        
        try:
            PropertyImages.objects.filter(images=image_name).delete()
            return JsonResponse({'success': True})
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=500)

    return JsonResponse({'success': False}, status=400)

def Buyerpersonaldetails(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists() 
    
    if request.method == 'POST':  
        tab = request.POST.get('tab')
        
        if tab == 'personalDetails':        
            user_details.personal_name = request.POST.get('personal_name')
            user_details.date_of_birth = request.POST.get('date_of_birth')
            user_details.city = request.POST.get('city')
            user_details.pin_code = request.POST.get('pin_code')
            selected_language = request.POST.getlist('language')
            user_details.language =  ', '.join(selected_language)
            user_details.personal_email = request.POST.get('personal_email')
            user_details.personal_phoneno = request.POST.get('personal_phoneno')
            user_details.verification_stage = '1'
            user_details.save()
            
            return redirect(f"{reverse('buyer-profile')}?tab=agreement")
            
        elif tab == 'agreement':  
            if 'signed_agreement' in request.FILES:
                signed_agreement = request.FILES['signed_agreement']
                timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                signed_agreement_name = f'AGREEMENT_{user_id}_{timestamp}{os.path.splitext(signed_agreement.name)[1]}'
                signed_agreement_path = os.path.join(settings.BUYER_AGREEMENT_PATH, signed_agreement_name)            
                os.makedirs(os.path.dirname(signed_agreement_path), exist_ok=True)

                with open(signed_agreement_path, 'wb+') as destination_file:
                    for chunk in signed_agreement.chunks():
                        destination_file.write(chunk)  
                        
                user_details.signed_agreement = signed_agreement_name
                user_details.save()    
                user_master = get_object_or_404(userMaster, user_id=user_id)
                user_master.is_agreement_agreed = 1
                user_master.save()
            
        elif tab == 'changePassword':
            old_password = request.POST.get('old_password')
            new_password = request.POST.get('new_password')
            confirm_password = request.POST.get('confirm_password')

            user_id = request.session.get('user_id')
            user = userMaster.objects.filter(id=user_id).first()
            
            if check_password(old_password, user.password):
                if new_password == confirm_password:
                    user.password = make_password(new_password)
                    user.save()  
                else: 
                    return render(request, 'buyerpanel/my-profile.html', {'error': 'New password and confirm password do not match'})
            else:
                return render(request, 'buyerpanel/my-profile.html', {'error': 'Old password is incorrect'})
                
        return redirect('buyer-bdashboard')
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/my-profile.html', {'active_tab': request.GET.get('tab', 'personalDetails'), 'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def RaiseTicket(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/raise-ticket.html', {'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def my_journey(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    user_type = userMaster.objects.filter(user_type=1)
    user_id = user_type.values_list('user_id', flat=True)  
    properties = PropertyMaster.objects.filter(property_user_id__in=user_id)
    property = PropertyMaster.objects.filter(property_user_id=request.session['user_id'])
    has_properties = property.exists()
    site_visits = SiteVisit.objects.filter(buyer_id=request.session['user_id'])
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    saved_property = SavedProperty.objects.filter(user_id=request.session['user_id'])
    return render(request, 'buyerpanel/my-journey.html', {'user_details': user_details, 'properties': properties, 'has_properties': has_properties, 'site_visits': site_visits, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count, 'saved_property': saved_property})

def BuyerCancelSiteVisitView(request):
    if request.method == 'POST':
        buyer_note = request.POST.get('buyer_note')
        property_id = request.POST.get('property_id')
        site_visits = SiteVisit.objects.get(property_id=property_id)
        site_visits.status = 6
        site_visits.buyer_note = buyer_note
        site_visits.save()
        return redirect('buyer-journey')

def propertyviewed(request):
    p_viewed = PropertyViewed.objects.select_related('property_master').all()
    return render(request, 'buyer/property-viewed.html', {'p_viewed': p_viewed})

def ContactUs(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/contact-us.html', {'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def ReferAndEarn(request):    
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/refer-earn.html', {'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def ScheduleVisitView(request):
    if request.method == 'POST':
        action = request.POST.get('action')

        if action == 'fetch_slots':
            day_of_week = request.POST.get('day_of_week')
            property_id = request.POST.get('property_id')
            property = PropertyMaster.objects.get(property_id=property_id)
            builder_id = property.property_user_id

            # Fetch time slots for the selected day and builder
            time_slots = TimeSlot.objects.filter(builder_id=builder_id, days=day_of_week)
            time_slot_data = list(time_slots.values('from_time', 'to_time'))

            return JsonResponse({'time_slots': time_slot_data})
        
        elif action == 'schedule':
            visit_date = request.POST.get('visit_date')
            visit_time = request.POST.get('visit_time')
            property_id = request.POST.get('property_id')
            
            property = PropertyMaster.objects.get(property_id=property_id)
            property_name = property.name
            builder_id = property.property_user_id
            
            buyer = userMaster.objects.get(user_id=request.session.get('user_id'))
            buyer_name = buyer.name
            
            builder = userMaster.objects.get(user_id=builder_id)
            builder_name = builder.name
            
            site_visit = SiteVisit(
                buyer_id=request.session['user_id'],
                buyer_name=buyer_name,
                builder_id=builder_id,
                builder_name=builder_name,
                visit_date=visit_date,
                property_id=property_id,
                property_name=property_name,
                visit_time=visit_time,
                otp = '12345',
                status=0 
            )
            site_visit.save()
            
            return redirect('buyer-journey')
        
        elif action == 'reschedule':
            visit_date = request.POST.get('visit_date')
            visit_time = request.POST.get('visit_time')
            property_id = request.POST.get('property_id')

            site_visit = get_object_or_404(SiteVisit, property_id=property_id, buyer_id=request.session['user_id'])
            site_visit.visit_date = visit_date
            site_visit.visit_time = visit_time
            site_visit.status = 3 
            site_visit.save()
            
            return redirect('buyer-journey')

    return render(request, 'buyerpanel/my-journey.html')

def ServicesView(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/services.html', {'user_details': user_details, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def SubscriptionView(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    user_id = request.session.get('user_id')
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists() 
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/subscriptions.html', {'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def EMI_calculator(request):
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    user_id = request.session.get('user_id')
    properties = PropertyMaster.objects.filter(property_user_id=user_id)
    has_properties = properties.exists() 
    site_visits_queryset = NotificationsList.objects.filter(buyer_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'buyerpanel/emi-calculator.html', {'user_details': user_details, 'has_properties': has_properties, 'site_visits_queryset': site_visits_queryset, 'notification_count': notification_count})

def update_time_slots(request):
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])    
    if request.method == "POST":
        for time_slot in time_slots:
            if time_slot.days == 'Sunday':
                time_slot.from_time = f"{request.POST.get('sun_from_time')} {request.POST.get('sun_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('sun_to_time')} {request.POST.get('sun_to_meridiem')}"
            if time_slot.days == 'Monday':
                time_slot.from_time = f"{request.POST.get('mon_from_time')} {request.POST.get('mon_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('mon_to_time')} {request.POST.get('mon_to_meridiem')}"
            if time_slot.days == 'Tuesday':
                time_slot.from_time = f"{request.POST.get('tue_from_time')} {request.POST.get('tue_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('tue_to_time')} {request.POST.get('tue_to_meridiem')}"
            if time_slot.days == 'Wednesday':
                time_slot.from_time = f"{request.POST.get('wed_from_time')} {request.POST.get('wed_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('wed_to_time')} {request.POST.get('wed_to_meridiem')}"
            if time_slot.days == 'Thursday':
                time_slot.from_time = f"{request.POST.get('thu_from_time')} {request.POST.get('thu_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('thu_to_time')} {request.POST.get('thu_to_meridiem')}"
            if time_slot.days == 'Friday':
                time_slot.from_time = f"{request.POST.get('fri_from_time')} {request.POST.get('fri_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('fri_to_time')} {request.POST.get('fri_to_meridiem')}"
            if time_slot.days == 'Saturday':
                time_slot.from_time = f"{request.POST.get('sat_from_time')} {request.POST.get('sat_from_meridiem')}"
                time_slot.to_time = f"{request.POST.get('sat_to_time')} {request.POST.get('sat_to_meridiem')}"
            time_slot.save()
        return redirect('bdashboard')
    return render(request, 'builderpanel/base.html', {'time_slots': time_slots})

def businessdetail(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/business-details.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def site_visits(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits = SiteVisit.objects.filter(builder_id=request.session['user_id'])
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/site-visits.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'site_visits': site_visits, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def DatapointsignupView(request):
    if 'datapoint' in request.session:
        del request.session['datapoint']
    template_name = "datapoint/register.html"
    if request.method == "POST":
        name = request.POST.get("name")
        mobile_no = request.POST.get("mobile_no")
        email = request.POST.get("email")
        password = request.POST.get("password")
        repassword = request.POST.get("repassword")
        if password != repassword:
            # If passwords don't match, add error message and redirect
            messages.error(request, "Passwords do not match. Please try again.")
            return redirect('/datapoint/signup')
        encrypted_password  = make_password(password)
        decrypted_password = password
        
        datapoint = DataPoint(name=name,
                        email=email,
                        mobile_no=mobile_no,
                        password=encrypted_password,
                        decrypted_password=decrypted_password,
                        registration_stage=0)
        datapoint.save()
            
        datapoint_id = datapoint.id
        
        link_types = ['A', 'B', 'C']
        for link_type in link_types:
            links_to_allocate = Links.objects.filter(link_type=link_type, allocated_for__isnull=True)[:15]
            
            for link in links_to_allocate:
                link.allocated_for = datapoint_id
                link.save()
        
        request.session['mobile_no'] = mobile_no
        request.session['datapoint'] = True
        return redirect('datapoint-login')

    return render(request, template_name, locals())

def DatapointLoginView(request):
    template_name = "datapoint/sign-in.html"
    if request.method == 'POST':
        customer_contact = request.POST.get('mobile')
        password = request.POST.get('password')
        
        try:
            user = DataPoint.objects.get(mobile_no=customer_contact)
        except DataPoint.DoesNotExist:
            user = None
        
        if check_password(password, user.password):
            # Authentication successful, log the user in
            request.session['name'] = user.name
            request.session['user_username'] = user.mobile_no
            request.session['user_id'] = user.id
            
            if user.registration_stage == '2' or user.registration_stage == '3':
                return redirect('datapoint-allocted-links')
            else:
                return redirect('datapoint-dashboard')

        else:
            # Authentication failed, display an error message
            error_message = "Invalid login credentials."
            return render(request, template_name, {'error_message': error_message})
    return render(request, template_name)

def DatapointDashboardView(request):
    user_id = request.session.get('user_id')
    user_details = DataPoint.objects.filter(id=user_id).first()
    return render(request, 'datapoint/datapoint-dashboard.html', {'user_details': user_details})

def DatapointProfile(request):
    user_id = request.session.get('user_id')
    user_details = DataPoint.objects.filter(id=user_id).first()
    registration_stage = user_details.registration_stage
    if registration_stage == '1':
        return redirect('step2-datapoint')
    else:
        print("ok")
    if request.method == 'POST':         
        user_details.date_of_birth = request.POST.get('date_of_birth')
        user_details.city = request.POST.get('city')
        user_details.pin_code = request.POST.get('pin_code')
        selected_language = request.POST.getlist('language')
        user_details.language =  ', '.join(selected_language)
        user_details.facebook_links = request.POST.get('facebook_links')
        user_details.instagram_links = request.POST.get('instagram_links')
        user_details.linkedin_links = request.POST.get('linkedin_links')
        user_details.registration_stage = '1'
        user_details.save()
        return redirect('step2-datapoint')
    return render(request, 'datapoint/my-profile.html', {'user_details': user_details})

def step2Datapoint(request):
    user_id = request.session.get('user_id')
    user_details = DataPoint.objects.filter(id=user_id).first()
    registration_stage = user_details.registration_stage
    if registration_stage is None or registration_stage == '0':
        return redirect('datapoint-my-profile')
    elif registration_stage == '2':
        return redirect('datapoint/allocted-links')
    else:
        print("ok")
    if request.method == 'POST':
        question1 = request.POST.get('question1')
        question2 = request.POST.get('question2')
        question3 = request.POST.get('question3')
        question4 = request.POST.get('question4')
        question5 = request.POST.get('question5')
        question6 = request.POST.get('question6')
        question7 = request.POST.get('question7')
        question8 = request.POST.get('question8')
        question9 = request.POST.get('question9')
        question10 = request.POST.get('question10')
        
        correct_answers = {
            'question1': 'option1',  
            'question2': 'option1',  
            'question3': 'option1',   
            'question4': 'option1',  
            'question5': 'option4',  
            'question6': 'option4',   
            'question7': 'option1',  
            'question8': 'option4',  
            'question9': 'option4',   
            'question10': 'option1'  
        }
        
        is_correct = (
            question1 == correct_answers['question1'] and
            question2 == correct_answers['question2'] and
            question3 == correct_answers['question3'] and
            question4 == correct_answers['question4'] and
            question5 == correct_answers['question5'] and
            question6 == correct_answers['question6'] and
            question7 == correct_answers['question7'] and
            question8 == correct_answers['question8'] and
            question9 == correct_answers['question9'] and
            question10 == correct_answers['question10']
        )
        
        if is_correct:
            user_details.registration_stage = '2'
            user_details.save()
            return redirect('datapoint-allocted-links')
        else:
            messages.error(request, 'One or more answers are incorrect. Please watch the video and try again.')
            return redirect('step2-datapoint')
    return render(request, 'datapoint/step2-datapoint.html')

def DatapointAlloctedLinksView(request):
    user_id = request.session.get('user_id')
    user_details = DataPoint.objects.filter(id=user_id).first()
    registration_stage = user_details.registration_stage
    if registration_stage is None or registration_stage == '0':
        return redirect('datapoint-my-profile')
    elif registration_stage == '1':
        return redirect('step2-datapoint')
    else:
        links = Links.objects.filter(allocated_for=user_id)
        if request.method == 'POST':
            if 'submit_task' in request.POST:
                user_details.registration_stage = '3'
                user_details.save()
                return redirect('thank-you')
    return render(request, 'datapoint/allocted-links.html', {'links': links, 'user_details': user_details})

def DatapointEnterDetail(request, link_id):
    links = Links.objects.filter(id=link_id).first()
    if request.method == 'POST':    
        links.name  = request.POST.get('name')
        links.mobile_no  = request.POST.get('number')
        links.comment  = request.POST.get('comment')
        links.save()

        return redirect('datapoint-allocted-links')
    return render(request, 'datapoint/enter-detail.html', {'links': links})

def DatapointThankYou(request):
    return render(request, 'datapoint/thank-you.html')

def AllDatapointUserView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    datapoints = DataPoint.objects.all()
    return render(request, 'adminpanel/all-datapoint-user.html', {'notifications': notifications, 'notification_count': notification_count, 'datapoints': datapoints})

def CompleteDatapointTaskView(request):
    notifications = NotificationsList.objects.all()
    notification_count = notifications.count()
    datapoints = DataPoint.objects.filter(registration_stage=3)
    return render(request, 'adminpanel/complete-datapoint-task.html', {'notifications': notifications, 'notification_count': notification_count, 'datapoints': datapoints})

def builder_payment(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    set_payments = SiteVisit.objects.filter(status=9, builder_id=user_id)
    
    time_slots = TimeSlot.objects.filter(builder_id=user_id)
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
     
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    
    p = inflect.engine()
    payment_histories = PaymentHistory.objects.filter(payment_id__in=Payments.objects.filter(builder_id=request.session['user_id']).values_list('payment_id', flat=True))
    payment_property_ids = Payments.objects.filter(builder_id=user_id).values_list('property_id', flat=True)
    # return HttpResponse(payment_property_ids)
    data = []
    for history in payment_histories:
        # Get corresponding Payment record
        payment = Payments.objects.filter(payment_id=history.payment_id).first()
        if payment:
            data.append({
                'payment_id': payment.payment_id,
                'property_id': payment.property_id,
                'payment_amount': history.payment_amount,
                'payment_date': history.payment_date,
                'received_payment_date': history.received_payment_date,
                'id': history.id,
                'status': history.status
            })
            
    def format_amount(amount):
        if amount >= 1000000000:
            return f"{amount / 1000000000:.2f} B"
        elif amount >= 1000000:
            return f"{amount / 1000000:.2f} Lakh"
        else:
            return f"{amount:.2f}"
        
    payment_till_date_no = sum(item['payment_amount'] for item in data if item['status'] == 1)
    total_upcoming_payment_no = sum(item['payment_amount'] for item in data if item['status'] in {0, 2})
    
    payments_with_status_1 = [item for item in data if item['status'] == 1]

    if payments_with_status_1:
        last_payment_date = max(item['received_payment_date'] for item in payments_with_status_1)
        last_payment_amount_no = next(item['payment_amount'] for item in payments_with_status_1 if item['received_payment_date'] == last_payment_date)
    else:
        last_payment_date = None
        last_payment_amount_no = 0
    
    next_payment_data = [item for item in data if item['status'] in {0, 2}]

    if next_payment_data:
        next_payment_date = min(item['payment_date'] for item in next_payment_data)
        next_payment_amount_no = next(item['payment_amount'] for item in next_payment_data if item['payment_date'] == next_payment_date)
    else:
        next_payment_date = None
        next_payment_amount_no = 0
    
    payment_till_date = format_amount(payment_till_date_no)
    total_upcoming_payment = format_amount(total_upcoming_payment_no)
    if next_payment_amount_no is not None:
        next_payment_amount = format_amount(next_payment_amount_no)
    else:
        next_payment_amount = 0

    if last_payment_amount_no is not None:
        last_payment_amount = format_amount(last_payment_amount_no)
    else:
        last_payment_amount = 0
    
    payment_till_date_words = p.number_to_words(payment_till_date_no)
    total_upcoming_payment_words = p.number_to_words(total_upcoming_payment_no)
    
    return render(request, 'builderpanel/payments.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'set_payments': set_payments, 'payment_property_ids': payment_property_ids, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset, 'payment_till_date': payment_till_date, 'total_upcoming_payment': total_upcoming_payment, 'last_payment_amount': last_payment_amount, 'next_payment_amount': next_payment_amount, 'payment_till_date_words': payment_till_date_words, 'total_upcoming_payment_words': total_upcoming_payment_words})

def set_payments(request, property_id):
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first() 
    payments = SiteVisit.objects.filter(property_id=property_id) 
    if request.method == 'POST':          
        property_id = request.POST.get('property_id')
        property_name = request.POST.get('property_name')
        buyer_id = request.POST.get('buyer_id')
        total_payment = request.POST.get('total_payment')
        received_payment = request.POST.get('received_payment')
        pending_payment = request.POST.get('pending_payment')
        payment_type = request.POST.get('payment_type')
        builder_id = request.session['user_id']

        payment = Payments(
            property_id=property_id,
            property_name=property_name,
            buyer_id=buyer_id,
            builder_id=builder_id,
            total_payment=total_payment,
            received_payment=received_payment,
            pending_payment=pending_payment,
            payment_type=payment_type,
            status=0
        )
        payment.save()
        
        payment_amounts = request.POST.getlist('payment_amount[]')
        payment_dates = request.POST.getlist('payment_date[]')
        
        for i in range(len(payment_amounts)):
            payment_history = PaymentHistory(
                payment_id=payment.payment_id,
                property_id=property_id,
                payment_amount=payment_amounts[i],
                payment_date=payment_dates[i],
                status=0
            )
            payment_history.save()
        
        return redirect('payment')
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/set-payments.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'payments': payments, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def my_profile(request):
    return render(request, 'builderpanel/my-profile.html')

def profile_settings(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    if request.method == 'POST':
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('new_password')
        confirm_password = request.POST.get('confirm_password')

        user_id = request.session.get('user_id')
        user = userMaster.objects.filter(id=user_id).first()
        
        if check_password(old_password, user.password):
            if new_password == confirm_password:
                user.password = make_password(new_password)
                user.save()
                return redirect('builderpanel-login')  
            else: 
                return render(request, 'builderpanel/profile-settings.html', {'error': 'New password and confirm password do not match'})
        else:
            return render(request, 'builderpanel/profile-settings.html', {'error': 'Old password is incorrect'})
    
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/profile-settings.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def payment_to_date(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    
    payment_histories = PaymentHistory.objects.filter(payment_id__in=Payments.objects.filter(builder_id=request.session['user_id']).values_list('payment_id', flat=True), status=1)

    data = []
    for history in payment_histories:
        # Get corresponding Payment record
        payment = Payments.objects.filter(payment_id=history.payment_id).first()
        if payment:
            data.append({
                'payment_id': payment.payment_id,
                'property_id': history.property_id,
                'property_name': payment.property_name,
                'buyer_id': payment.buyer_id,
                'payment_amount': history.payment_amount,
                'payment_date': history.payment_date,
                'received_payment_date': history.received_payment_date,
                'id': history.id
            })
    return render(request, 'builderpanel/payments-till-date.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset, 'payment_histories': data})

def total_outstanding_payment(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    
    payment_histories = PaymentHistory.objects.filter(
        Q(status=0) | Q(status=2) | Q(status=3),
        payment_id__in=Payments.objects.filter(builder_id=request.session['user_id']).values_list('payment_id', flat=True)
    )

    data = []
    for history in payment_histories:
        # Get corresponding Payment record
        payment = Payments.objects.filter(payment_id=history.payment_id).first()
        if payment:
            data.append({
                'payment_id': payment.payment_id,
                'property_id': history.property_id,
                'property_name': payment.property_name,
                'buyer_id': payment.buyer_id,
                'payment_amount': history.payment_amount,
                'payment_date': history.payment_date,
                'status': history.status,
                'id': history.id
            })
    return render(request, 'builderpanel/total-outstanding-payment.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset, 'payment_histories': data})

def update_payment_status(request):
    if request.method == "POST":
        payment_id = request.POST.get('payment_id')
        tab = request.POST.get('tab')
        payment_status = PaymentHistory.objects.get(id=payment_id)
        if tab == 'received_payment':
            payment_note = request.POST.get('payment_note')
            payment_status.status = 1
            payment_status.payment_note = payment_note
            payment_status.received_payment_date = datetime.date.today()
            payment_status.save()

            payment = Payments.objects.get(payment_id=payment_status.payment_id)
            payment.received_payment += payment_status.payment_amount
            payment.pending_payment -= payment_status.payment_amount
            payment.save()

        elif tab == 'reschedule_date':
            reschedule_date = request.POST.get('reschedule_date')
            payment_status.payment_date = reschedule_date
            payment_status.status = 2
            payment_status.save()

        elif tab == 'cancel_payment':
            cancel_payment_note = request.POST.get('cancel_payment_note')
            payment_status.cancel_payment_note = cancel_payment_note
            payment_status.status = 3
            payment_status.save()

        return redirect('total-outstanding-payment')



def payment_history(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/payment-history.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def builder_subscription(request):
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/subscription.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def builder_services(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/services.html', {'user_details': user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def builder_service_dashboard(request):    
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = userMaster.objects.filter(user_id=request.session['user_id']).first()
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/service-dashboard.html', {'user_details':user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def team_details(request):
    return render(request, 'builderpanel/team-details.html')

def Residential_CommercialView(request):  
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_master = userMaster.objects.get(user_id=request.session['user_id'])
    if user_master.is_agreement_agreed != 1:
        return HttpResponse("Error: You must agree to the terms and conditions before creating a property.")  
     
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()  
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }  
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count() 
    return render(request, 'builderpanel/add-residential-commercial-property.html', {'user_details':user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def  Add_Residential_Commercial(request):    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    if request.method == "POST":
        name = request.POST.get('name')
        
        furnished_status = request.POST.get('furnished_status')
        property_for = request.POST.get('property_for', 'sale')
        property_type = request.POST.get('property_add_for', 'residential')
        property_sub_type = request.POST.get('property_sub_type', 'apartment')
        if property_sub_type == 'pg':
            name = request.POST.get('pg_name')        
        price = float(request.POST.get('price', 500000)) if request.POST.get('price') else 0
        rent_amount = float(request.POST.get('rent_amount', 0)) if request.POST.get('rent_amount') else 0 
        rent_type = request.POST.get('rent_type', 'monthly')
        maintanence_amount = float(request.POST.get('maintanence_amount', 0)) if request.POST.get('maintanence_amount') else 0 
        maintainence_type = request.POST.get('maintainence_type', 'monthly')
        property_description = request.POST.get('property_description', 'A beautiful property')
        ageofproperty_office = request.POST.get('ageofproperty_office', '5 years')
        pantry = request.POST.get('pantry', 'yes')
        coworking_pantry = request.POST.get('coworking_pantry', 'yes')
        coveredparking = request.POST.get('coveredparking', 'yes')
        uncoveredparking = request.POST.get('uncoveredparking', 'no')
        balcony = request.POST.get('balcony', 'yes')
        powerbackup = request.POST.get('powerbackup', 'yes')
        coworkingpowerbackup = request.POST.get('coworkingpowerbackup', 'no')
        parking = request.POST.get('parking', 'yes')
        coworkingparking = request.POST.get('coworkingparking', 'no')
        liftavailable = request.POST.get('liftavailable', 'yes')
        coworkingliftavailable = request.POST.get('coworkingliftavailable', 'no')
        office_view = request.POST.get('office_view', 'Garden View')
        flooring = request.POST.get('flooring', 'Marble')
        floorno = int(request.POST.get('floorno', 0)) if request.POST.get('floorno') else 0  # Adjust as needed, providing a default value of 0
        coworkingfloorno = int(request.POST.get('coworkingfloorno', 0)) if request.POST.get('coworkingfloorno') else 0  # Adjust as needed, providing a default value of 0
        towerblock = request.POST.get('towerblock', 'Tower A')
        unitno = request.POST.get('unitno', 'A-101')
        no_of_rooms = int(request.POST.get('no_of_rooms', 0)) if request.POST.get('no_of_rooms') else 0  # Adjust as needed, providing a default value of 0
        possession_type = request.POST.get('possession_type', 'Ready to Move')
        ageofproperty = request.POST.get('ageofproperty', '10 years')
        possession_year = request.POST.get('possession_year', '2022')
        minlockinperiod = request.POST.get('minlockinperiod', '6 months')
        coworkingseatstype = request.POST.get('coworkingseatstype', 'Open Seat')
        coworking_noofseat = int(request.POST.get('coworking_noofseat', 0)) if request.POST.get('coworking_noofseat') else 0  # Adjust as needed, providing a default value of 0
        plotarea = float(request.POST.get('plotarea', 3000.5))  # Adjust as needed
        plot_area_type = request.POST.get('plot_area_type', 'Square Feet')
        terracearea = request.POST.get('terracearea', '500 sq. ft.')
        terrace_area_type = request.POST.get('terrace_area_type', 'Square Feet')
        bathroom = int(request.POST.get('bathroom', 2)) if request.POST.get('bathroom') else 0  # Adjust as needed, providing a default value of 0
        booking_amount = float(request.POST.get('booking_amount', 50000.0)) if request.POST.get('booking_amount') else 0   # Adjust as needed
        address = request.POST.get('address', '123 Main Street')
        landmark = request.POST.get('landmark', 'Near Park')
        state = request.POST.get('state', 'California')
        city = request.POST.get('city', 'Los Angeles')
        pincode = int(request.POST.get('pincode', 90001)) if request.POST.get('pincode') else 0 # Adjust as needed
        furnished_status = request.POST.get('furnished_status', 'Furnished')
        selected_amenities = request.POST.getlist('amenities[]')
        amenities_str = ', '.join(selected_amenities)
        images = request.FILES.getlist('Interior')
        keyword = request.POST.get('keyword','vadodara')
        property_sales_head=request.POST.get('property_sales_head'),
        sales_head_contact_no=request.POST.get('sales_head_contact_no'),
        sales_support_name=request.POST.get('sales_support_name'),
        sales_support_contact_no=request.POST.get('sales_support_contact_no')
        property_user_id=request.session['user_id']
        addressLine1 = request.POST.get('addressLine1','vadodara')
        addressLine2 = request.POST.get('addressLine2','vadodara')
        addressLine3 = request.POST.get('addressLine3','vadodara')
        commission = request.POST.get('commission','vadodara')
        building_name = request.POST.get('building_name','abc')
        
        pg_name = request.POST.get('pg_name')
        security_deposit = request.POST.get('security_deposit')
        company_lease = request.POST.get('company_lease')
        available_for = request.POST.get('available_for')
        available_from = request.POST.get('available_from')
        suited_for = request.POST.get('suited_for')
        room_type = request.POST.get('room_type')
        pg_furnished_status = request.POST.get('pg_furnished_status', 'semifurished')
        food_available = request.POST.get('food_available')
        food_charges = request.POST.get('food_charges')
        notice_period = request.POST.get('notice_period')
        electricity_charges = request.POST.get('electricity_charges')
        no_of_bed = request.POST.get('no_of_bed')
        pg_rules = request.POST.get('pg_rules')
        gate_closing = request.POST.get('gate_closing')
        pg_services = request.POST.get('pg_services')
        religion_bias = request.POST.get('religion_bias')
        religion_bias_note = request.POST.get('religion_bias_note')
        google_map = request.POST.get('google_map')
        hospital_localitie = request.POST.get('hospital_localitie')
        school_localitie = request.POST.get('school_localitie')
        railway_staion_localitie = request.POST.get('railway_staion_localitie')
        mall_localitie = request.POST.get('mall_localitie')
        airport_localitie = request.POST.get('airport_localitie')
        restaurants_localitie = request.POST.get('restaurants_localitie')
        saved_paths = []
        if request.method == 'POST' and request.FILES.getlist('Interior'):
            try:
                images = request.FILES.getlist('Interior')
                cover_image_name = request.POST.get('coverImage')  # Get the name of the selected cover image
                for image in images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename = f"IMG_{user_id}_{timestamp}_{image.name}"
                    # Construct the destination path
                    
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)
                    saved_paths.append(new_filename)
                    
                    # Get the category information from the request
                    category = request.POST.get('category_' + image.name)
                    is_cover_image = 1 if new_filename == cover_image_name else 0  # Determine if this image is the cover image
                    
                    # Save the image information to the database
                    PropertyImages.objects.create(
                        property_id=property_unique_id,
                        images=new_filename,
                        category=category,
                        cover_image=is_cover_image
                    )
                    # return JsonResponse({'message': 'Images saved successfully.'})
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'No images provided.'}, status=400)
        # Handle Floor Plan Images
        if request.FILES.getlist('floorplan'):
            try:
                floorplan_images = request.FILES.getlist('floorplan')
                for image in floorplan_images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    floorplan_filename = f"FLOOR_{user_id}_{timestamp}_{image.name}"
                    
                    # Construct the destination path
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floorplan_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)

                    # Save the floor plan image information to the database
                    PropertyImages.objects.create(
                        property_id=property_unique_id,
                        images=floorplan_filename
                    )
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'No floor plan images provided.'}, status=400)
        rera_id = request.POST.get('rera_id', 'ABC123')
        video_url = request.POST.get('video_url', 'https://www.youtube.com/watch?v=example')
        brochure_links = request.POST.get('brochure_links', 'brochure1.pdf,brochure2.pdf')
        property = PropertyMaster(
            name=name,            
            pg_name=pg_name,
            security_deposit=security_deposit,
            company_lease=company_lease,
            available_for=available_for,
            available_from=available_from,
            suited_for=suited_for,
            room_type=room_type,
            pg_furnished_status=pg_furnished_status,
            food_available=food_available,
            food_charges=food_charges,
            notice_period=notice_period,
            electricity_charges=electricity_charges,
            no_of_bed=no_of_bed,
            pg_rules=pg_rules,
            gate_closing=gate_closing,
            pg_services=pg_services,
            
            furnished_status=furnished_status,
            property_for=property_for, #bookingamoutn, price, pincode
            property_type=property_type,
            property_sub_type=property_sub_type,
            price=price,
            rent_amount=rent_amount,
            rent_type=rent_type,
            maintanence_amount=maintanence_amount,
            maintainence_type=maintainence_type,
            property_description=property_description,
            ageofproperty_office=ageofproperty_office,
            pantry=pantry,
            coworking_pantry=coworking_pantry,
            coveredparking=coveredparking,
            uncoveredparking=uncoveredparking,
            balcony=balcony,
            powerbackup=powerbackup,
            coworkingpowerbackup=coworkingpowerbackup,
            parking=parking,
            coworkingparking=coworkingparking,
            liftavailable=liftavailable,
            coworkingliftavailable=coworkingliftavailable,
            office_view=office_view,
            flooring=flooring,
            floorno=floorno,
            coworkingfloorno=coworkingfloorno,
            towerblock=towerblock,
            unitno=unitno,
            no_of_rooms=no_of_rooms,
            possession_type=possession_type,
            ageofproperty=ageofproperty,
            possession_year=possession_year,
            minlockinperiod=minlockinperiod,
            coworkingseatstype=coworkingseatstype,
            coworking_noofseat=coworking_noofseat,
            plotarea=plotarea,
            plot_area_type=plot_area_type,
            terracearea=terracearea,
            terrace_area_type=terrace_area_type,
            bathroom=bathroom,
            booking_amount=booking_amount,
            address=address,
            landmark=landmark,
            state=state,
            city=city,
            pincode=pincode,
            amenities=amenities_str,
            images=saved_paths,
            rera_id=rera_id,
            video_url=video_url,
            brochure_links=brochure_links,
            keyword=keyword,
            property_sales_head=property_sales_head,
            sales_head_contact_no=sales_head_contact_no,
            sales_support_name=sales_support_name,
            sales_support_contact_no=sales_support_contact_no,
            property_user_id=property_user_id,
            building_name = building_name,
            addressLine1 = addressLine1,
            addressLine2 = addressLine2,
            addressLine3 = addressLine3,
            commission = commission,
            religion_bias = religion_bias,
            religion_bias_note = religion_bias_note,
            google_map = google_map,
            hospital_localitie = hospital_localitie,
            school_localitie = school_localitie,
            railway_staion_localitie = railway_staion_localitie,
            mall_localitie = mall_localitie,
            airport_localitie = airport_localitie,
            restaurants_localitie = restaurants_localitie,
            is_active = 0,
            is_verify = 0
        )
        
        property.save()
    return redirect('myproperty')
    return render(request, 'builderpanel/add-residential-commercial-property.html', {'user_details':user_details})

def Edit_Residential_Commercial(request, property_id):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_details = UserMasterSubDetails.objects.filter(builder_id=request.session['user_id']).first()
    property_obj = PropertyMaster.objects.filter(property_unique_id=property_id).first()     
    property_images = PropertyImages.objects.filter(property_id=property_id)
    upload_images = property_images.filter(images__startswith='IMG')
    floorplan_images = property_images.filter(images__startswith='FLOOR')
    existing_amenities = property_obj.amenities.split(', ') if property_obj.amenities else []
          
    if request.method == 'POST':
        property_obj.property_for = request.POST.get('property_for')
        property_obj.property_sub_type = request.POST.get('property_sub_type')
        property_obj.name = request.POST.get('name')
        property_obj.price = request.POST.get('price')
        if property_obj.property_for != 'Sale':
            property_obj.rent_amount = request.POST.get('rent_amount')
            property_obj.rent_type = request.POST.get('rent_type')
            property_obj.maintanence_amount = request.POST.get('maintanence_amount')
            property_obj.maintainence_type = request.POST.get('maintainence_type')
        property_obj.property_description = request.POST.get('property_description')
        property_obj.security_deposit = request.POST.get('security_deposit')
        if property_obj.property_type == 'commercial':
            property_obj.ageofproperty_office = request.POST.get('ageofproperty_office')
            property_obj.pantry = request.POST.get('pantry')
            property_obj.coveredparking = request.POST.get('coveredparking')
            property_obj.uncoveredparking = request.POST.get('uncoveredparking')
            property_obj.balcony = request.POST.get('balcony')
            property_obj.powerbackup = request.POST.get('powerbackup')
            property_obj.liftavailable = request.POST.get('liftavailable')
            property_obj.office_view = request.POST.get('office_view')
            property_obj.flooring = request.POST.get('flooring')
            property_obj.floorno = request.POST.get('floorno')
            property_obj.towerblock = request.POST.get('towerblock')
            property_obj.unitno = request.POST.get('unitno')
            property_obj.coworking_pantry = request.POST.get('coworking_pantry')
            property_obj.coworkingparking = request.POST.get('parking')
            property_obj.coworkingpowerbackup = request.POST.get('coworkingpowerbackup')
            property_obj.coworkingliftavailable = request.POST.get('coworkingliftavailable')
            property_obj.coworkingfloorno = request.POST.get('floorno')
            property_obj.minlockinperiod = request.POST.get('minlockinperiod')
            property_obj.coworkingseatstype = request.POST.get('seatstype')
            property_obj.coworking_noofseat = request.POST.get('coworking_noofseat')
            property_obj.possession_type = request.POST.get('possession_type')
            property_obj.ageofproperty = request.POST.get('ageofproperty')
            property_obj.possession_year = request.POST.get('possession_year')
        property_obj.no_of_rooms = request.POST.get('no_of_rooms') if request.POST.get('no_of_rooms') else 0
        property_obj.plotarea = request.POST.get('residential_area') if request.POST.get('residential_area') else 0
        property_obj.plot_area_type = request.POST.get('area_type')
        property_obj.terracearea = request.POST.get('residential_terracearea')
        property_obj.terrace_area_type = request.POST.get('terrace_area_type')
        property_obj.bathroom = request.POST.get('bathroom')
        property_obj.booking_amount = request.POST.get('booking_amount')
        property_obj.building_name = request.POST.get('building_name')
        property_obj.addressLine1 = request.POST.get('addressLine1')
        property_obj.addressLine2 = request.POST.get('addressLine2')
        property_obj.addressLine3 = request.POST.get('addressLine3')
        property_obj.address = request.POST.get('address')
        property_obj.landmark = request.POST.get('landmark')
        property_obj.state = request.POST.get('state')
        property_obj.city = request.POST.get('city')
        property_obj.pincode = request.POST.get('pincode')
        property_obj.furnished_status = request.POST.get('furnished_status')
        property_obj.amenities = request.POST.get('amenities[]')
        property_obj.rera_id = request.POST.get('rera_id')
        property_obj.video_url = request.POST.get('video_url')
        property_obj.property_sales_head = request.POST.get('property_sales_head')
        property_obj.sales_head_contact_no = request.POST.get('sales_head_contact_no')
        property_obj.sales_support_name = request.POST.get('sales_support_name')
        property_obj.sales_support_contact_no = request.POST.get('sales_support_contact_no')
        property_obj.keyword = request.POST.get('keyword')
        
        property_obj.pg_name = request.POST.get('pg_name')
        property_obj.security_deposit = request.POST.get('security_deposit')
        property_obj.company_lease = request.POST.get('company_lease')
        property_obj.available_for = request.POST.get('available_for')
        property_obj.available_from = request.POST.get('available_from')
        property_obj.suited_for = request.POST.get('suited_for')
        property_obj.room_type = request.POST.get('room_type')
        property_obj.pg_furnished_status = request.POST.get('pg_furnished_status')
        property_obj.food_available = request.POST.get('food_available')
        selected_amenities = request.POST.getlist('amenities[]')
        property_obj.amenities = ', '.join(selected_amenities)
        property_obj.food_charges = request.POST.get('food_charges')
        property_obj.notice_period = request.POST.get('notice_period')
        property_obj.electricity_charges = request.POST.get('electricity_charges')
        property_obj.no_of_bed = request.POST.get('no_of_bed')
        property_obj.pg_rules = request.POST.get('pg_rules')
        property_obj.gate_closing = request.POST.get('gate_closing')
        property_obj.pg_services = request.POST.get('pg_services')        
        property_obj.religion_bias = request.POST.get('religion_bias')
        property_obj.religion_bias_note = request.POST.get('religion_bias_note')
        property_obj.google_map = request.POST.get('google_map')
        property_obj.hospital_localitie = request.POST.get('hospital_localitie')
        property_obj.school_localitie = request.POST.get('school_localitie')
        property_obj.railway_staion_localitie = request.POST.get('railway_staion_localitie')
        property_obj.mall_localitie = request.POST.get('mall_localitie')
        property_obj.airport_localitie = request.POST.get('airport_localitie')
        property_obj.restaurants_localitie = request.POST.get('restaurants_localitie')
        property_obj.save()
        
        saved_paths = []
        if request.method == 'POST' and request.FILES.getlist('Interior'):
            try:
                images = request.FILES.getlist('Interior')
                cover_image_name = request.POST.get('coverImage')  # Get the name of the selected cover image
                for image in images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    new_filename = f"IMG_{property_obj.property_user_id}_{timestamp}_{image.name}"
                    # Construct the destination path
                    
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, new_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)
                    saved_paths.append(new_filename)
                    
                    # Get the category information from the request
                    category = request.POST.get('category_' + image.name)
                    is_cover_image = 1 if new_filename == cover_image_name else 0  # Determine if this image is the cover image
                    
                    # Save the image information to the database
                    PropertyImages.objects.create(
                        property_id=property_obj.property_unique_id,
                        images=new_filename,
                        category=category,
                        cover_image=is_cover_image
                    )
                    # return JsonResponse({'message': 'Images saved successfully.'})
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        # Handle Floor Plan Images
        if request.method == 'POST' and request.FILES.getlist('floorplan'):
            try:
                floorplan_images = request.FILES.getlist('floorplan')
                for image in floorplan_images:
                    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                    floorplan_filename = f"FLOOR_{property_obj.property_user_id}_{timestamp}_{image.name}"
                    
                    # Construct the destination path
                    destination_path = os.path.join(settings.PROPERTY_IMAGES_PATH, floorplan_filename)
                    os.makedirs(os.path.dirname(destination_path), exist_ok=True)

                    # Save the image content to the destination path
                    with open(destination_path, 'wb') as destination_file:
                        for chunk in image.chunks():
                            destination_file.write(chunk)

                    # Save the floor plan image information to the database
                    PropertyImages.objects.create(
                        property_id=property_obj.property_unique_id,
                        images=floorplan_filename
                    )
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        
        return redirect('myproperty')
            
            
    return render(request, 'builderpanel/edit-residential-commercial-property.html',{'property_obj':property_obj, 'existing_amenities': existing_amenities, 'user_details': user_details, 'upload_images': upload_images, 'floorplan_images': floorplan_images})

def payment_overview(request):
    pending_payment_sum = PaymentHistory.objects.filter(pending_payment=True).aggregate(Sum('total_payment'))['total_payment__sum'] or 0
    paid_payment_sum = PaymentHistory.objects.filter(paid_payment=True).aggregate(Sum('total_payment'))['total_payment__sum'] or 0
    
    context = {
        'pending_payment_sum': pending_payment_sum,
        'paid_payment_sum': paid_payment_sum,
        'graph_data': {
            'paid': list(PaymentHistory.objects.filter(paid_payment=True).values_list('total_payment', flat=True)),
            'pending': list(PaymentHistory.objects.filter(pending_payment=True).values_list('total_payment', flat=True)),
        }
    }
    
    return render(request, 'payment_overview.html', context)


def business_overview(request):
    today = datetime.today()
    yesterday = today - timedelta(days=1)
    last_7_days = today - timedelta(days=7)
    last_30_days = today - timedelta(days=30)

    yesterday_data = PropertyViews.objects.filter(date=yesterday).aggregate(Sum('views'), Sum('clicks'))
    
    last_7_days_data = PropertyViews.objects.filter(date__gte=last_7_days, date__lte=today).aggregate(Sum('views'), Sum('clicks'))
    last_30_days_data = PropertyViews.objects.filter(date__gte=last_30_days, date__lte=today).aggregate(Sum('views'), Sum('clicks'))
    full_data = PropertyViews.objects.aggregate(Sum('views'), Sum('clicks'))

    print('Yesterday:', yesterday_data)
    print('Last 7 Days:', last_7_days_data)
    print('Last 30 Days:', last_30_days_data)
    print('Full Data:', full_data)

    context = {
        'yesterday_views': yesterday_data['views__sum'] or 0,
        'yesterday_clicks': yesterday_data['clicks__sum'] or 0,
        'last_7_days_views': last_7_days_data['views__sum'] or 0,
        'last_7_days_clicks': last_7_days_data['clicks__sum'] or 0,
        'last_30_days_views': last_30_days_data['views__sum'] or 0,
        'last_30_days_clicks': last_30_days_data['clicks__sum'] or 0,
        'full_views': full_data['views__sum'] or 0,
        'full_clicks': full_data['clicks__sum'] or 0,
    }
    return render(request, 'builder/businessdetail.html', context)

def graph(request):
    return render(request, 'builder/graph_businessdetail.html')

def store_user_data(request):
    if request.method == 'POST':
        
        business_name = request.POST.get('business_name')
        incorporated_since = request.POST.get('incorporated_since')
        ongoing_residential_projects = request.POST.get('ongoing_residential_projects')
        ongoing_commercial_projects = request.POST.get('ongoing_commercial_projects')
        completed_residential_projects = request.POST.get('completed_residential_projects')
        completed_commercial_projects = request.POST.get('completed_commercial_projects')
        company_hq_address = request.POST.get('company_hq_address')
        short_description = request.POST.get('short_description')
        name = request.POST.get('name')
        designation = request.POST.get('designation')
        phoneno = request.POST.get('phoneno')
        email = request.POST.get('email')
        company_pan_no = request.POST.get('company_pan_no')
        personal_name = request.POST.get('personal_name')
        personal_designation = request.POST.get('personal_designation')
        personal_phoneno = request.POST.get('personal_phoneno')
        personal_whatsapp_number = request.POST.get('personal_whatsapp_number')
        personal_email = request.POST.get('personal_email')
        profile_picture = request.POST.get('profile_picture')
        aadhar_image = request.POST.get('aadhar_image')
        gst_number = request.POST.get('gst_number')
        company_logo = request.POST.get('company_logo')
        pan_image = request.POST.get('pan_image')
        bank_account_no = request.POST.get('bank_account_no')
        ifsc_code = request.POST.get('ifsc_code')
        bank_name = request.POST.get('bank_name')
        account_holder_name = request.POST.get('account_holder_name')
        signature = request.POST.get('signature')

        user = UserMasterSubDetails(
            business_name=business_name,
            incorporated_since=incorporated_since,
            ongoing_residential_projects=ongoing_residential_projects,
            ongoing_commercial_projects=ongoing_commercial_projects,
            completed_residential_projects=completed_residential_projects,
            completed_commercial_projects=completed_commercial_projects,
            company_hq_address=company_hq_address,
            short_description=short_description,
            name=name,
            designation=designation,
            phoneno=phoneno,
            email=email,
            company_pan_no=company_pan_no,
            personal_name=personal_name,
            personal_designation=personal_designation,
            personal_phoneno=personal_phoneno,
            personal_whatsapp_number=personal_whatsapp_number,
            personal_email=personal_email,
            profile_picture=profile_picture,
            aadhar_image=aadhar_image,
            gst_number=gst_number,
            company_logo=company_logo,
            pan_image=pan_image,
            bank_account_no=bank_account_no,
            ifsc_code=ifsc_code,
            bank_name=bank_name,
            account_holder_name=account_holder_name,
            signature=signature,
        )
        user.save()
        
        return redirect("my-profile")

from PyPDF2 import PdfReader, PdfWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from django.core.files.storage import FileSystemStorage
from io import BytesIO
from django.core.mail import EmailMessage

def saveBuilderProfileDetailsView(request):
    if 'user_id' not in request.session:
        return redirect('login')
    
    user_id = request.session.get('user_id')
    user_details = UserMasterSubDetails.objects.filter(builder_id=user_id).first()
    
    if request.method == 'POST':    
        user_id = request.session.get('user_id')
        user_details = get_object_or_404(UserMasterSubDetails,builder_id=user_id)
        tab = request.POST.get('tab')

        if tab == 'businessDetail':        
            user_details.business_name = request.POST.get('business_name')
            user_details.incorporated_since = request.POST.get('incorporated_since')
            user_details.ongoing_residential_projects = int(request.POST.get('ongoing_residential_projects')) if request.POST.get('ongoing_residential_projects') else 0 
            user_details.ongoing_commercial_projects = int(request.POST.get('ongoing_commercial_projects')) if request.POST.get('ongoing_commercial_projects') else 0 
            user_details.completed_residential_projects = int(request.POST.get('completed_residential_projects')) if request.POST.get('completed_residential_projects') else 0 
            user_details.completed_commercial_projects = int(request.POST.get('completed_commercial_projects')) if request.POST.get('completed_commercial_projects') else 0 
            user_details.company_hq_address = request.POST.get('company_hq_address')
            user_details.google_map_link = request.POST.get('google_map_link')
            user_details.short_description = request.POST.get('short_description')        
            user_details.gst_number = request.POST.get('gst_number')     
            user_details.gst_name = request.POST.get('gst_name')    
            if user_details.business_name != user_details.gst_name:
                return JsonResponse({'success': False, 'message': 'Business name and GST name must match.'})
            if request.method == 'POST' and request.FILES.get('company_logo'):
                company_logo = request.FILES['company_logo']
                new_filename = handle_file_upload(company_logo, 'LOGO')
                user_details.company_logo = new_filename 
            user_details.verification_stage = '1'
            user_details.save()        
        
            return JsonResponse({'success': True, 'message': 'Business details saved successfully'})
    
        elif tab == 'personalDetail': 
            user_details.personal_name = request.POST.get('personal_name')
            user_details.personal_phoneno = request.POST.get('personal_phoneno')
            user_details.secondary_number = request.POST.get('secondary_number')
            user_details.personal_email = request.POST.get('personal_email')
            personal_designation = request.POST.get('personal_designation')
            other_designation = request.POST.get('other_designation')
            if personal_designation == 'Others':
                user_details.personal_designation = other_designation
            else:
                user_details.personal_designation = personal_designation
            whatsapp_number_show = request.POST.get('whatsapp_number_show')
            if whatsapp_number_show == 'yes':
                user_details.personal_whatsapp_number = user_details.personal_phoneno
            else:
                user_details.personal_whatsapp_number = request.POST.get('personal_whatsapp_number')
            
            if request.method == 'POST' and request.FILES.get('profile_picture'):
                profile_picture = request.FILES['profile_picture']
                new_filename = handle_file_upload(profile_picture, 'PROFILE')
                user_details.profile_picture = new_filename
            user_details.verification_stage = '2'
            user_details.save() 
            return JsonResponse({'success': True, 'message': 'Personal details saved successfully'})  
        
        elif tab == 'kycDetail': 
            user_details.aadhar_image = request.POST.get('aadhar_image')
            user_details.company_pan_no = request.POST.get('company_pan_no')
            if request.method == 'POST' and request.FILES.get('pan_image'):
                pan_image = request.FILES['pan_image']
                new_filename = handle_file_upload(pan_image, 'PAN')        
                user_details.pan_image = new_filename
            if request.method == 'POST' and request.FILES.get('gst_image'):
                gst_image = request.FILES['gst_image']
                new_filename = handle_file_upload(gst_image, 'GST')        
                user_details.gst_image = new_filename
            user_details.verification_stage = '3'            
            user_details.save() 
            return JsonResponse({'success': True, 'message': 'KYC details saved successfully'})  
        
        elif tab == 'bankDetail': 
            user_details.bank_account_no = int(request.POST.get('bank_account_no')) if request.POST.get('bank_account_no') else 0 
            user_details.ifsc_code = request.POST.get('ifsc_code')
            user_details.bank_name = request.POST.get('bank_name')
            user_details.account_holder_name = request.POST.get('account_holder_name')
            user_details.verification_stage = '4'
            user_details.save() 
            return JsonResponse({'success': True, 'message': 'Bank details saved successfully'})  
    
        elif tab == 'teamDetail':             
            user_details.sales_director_name = request.POST.get('sales_director_name')
            user_details.sales_director_email = request.POST.get('sales_director_email')
            user_details.sales_director_number = request.POST.get('sales_director_number')
            user_details.project_sales_manager_name = request.POST.get('project_sales_manager_name')
            user_details.project_sales_manager_email = request.POST.get('project_sales_manager_email')
            user_details.project_sales_manager_number = request.POST.get('project_sales_manager_number')
            user_details.finance_team_name = request.POST.get('finance_team_name')
            user_details.finance_team_email = request.POST.get('finance_team_email')
            user_details.finance_team_number = request.POST.get('finance_team_number') 
            user_details.verification_stage = '5'                            
            user_details.save() 
            return JsonResponse({'success': True, 'message': 'Team details saved successfully'})         
        
        elif tab == 'channelPartnerAgreementDetail': 
            if request.method == 'POST' and request.FILES.get('signed_agreement'):
                signed_agreement = request.FILES['signed_agreement']
                new_filename = handle_file_upload(signed_agreement, 'AGREEMENT')        
                user_details.signed_agreement = new_filename
            user_details.verification_stage = '7'
            user_details.save()
            user_master = get_object_or_404(userMaster, user_id=user_id)
            user_master.is_agreement_agreed = 1
            user_master.save()
            return redirect('bdashboard') 
        
        # elif tab == 'signatureDetail': 
        #     if request.method == 'POST' and request.FILES.get('signature'):
        #         signature = request.FILES['signature']
        #         new_filename = handle_file_upload(signature, 'SIGNATURE')        
        #         user_details.signature = new_filename
                
        #         # Save the uploaded files
        #         fs = FileSystemStorage()
        #         pdf_file_path = os.path.join(settings.GST_CERTIFICATE_PATH, user_details.gst_image)
        #         png_file_path = os.path.join(settings.SIGNATURE_PATH, user_details.signature)

        #         # Check if files exist
        #         if not os.path.exists(pdf_file_path):
        #             return JsonResponse({'success': False, 'message': 'PDF file does not exist.'})
        #         if not os.path.exists(png_file_path):
        #             return JsonResponse({'success': False, 'message': 'Signature file does not exist.'})

        #         # Read the original PDF
        #         reader = PdfReader(pdf_file_path)
        #         writer = PdfWriter()

        #         # Add all pages from the original PDF to the writer
        #         for page_num in range(len(reader.pages)):
        #             writer.add_page(reader.pages[page_num])

        #         # Create a PDF with the PNG image and add it to the writer
        #         packet = BytesIO()
        #         can = canvas.Canvas(packet, pagesize=letter)
        #         can.drawImage(png_file_path, 0, 0, width=200, height=70)
        #         can.save()

        #         packet.seek(0)
        #         image_pdf = PdfReader(packet)
        #         writer.add_page(image_pdf.pages[0])

        #         # Save the new PDF
        #         output_filename = generate_filename(None, 'output.pdf', 'GST')
        #         output_file_path = os.path.join(settings.GST_CERTIFICATE_PATH, output_filename)
        #         with open(output_file_path, 'wb') as output_pdf:
        #             writer.write(output_pdf)

        #         # Update user_details.gst_image with the new file path
        #         user_details.gst_image = output_filename
                
        #         email = EmailMessage(
        #             subject = 'Channel Partner Agreement with Realezi',
        #             body = 'Hello, Thanks for signing Channel Partner Agreement, Please find a signed copy of agreement in mail',
        #             from_email = settings.EMAIL_HOST_USER,
        #             to = [user_details.personal_email]
        #         )
        #         email.attach_file(output_file_path)
        #         email.send()
                    
        #     user_details.verification_stage = '7' 
        #     user_details.save() 
        #     return redirect('bdashboard')      
    
    time_slots = TimeSlot.objects.filter(builder_id=request.session['user_id'])
    time_slots_dict = {}
    for slot in time_slots:
        if slot.days not in time_slots_dict:
            from_time = slot.from_time.split()
            to_time = slot.to_time.split()
            time_slots_dict[slot.days] = {
            'from_time': from_time[0],
            'from_meridiem': from_time[1],
            'to_time': to_time[0],
            'to_meridiem': to_time[1],
        }
    site_visits_queryset = NotificationsList.objects.filter(builder_id=request.session['user_id'])
    notification_count = site_visits_queryset.count()
    return render(request, 'builderpanel/my-profile.html',{'user_details':user_details, 'time_slots_dict': time_slots_dict, 'notification_count': notification_count, 'site_visits_queryset': site_visits_queryset})

def handle_file_upload(file, file_type):
    new_filename = generate_filename(None, file.name, file_type)
    if file_type == 'PAN':
        file_path = os.path.join(settings.PAN_IMAGES_PATH, new_filename)
    elif file_type == 'GST':
        file_path = os.path.join(settings.GST_CERTIFICATE_PATH , new_filename)
    elif file_type == 'PROFILE':
        file_path = os.path.join(settings.PROFILE_IMAGES_PATH, new_filename)
    elif file_type == 'LOGO':
        file_path = os.path.join(settings.COMPANY_LOGO_PATH, new_filename)
    elif file_type == 'SIGNATURE':
        file_path = os.path.join(settings.SIGNATURE_PATH, new_filename)
    elif file_type == 'AGREEMENT':
        file_path = os.path.join(settings.AGREEMENT_PATH, new_filename)
    
    # Save file to the specified upload_to directory
    with open(file_path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    
    return new_filename

def generate_filename(instance, filename, file_type):
    # Get the current timestamp
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    
    # Generate a random 6-digit number
    random_number = ''.join(random.choices(string.digits, k=6))
    
    # Concatenate parts to form the filename
    if file_type == 'PAN':
        new_filename = f"PAN_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    elif file_type == 'GST':
        new_filename = f"GST_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    elif file_type == 'PROFILE':
        new_filename = f"PROFILE_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    elif file_type == 'LOGO':
        new_filename = f"LOGO_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    elif file_type == 'SIGNATURE':
        new_filename = f"SIGNATURE_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    elif file_type == 'AGREEMENT':
        new_filename = f"AGREEMENT_{timestamp}_RZ{random_number}{os.path.splitext(filename)[1]}"
    
    return new_filename

def download_document(request, document_type):
    user_id = request.session.get('user_id')
    user_details = get_object_or_404(UserMasterSubDetails, builder_id=user_id)
    property_obj = PropertyMaster.objects.filter(property_user_id=user_id)

    if document_type == 'pan':        
        file_path = os.path.join(settings.PAN_IMAGES_PATH, user_details.pan_image)
    elif document_type == 'gst':
        file_path = os.path.join(settings.GST_CERTIFICATE_PATH, user_details.gst_image)
    elif document_type == 'logo':
        file_path = os.path.join(settings.COMPANY_LOGO_PATH, user_details.company_logo)
    else:
        file_path = None

    with open(file_path, 'rb') as f:
        response = HttpResponse(f.read())
        response['Content-Disposition'] = f'attachment; filename={os.path.basename(file_path)}'
        return response

def import_leads(file_path):
    data = pd.read_csv(file_path)
    for _, row in data.iterrows():
        lead = Lead.objects.create(
            lead_id=row['lead_id'],
            name=row['name'],
            email=row['email'],
            phone=row['phone'],
            source=row['source'],
            status=row['status'],
            revenue=row['revenue'],
        )

def manage_lead(request):
    leads = Lead.objects.all().order_by('-lead_id')
    return render(request, 'adminpanel/manage-lead.html', {'leads': leads})

def add_lead(request):
    if request.method == 'POST':
        last_lead_id = Lead.objects.all().order_by('lead_id').last()
        if last_lead_id:
            last_id = int(last_lead_id.lead_id[4:])
            new_id = last_id + 1
        else:
            new_id = 10001

        lead_id = f"LEAD{new_id:05d}"
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        source = request.POST.get('source')
        status = request.POST.get('status')
        revenue = request.POST.get('revenue')

        lead = Lead(
            lead_id=lead_id,
            name=name,
            email=email,
            phone=phone,
            source=source,
            status=status,
            revenue=revenue
        )
        lead.save()
        return redirect('realeziadmin-manage-lead')
    return render(request, 'adminpanel/add-lead.html')

def edit_lead(request, lead_id):
    lead = get_object_or_404(Lead, lead_id=lead_id)
    if request.method == 'POST':
        lead.name = request.POST.get('name')
        lead.email = request.POST.get('email')
        lead.phone = request.POST.get('phone')
        lead.source = request.POST.get('source')
        lead.status = request.POST.get('status')
        lead.revenue = request.POST.get('revenue')
        lead.save()        
        return redirect('realeziadmin-manage-lead')
    return render(request, 'adminpanel/edit-lead.html', {'lead': lead})

def delete_lead(request, lead_id):
    lead = get_object_or_404(Lead, lead_id=lead_id)
    lead.status = 'deleted'
    lead.save()        
    return redirect('realeziadmin-manage-lead')

def lead_funnel():
    # Sample lead counts from each source
    sources = ['Facebook', 'Instagram', 'Google Ads']
    lead_counts = [100, 75, 50]

    plt.bar(sources, lead_counts)
    plt.show()
    
def generate_report(start_date, end_date):
    leads = Lead.objects.filter(created_at__range=[start_date, end_date])
    total_leads = leads.count()
    total_converted = leads.filter(status='converted').count()
    total_revenue = leads.filter(status='converted').aggregate(Sum('revenue'))['revenue__sum']

    conversion_ratio = (total_converted / total_leads) * 100
    arpu = total_revenue / total_leads if total_leads > 0 else 0

    return {
        'total_leads': total_leads,
        'converted_leads': total_converted,
        'conversion_ratio': conversion_ratio,
        'arpu': arpu,
        'total_revenue': total_revenue
    }
