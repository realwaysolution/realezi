# Generated by Django 5.0.4 on 2024-08-30 11:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0068_sitevisit_builder_id_sitevisit_builder_name_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeSlot',
            fields=[
                ('slot_id', models.AutoField(primary_key=True, serialize=False)),
                ('builder_id', models.CharField(max_length=10, null=True)),
                ('days', models.CharField(max_length=20, null=True)),
                ('from_time', models.CharField(max_length=20, null=True)),
                ('to_time', models.CharField(max_length=20, null=True)),
                ('created_date', models.DateField(null=True)),
                ('updated_date', models.TimeField(null=True)),
            ],
            options={
                'db_table': 'time_slot',
            },
        ),
    ]
