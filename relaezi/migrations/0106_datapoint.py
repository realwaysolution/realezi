# Generated by Django 5.0.4 on 2024-09-16 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0105_delete_datapoint'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataPoint',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=20)),
                ('email', models.CharField(max_length=100)),
                ('mobile_no', models.CharField(max_length=10)),
                ('password', models.TextField(max_length=16)),
                ('decrypted_password', models.TextField(max_length=16, null=True)),
                ('date_of_birth', models.DateField(null=True)),
                ('language', models.TextField(null=True)),
                ('city', models.CharField(max_length=20, null=True)),
                ('pin_code', models.CharField(max_length=20, null=True)),
                ('facebook_links', models.CharField(max_length=200, null=True)),
                ('instagram_links', models.CharField(max_length=200, null=True)),
                ('linkedin_links', models.CharField(max_length=200, null=True)),
                ('registration_stage', models.CharField(max_length=10, null=True)),
            ],
            options={
                'db_table': 'data_point',
            },
        ),
    ]
