# Generated by Django 5.0.4 on 2024-08-05 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0035_usermastersubdetails_company_logo_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermastersubdetails',
            name='finance_team_email',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='finance_team_name',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='finance_team_number',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='project_sales_manager_email',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='project_sales_manager_name',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='project_sales_manager_number',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='sales_director_email',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='sales_director_name',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='usermastersubdetails',
            name='sales_director_number',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
