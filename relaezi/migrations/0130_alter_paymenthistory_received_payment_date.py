# Generated by Django 5.0.4 on 2024-09-21 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0129_paymenthistory_received_payment_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymenthistory',
            name='received_payment_date',
            field=models.DateTimeField(null=True),
        ),
    ]
