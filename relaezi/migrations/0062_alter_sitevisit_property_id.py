# Generated by Django 5.0.4 on 2024-08-21 09:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0061_alter_usermaster_created_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitevisit',
            name='property_id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
