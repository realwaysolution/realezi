# Generated by Django 5.0.4 on 2024-08-06 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0038_alter_usermastersubdetails_signature'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermastersubdetails',
            name='verification_stage',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
