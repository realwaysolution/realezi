# Generated by Django 5.0.4 on 2024-09-12 07:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0092_sitevisit_otp'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitevisithistory',
            name='employee_id',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
