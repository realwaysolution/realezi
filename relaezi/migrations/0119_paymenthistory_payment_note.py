# Generated by Django 5.0.4 on 2024-09-18 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0118_alter_propertymaster_property_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenthistory',
            name='payment_note',
            field=models.TextField(null=True),
        ),
    ]
