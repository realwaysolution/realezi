# Generated by Django 5.0.4 on 2024-09-11 10:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0085_datapoint_city_datapoint_date_of_birth_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitevisit',
            name='admin_note',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='sitevisit',
            name='buyer_note',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='sitevisit',
            name='rm_note',
            field=models.TextField(null=True),
        ),
    ]
