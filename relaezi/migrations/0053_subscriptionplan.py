# Generated by Django 5.0.4 on 2024-08-13 10:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0052_usermaster_subscription_end_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionPlan',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.TextField(null=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=20, null=True)),
            ],
            options={
                'db_table': 'subscription_plan',
            },
        ),
    ]
