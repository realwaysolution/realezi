# Generated by Django 5.0.4 on 2024-09-17 05:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0107_payments_remove_payment_user_delete_paymenthistory_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentHistory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('payment_id', models.CharField(max_length=20, null=True)),
                ('property_id', models.CharField(max_length=20, null=True)),
                ('payment_amount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('payment_date', models.DateField(null=True)),
            ],
            options={
                'db_table': 'payment_history',
            },
        ),
    ]
