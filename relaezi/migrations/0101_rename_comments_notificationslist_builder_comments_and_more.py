# Generated by Django 5.0.4 on 2024-09-16 06:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0100_datapoint_decrypted_password'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notificationslist',
            old_name='comments',
            new_name='builder_comments',
        ),
        migrations.RemoveField(
            model_name='notificationslist',
            name='status',
        ),
        migrations.AddField(
            model_name='notificationslist',
            name='buyer_comments',
            field=models.TextField(null=True),
        ),
    ]
