# Generated by Django 5.0.4 on 2024-05-29 11:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0018_propertymaster_available_for_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propertymaster',
            name='pg_name',
            field=models.TextField(null=True, verbose_name='Name of PG'),
        ),
    ]
