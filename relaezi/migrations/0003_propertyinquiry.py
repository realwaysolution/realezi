# Generated by Django 4.1.7 on 2024-04-03 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0002_bloglist_documents_propertymaster_usermaster_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='PropertyInquiry',
            fields=[
                ('inquiry_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('mobile', models.TextField(default='', null=True)),
                ('email', models.TextField(default='', null=True)),
                ('message', models.TextField(default='', null=True)),
                ('inquiry_property_id', models.IntegerField(default=1, max_length=12)),
                ('created_at', models.IntegerField(default=0)),
                ('update_at', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'property_inquiry',
            },
        ),
    ]
