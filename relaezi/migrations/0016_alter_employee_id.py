# Generated by Django 5.0.4 on 2024-05-22 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relaezi', '0015_rename_employee_password_employee_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
